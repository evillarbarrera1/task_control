class CauseHistory < ApplicationRecord
  belongs_to :attorney
  belongs_to :cause
end
