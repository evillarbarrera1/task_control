class WorkStatus < ApplicationRecord
  belongs_to :work
  belongs_to :attorney
end
