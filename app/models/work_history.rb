class WorkHistory < ApplicationRecord
  belongs_to :attorney
  belongs_to :work
end
