class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new
    if user.role == "admin"

      can :manage, :all

    elsif user.role == "attorney"

    elsif user.role == "client"

  end

  end
end
