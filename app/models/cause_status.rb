class CauseStatus < ApplicationRecord
  belongs_to :cause
  belongs_to :attorney
end
