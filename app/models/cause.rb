class Cause < ApplicationRecord
  belongs_to :attorney
  belongs_to :client
end
