class Audience < ApplicationRecord
  belongs_to :attorney
  belongs_to :cause
end
