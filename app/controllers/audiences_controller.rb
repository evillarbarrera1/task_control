class AudiencesController < ApplicationController
  before_action :set_audience, only: [:show, :edit, :update, :destroy]

  # GET /audiences
  # GET /audiences.json
  def index
    @user = current_user.id
    @emailuser = User.find(current_user.id)
    @audiences = Audience.joins(:attorney).joins(:cause)
    .pluck(
      'audiences.date',
      'audiences.attorney_id',
      'attorneys.name',
      'audiences.cause_id',
      'audiences.created_at',
      'audiences.updated_at',
      'audiences.id',
      'audiences.description',
      'audiences.finished',
      'causes.code',
      'attorneys.photo',
    )
   render json: @audiences
  end

  

  # GET /audiences/1
  # GET /audiences/1.json
  def show
    
  end


  # GET /audiences/new
  def new
    @audience = Audience.new
  end

  # GET /audiences/1/edit
  def edit
  end

  # POST /audiences
  # POST /audiences.json
  def create
    @audience = Audience.create(audience_params)
  end

  # PATCH/PUT /audiences/1
  # PATCH/PUT /audiences/1.json
  def update
    respond_to do |format|
      if @audience.update(audience_params)
        format.html { redirect_to @audience, notice: 'Attorney was successfully updated.' }
        format.json { render :show, status: :ok, location: @audience }
      else
        format.html { render :edit }
        format.json { render json: @audience.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /audiences/1
  # DELETE /audiences/1.json
  def destroy
    @audience.destroy
    respond_to do |format|
      format.html { redirect_to audiences_url, notice: 'Audience was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_audience
      @audience = Audience.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def audience_params
      params.require(:audience).permit(:date, :attorney_id, :cause_id, :description,:finished)
    end
end
