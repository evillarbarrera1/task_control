class WorkJsonController < ApplicationController
    def index
        @works = Work.joins(:client)
        .pluck(
          'works.id',
          'works.description',
          'works.client_id',
          'clients.name'
        )
        
          render json: @works
    end
end
