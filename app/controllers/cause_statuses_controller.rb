class CauseStatusesController < ApplicationController
  before_action :set_cause_status, only: [:show, :edit, :update, :destroy]

  # GET /cause_statuses
  # GET /cause_statuses.json
  def index
    @cause_statuses = CauseStatus.joins(:attorney)
    .pluck(
      'cause_statuses.description',
      'cause_statuses.expiration_date',
      'cause_statuses.attorney_id',
      'cause_statuses.id',
      'cause_statuses.finished',
      'cause_statuses.cause_id',
      'attorneys.photo'
    )
   render json: @cause_statuses


  end

  # GET /cause_statuses/1
  # GET /cause_statuses/1.json
  def show
  end

  # GET /cause_statuses/new
  def new
    @cause_status = CauseStatus.new
  end

  # GET /cause_statuses/1/edit
  def edit
  end

  # POST /cause_statuses
  # POST /cause_statuses.json
  def create
    @cause_status = CauseStatus.create(cause_status_params)
  end

  # PATCH/PUT /cause_statuses/1
  # PATCH/PUT /cause_statuses/1.json
  def update
    respond_to do |format|
      if @cause_status.update(cause_status_params)
        format.html { redirect_to @cause_status, notice: 'Audience was successfully updated.' }
        format.json { render :show, status: :ok, location: @cause_status }
      else
        format.html { render :edit }
        format.json { render json: @cause_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cause_statuses/1
  # DELETE /cause_statuses/1.json
  def destroy
    @cause_status.destroy
    respond_to do |format|
      format.html { redirect_to cause_statuses_url, notice: 'Cause status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cause_status
      @cause_status = CauseStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cause_status_params
      params.require(:cause_status).permit(:cause_id, :attorney_id, :expiration_date, :finished,:description)
    end
end
