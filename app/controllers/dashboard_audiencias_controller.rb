class DashboardAudienciasController < ApplicationController
    def index
      @user = current_user.id
      @emailuser = User.find(current_user.id)
        
        sql = "SELECT a.description,"\
            "date(a.date) as fecha_audiencia,"\
            "cast(a.date::timestamp as time) as hora,"\
            "c.name,"\
            "c.id,"\
            "'audiencia' as tipo,"\
            "a.id as id_t,"\
            "c2.name as cliente,"\
            "c.id as id_tr,"\
            "a.attorney_id as attorney_id"\
            " FROM audiences a "\
            " join causes c on a.cause_id = c.id"\
            " join clients c2 on c.client_id = c2.id"\
            " where  a.finished = false"\
            " UNION"\
            " SELECT m.description,"\
                    "date(m.date) as fecha_audiencia,"\
                    "cast(m.date::timestamp as time) as hora,"\
                    "w.name,"\
                    "w.id,"\
                    "'reunion' as tipo,"\
                    "m.id as id_t,"\
                    "c3.name as cliente,"\
                    "w.id as id_tr,"\
                    "m.attorney_id as attorney_id"\
            " from meetings m"\
            " join works w on m.work_id = w.id "\
            " JOIN clients c3 on w.client_id = c3.id"\
            " where  m.finished = false "\
            " order by fecha_audiencia desc"


        @audiencias = ActiveRecord::Base.connection.execute(sql)
        render json: @audiencias
    
    end

  end
   