class WorkStatusesController < ApplicationController
  before_action :set_work_status, only: [:show, :edit, :update, :destroy]

  # GET /work_statuses
  # GET /work_statuses.json
  def index
   
    @user = current_user.id
    @emailuser = User.find(current_user.id)
    @attorney = Attorney.where(:user_id => current_user.id)
  

    if @emailuser.role == "admin"
      @work_statuses = WorkStatus.joins(:attorney)
      .pluck(
        'work_statuses.work_id',
        'work_statuses.finished',
        'work_statuses.expiration_date',
        'work_statuses.attorney_id',
        'attorneys.name',
        'work_statuses.description',
        'work_statuses.id',
        'attorneys.photo'
      )
    else
      
      @attorney.each do |attorney|
        @work_statuses = WorkStatus.joins(:attorney).where(:attorney_id => attorney.id)
        .pluck(
          'work_statuses.work_id',
          'work_statuses.finished',
          'work_statuses.expiration_date',
          'work_statuses.attorney_id',
          'attorneys.name',
          'work_statuses.description',
          'work_statuses.id',
          'attorneys.photo'
        )
      end   

    end

    render json: @work_statuses
  end

  # GET /work_statuses/1
  # GET /work_statuses/1.json
  def show
  end

  # GET /work_statuses/new
  def new
    @work_status = WorkStatus.new
  end

  # GET /work_statuses/1/edit
  def edit
  end

  # POST /work_statuses
  # POST /work_statuses.json
  def create
    @work_status = WorkStatus.create(work_status_params)
    
  end

  # PATCH/PUT /work_statuses/1
  # PATCH/PUT /work_statuses/1.json
  def update
    respond_to do |format|
      if @work_status.update(work_status_params)
        format.html { redirect_to @work_status, notice: 'Work status was successfully updated.' }
        format.json { render :show, status: :ok, location: @work_status }
      else
        format.html { render :edit }
        format.json { render json: @work_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /work_statuses/1
  # DELETE /work_statuses/1.json
  def destroy
    @work_status.destroy
    respond_to do |format|
      format.html { redirect_to work_statuses_url, notice: 'Work status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_work_status
      @work_status = WorkStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def work_status_params
      params.require(:work_status).permit(:work_id, :attorney_id, :expiration_date, :finished, :description)
    end
end
