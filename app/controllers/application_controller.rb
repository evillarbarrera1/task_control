class ApplicationController < ActionController::Base
    before_action :authenticate_user! 

    def index
       @user = current_user.id
       @emailuser = User.find(current_user.id)
       
       @attorney = Attorney.where(:user_id => current_user.id)
       
      

       if @emailuser.role == "admin"

        @tareas = WorkStatus.joins(:attorney)
        .pluck(
          'DATE(work_statuses.expiration_date)',
          'attorneys.name',
          'work_statuses.description',
          'work_statuses.finished',
          'work_statuses.work_id'
          
        )
      
        @cause_statuses = CauseStatus.joins(:attorney)
          .pluck(
            'DATE(cause_statuses.expiration_date)',
            'attorneys.name',
            'cause_statuses.description',
            'cause_statuses.finished',
            'cause_statuses.cause_id'
          )
      else

        @attorney.each do |attorney|
          @tareas = WorkStatus.joins(:attorney).where(:attorney_id => attorney.id)
          .pluck(
            'work_statuses.expiration_date',
            'attorneys.name',
            'work_statuses.description',
            'work_statuses.finished',
            'work_statuses.work_id'
 
          
          )
        end  

      end
    end
    

end
