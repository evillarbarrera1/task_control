class WorkAbogadoController < ApplicationController
    def index
      @user = current_user.id
      @emailuser = User.find(current_user.id)
   
        sql = "select w.id, w.code, w.name, ws.attorney_id, me.attorney_id as id_attorney_me, wh.attorney_id as id_attorney_id_h , w.active, w.attorney_id as attorney_id_w " \
        "from works w " \
        "left join work_statuses ws on ws.work_id = w.id " \
        "left join meetings me on me.work_id = w.id " \
        "left join work_histories wh on wh.work_id = w.id" \
        " order by w.code desc"
       
        @records_array = ActiveRecord::Base.connection.execute(sql)
        render json: @records_array
    
    end

  end
   