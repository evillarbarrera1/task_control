class CausesAbogadoController < ApplicationController
    def index
      @user = current_user.id
      @emailuser = User.find(current_user.id)
   
        sql = "Select c.id,c.code,c.name,cs.attorney_id, c.archived, au.attorney_id as id_attorney_au, ch.attorney_id as id_attorney_id_h , c.attorney_id as id_attorney_c " \
        "from causes c " \
        "left join cause_statuses cs on cs.cause_id = c.id " \
        "left join audiences au on au.cause_id = c.id " \
        "left join cause_histories ch on ch.cause_id = c.id" \
        " order by c.code desc"
       
        @records_array = ActiveRecord::Base.connection.execute(sql)
        render json: @records_array
    

    end

  end
  