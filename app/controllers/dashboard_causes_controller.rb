class DashboardCausesController < ApplicationController
    def index
      @user = current_user.id
      @emailuser = User.find(current_user.id)
   
        sql = "SELECT  cs.description,"\
        "date(cs.expiration_date) as fecha_tarea,"\
        "cast (cs.expiration_date::timestamp as time) as hora,"\
        "c.name,"\
        "c.id,"\
        "'causa' as tipo,"\
        "cs.id as id_t,"\
        "c2.name as cliente,"\
        "c.id as it_g,"\
        "cs.attorney_id as attorney_id"\
        " FROM cause_statuses cs"\
        " join causes c on c.id = cs.cause_id"\
        " join clients c2 on c.client_id = c2.id"\
        " where cs.finished = false"\
        " UNION"\
        " select ws.description,"\
             "date(ws.expiration_date) as fecha_tarea,"\
            "cast (ws.expiration_date::timestamp as time) as hora,"\
            "w.name,"\
            "w.id,"\
            "'gestion' as tipo,"\
            "ws.id as id_t,"\
            "c3.name as cliente,"\
            "w.id as id_g,"\
            "ws.attorney_id as attorney_id"\
        " from work_statuses ws"\
        " join works w on w.id = ws.work_id"\
        " join clients c3 on w.client_id = c3.id"\
        " where ws.finished = false"\
        " order by fecha_tarea desc"
       
        @tareas = ActiveRecord::Base.connection.execute(sql)
        render json: @tareas
    
    end

  end
   