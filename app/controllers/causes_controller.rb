class CausesController < ApplicationController
  before_action :set_cause, only: [:show, :edit, :update, :destroy]

  # GET /causes
  # GET /causes.json
  def index
       @user = current_user.id
       @emailuser = User.find(current_user.id)
       @attorney = Attorney.where(:user_id => current_user.id)
      
        if @emailuser.role == "admin"
          @causes = Cause.all.order(id: :asc)
           @causesarchivadas = Cause.all.where(archived: true)
          @attorney.each do |attorney|
            @id_abogado = attorney.id
          end
        else
          @attorney.each do |attorney|
            @causes = Cause.all.order(id: :asc)
            @id_abogado = attorney.id
          end
        end

    respond_to do |format|
      format.html
      format.json 
    end
    
  end

  # GET /causes/1
  # GET /causes/1.json
  def show
     
  end

  # GET /causes/news
  def new
    @cause = Cause.new
  end

  # GET /causes/1/edit    @cause = Cause.new
  #
  def edit
  end

  # POST /causes
  # POST /causes.json
  def create
    @cause = Cause.create(cause_params)
  end

  # PATCH/PUT /causes/1
  # PATCH/PUT /causes/1.json
  def update
    respond_to do |format|
      if @cause.update(cause_params)
        format.html { redirect_to @cause, notice: 'Cause was successfully updated.' }
        format.json { render :show, status: :ok, location: @cause }
      else
        format.html { render :edit }
        format.json { render json: @cause.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /causes/1
  # DELETE /causes/1.json
  def destroy
    @cause.destroy
    respond_to do |format|
      format.html { redirect_to causes_url, notice: 'Cause was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cause
      @cause = Cause.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cause_params
      params.require(:cause).permit(:name, :court, :code, :description, :attorney_id, :client_id, :role , :caratulado, :amount, :matter, :process, :archived)
    end
end
