class CauseHistoriesController < ApplicationController
  before_action :set_cause_history, only: [:show, :edit, :update, :destroy]

  # GET /cause_histories
  # GET /cause_histories.json
  def index
    @cause_histories = CauseHistory.joins(:attorney)
    .pluck(
      'cause_histories.description',
      'cause_histories.date',
      'cause_histories.id',
      'cause_histories.cause_id',
      'attorneys.photo',
      'attorneys.id'
    )
    render json: @cause_histories
    
  end

  # GET /cause_histories/1
  # GET /cause_histories/1.json
  def show
  end

  # GET /cause_histories/new
  def new
    @cause_history = CauseHistory.new
  end

  # GET /cause_histories/1/edit
  def edit
  end

  # POST /cause_histories
  # POST /cause_histories.json
  def create
    @cause_history = CauseHistory.create(cause_history_params)
  end

  # PATCH/PUT /cause_histories/1
  # PATCH/PUT /cause_histories/1.json
  def update
    respond_to do |format|
      if @cause_history.update(cause_history_params)
        format.html { redirect_to @cause_history, notice: 'Cause history was successfully updated.' }
        format.json { render :show, status: :ok, location: @cause_history }
      else
        format.html { render :edit }
        format.json { render json: @cause_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cause_histories/1
  # DELETE /cause_histories/1.json
  def destroy
    @cause_history.destroy
    respond_to do |format|
      format.html { redirect_to cause_histories_url, notice: 'Cause history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cause_history
      @cause_history = CauseHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cause_history_params
      params.require(:cause_history).permit(:description, :date, :attorney_id, :cause_id)
    end
end
