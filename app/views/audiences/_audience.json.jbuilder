json.extract! audience, :id, :date, :attorney_id, :cause_id, :created_at, :updated_at
json.url audience_url(audience, format: :json)
