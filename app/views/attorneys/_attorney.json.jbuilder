json.extract! attorney, :id, :name, :rut, :profession, :job, :civil_status, :birthdate, :mobile, :email, :photo, :user_id, :created_at, :updated_at, :kind
json.url attorney_url(attorney, format: :json)
