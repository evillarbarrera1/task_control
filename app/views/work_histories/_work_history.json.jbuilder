json.extract! work_history, :id, :description, :date, :attorney_id, :work_id, :created_at, :updated_at
json.url work_history_url(work_history, format: :json)
