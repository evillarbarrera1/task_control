json.extract! cause_status, :id, :cause_id, :attorney_id, :expiration_date, :finished, :description, :created_at, :updated_at
json.url cause_status_url(cause_status, format: :json)
