json.extract! cause_history, :id, :description, :date, :attorney_id, :cause_id, :created_at, :updated_at
json.url cause_history_url(cause_history, format: :json)
