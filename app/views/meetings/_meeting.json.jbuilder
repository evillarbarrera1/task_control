json.extract! meeting, :id, :description, :date, :attorney_id, :work_id, :finished, :created_at, :updated_at
json.url meeting_url(meeting, format: :json)
