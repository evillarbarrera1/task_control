json.extract! work_status, :id, :work_id, :attorney_id, :expiration_date, :finished, :description, :created_at, :updated_at
json.url work_status_url(work_status, format: :json)
