//MOSTRAR LISTADO DE GESTORES
function listadoGestoresActivos() {
    console.log("listadoGestoresActivos")
    $.getJSON('/attorneys.json',
        function(data) {
            var tr;
            $("#table-gestores tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {

                tr = $("<tr/ style='cursor:pointer' onclick='(seleccionarDatosGestores(" + data[i].id + "))'>");
                tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                tr.append("<td>" + data[i].rut + "</td>");
                tr.append("<td >" + data[i].name + "</td>");
                $('#table-gestores').append(tr);
                j = j + 1;

            }
        });

}

//MOSTRAR LISTADO DE CLIENTES
function listadoClientesActivos() {
    console.log("listadoClientesActivos")
    $.getJSON('/clients.json',
        function(data) {
            var tr;
            $("#table-clientes tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                tr = $("<tr/ style='cursor:pointer'  onclick='(seleccionarDatosClientes(" + data[i].id + "))'>");
                tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                tr.append("<td >" + data[i].name + "</td>");
                $('#table-clientes').append(tr);
                j = j + 1;

            }
        });

}

//SELECCIONAR GESTEROES
function seleccionarDatosGestores(id_gestor) {
    modificarGestor()
    var id = id_gestor;
    $("#id_gestor").val(id);


    $.getJSON('/attorneys/' + id + '.json',
        function(data) {
            console.log(data)
            debugger
            $("#nombre_abogado_menu1").val(data.name);
            $("#rut_abogado_menu1").val(data.rut);
            $("#nombrem").val(data.name);
            $("#rutm").val(data.rut);
            $("#profesionm").val(data.profession);
            $("#oficiom").val(data.job);
            $("#estadocivilm").val(data.civil_status);
            $('#email_abogado2').val(data.email);
            $('#fechanacimientom').val(data.birthdate);
            $('#tipogestorm').val(data.kind);
            $('#fotom').val(data.photo);
            $("#id_user").val(data.user_id);
        });

}

//SELECCIONAR CLIENTES

function seleccionarDatosClientes(id_cliente) {
    var id = id_cliente;
    ModificarCliente_Menu();
    $("#id_cliente").val(id);

    //fecha
    var cumpleano = $("#fechanacimiento").val();
    debugger
    $.getJSON('/clients/' + id + '.json',
        function(data) {
            console.log(data)
            $("#nombre_cliente_m").val(data.name);
            $("#rut_cliente_m").val(data.rut);
            $("#nombrecm").val(data.name);
            $("#rutcm").val(data.rut);
            $("#plancm").val(data.plan);
            $("#oficiocm").val(data.kind);
            $("#regionescm").val(data.region);

            $('#emailcm').val(data.mail_legal);
            $('#comunacm').val(data.commune);
            $('#comboabogado_clientem').val(data.attorney_id);


            $('#domiciliom').val(data.address);
            $('#opcionm').val(data.option);
            $('#rlegalm').val(data.name_legal);
            $('#rutlegalm').val(data.rut_legal);
            $("#oficiom").val(data.profession);
            $("#civilm").val(data.marital_status);
            $("#mandatom").val(data.court_order);
            menu_cliente(data.option);

        });
}

//MENU NUEVO ABOGADO
function NuevoGestor() {
    $('#gestores_nuevo').css("display", "");
    $('#gestores_modificar').css("display", "none");
    $('#clientes_nuevo').css("display", "none");
    $('#clientes_modificar').css("display", "none");
}

function modificarGestor() {
    $('#gestores_modificar').css("display", "");
    $('#gestores_nuevo').css("display", "none");
    $('#clientes_nuevo').css("display", "none");
    $('#clientes_modificar').css("display", "none");
}

//LLENAR REGIONES
function llenarRegiones() {
    console.log("llenarRegiones")
    $.getJSON('/regions.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                $("#regionesc").append('<option  value=' + data[i].code + '>' + data[i].name + '</option>');
                $("#regionescm").append('<option  value=' + data[i].code + '>' + data[i].name + '</option>');
            }

        });
}


//MENU CLIENTES
function NuevoCliente() {
    $('#clientes_nuevo').css("display", "");
    $('#gestores_modificar').css("display", "none");
    $('#gestores_nuevo').css("display", "none");
    $('#clientes_modificar').css("display", "none");

}

function ModificarCliente_Menu() {
    $('#clientes_nuevo').css("display", "none");
    $('#clientes_modificar').css("display", "");
    $('#gestores_modificar').css("display", "none");
    $('#gestores_nuevo').css("display", "none");
}

//LLENAR COMUNAS
function llenarComunas() {
    var code_reg = document.getElementById("regionesc").value;
    console.log("LLENAR COMUNAS")
    console.log(code_reg);
    $("#comunac option").remove()
    $.getJSON('/commons.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].code == code_reg) {
                    $("#comunac").append('<option  value=' + data[i].id + '>' + data[i].name + '</option>');
                    $("#comunacm").append('<option  value=' + data[i].id + '>' + data[i].name + '</option>');
                }

            }

        });
}

//AGREGAR GESTORES

function AgregarGestores() {
    console.log("agregarGestores")

    var email = $("#email").val();
    var tipo = $("#tipo").val();
    var password = $("#password").val();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        url: '/user_web',
        data: {
            user: {
                email: email,
                password: password,
                password_confirmation: password,
                role: tipo
            }
        },
        success: function(data, textStatus, jQxhr) {
            Obtenerusuario("abogado")

        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }

    });



}

function Obtenerusuario(cod) {
    console.log("Obtenerusuario")
    $.getJSON('/user_datos.json',
        function(data) {
            var ult = data.length - 1
            if (cod == "abogado") {
                crearAbogado(data[ult].id)
            } else {
                crearClientef(data[ult].id)
            }

        });
}

function crearAbogado(id_user) {
    console.log(crearAbogado);
    var nombre = $("#nombreabogado").val();
    var rut_abogado = $("#rut_abogado").val();
    var profesion = $("#profesion").val();
    var oficio = $("#oficio").val();
    var estadocivil = $("#estadocivil").val();
    var email = $("#email").val();
    var foto = $("#foto").val();
    var tipo = $("#tipo").val();
    var password = $("#password").val();
    var celular = $("#celular").val();

    //fecha
    // var cumpleano = $("#fechanacimiento").val();
    // var ano = cumpleano.substring(0, 4);
    // var mes = cumpleano.substring(7, 5);
    // var dia = cumpleano.substring(10, 8);
    // var hh = cumpleano.substring(13, 11);
    // var mm = cumpleano.substring(16, 14);
    // "birthdate(3i)": dia,
    // "birthdate(2i)": mes,
    // "birthdate(1i)": ano,
    // "birthdate(4i)": hh,
    // "birthdate(5i)": mm,

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        url: '/attorneys',
        data: {
            attorney: {
                name: nombre,
                rut: rut_abogado,
                profession: profesion,
                job: oficio,
                civil_status: estadocivil,
                mobile: celular,
                email: email,
                photo: foto,
                user_id: id_user,
                kind: tipo
            }
        },
        success: function(data, textStatus, jQxhr) {
            toastr.success('Abogado Creado')

        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }

    });
}

//MODIFICAR GESTORES

function modificarGestores() {
    id_abogado = $("#id_gestor").val();
    console.log("modificarGestores");
    console.log(id_abogado);


    var nombre2 = $("#nombrem").val();
    var rut_abogado2 = $("#rutm").val();
    var profesion2 = $("#profesionm").val();
    var oficio2 = $("#oficiom").val();
    var estadocivil2 = $("#estadocivilm").val();
    var email2 = $("#email_abogado2").val();
    var foto2 = $("#fotom").val();
    var tipo2 = $("#tipogestorm").val();


    //fecha
    // var cumpleano = $("#fechanacimientom").val();
    // var ano = cumpleano.substring(0, 4);
    // var mes = cumpleano.substring(7, 5);
    // var dia = cumpleano.substring(10, 8);
    // var hh = cumpleano.substring(13, 11);
    // var mm = cumpleano.substring(16, 14);

    // "birthdate(3i)": dia,
    // "birthdate(2i)": mes,
    // "birthdate(1i)": ano,
    // "birthdate(4i)": hh,
    // "birthdate(5i)": mm,

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/attorneys/' + id_abogado,
        data: {
            attorney: {
                name: nombre2,
                rut: rut_abogado2,
                profession: profesion2,
                job: oficio2,
                civil_status: estadocivil2,
                email: email2,
                photo: foto2,
                kind: tipo2
            },
            success: function(data, textStatus, jQxhr) {
                toastr.success('Se modifico el abogado')

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });
}

//CREAR CLIENTE

function crearCliente() {
    var emailcliente = $("#emailc").val();
    var passwordcliente = $("#passwordc").val();
    debugger
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        url: '/user_web',
        data: {
            user: {
                email: emailcliente,
                password: passwordcliente,
                password_confirmation: passwordcliente,
                role: "cliente"
            }
        },
        success: function(data, textStatus, jQxhr) {
            Obtenerusuario("cliente")

        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }

    });
}

function crearClientef(id_user) {

    var nombrec = $("#nombrec").val();
    var rutc = $("#rutc").val();
    var planc = $("#planc").val();
    var oficio = $("#tipoc").val();

    var regionesc = $("#regionesc").val();
    var comunac = $("#comunac").val();
    var emailc = $("#emailc").val();

    var abogado = $("#comboabogado_clientec").val();

    var opcion = $("#opcionn").val();
    var rlegaln = $("#rlegaln").val();
    var rutlegaln = $("#rutlegaln").val();
    var oficion = $("#oficion").val();
    var civiln = $("#civiln").val();
    var mjudicialn = $("#mjudicialn").val();
    var domicilion = $("#domicilion").val();

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'POST',
        url: '/clients',
        data: {
            client: {
                name: nombrec,
                plan: planc,
                kind: oficio,
                region: regionesc,
                commune: comunac,
                rut: rutc,
                rut_legal: rutlegaln,
                mail_legal: emailc,
                user_id: id_user,
                attorney_id: abogado,
                address: domicilion,
                option: opcion,
                profession: oficion,
                marital_status: civiln,
                court_order: mjudicialn,
                name_legal: rlegaln
            }
        },
        success: function(data, textStatus, jQxhr) {
            toastr.success('Se creo cliente')


        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }

    });

}

//MODIFICAR CLIENTE

function modificarCliente() {
    var id_cliente = $("#id_cliente").val();
    var nombre = $("#nombrecm").val();
    var rut = $("#rutcm").val();
    var plan = $("#plancm").val();
    var oficio = $("#oficiom").val();
    var regiones = $("#regionescm").val();
    var comuna = $("#comunacm").val();
    var email = $("#emailcm").val();
    var abogado_2 = $("#comboabogado_clientem").val();


    var opcionm = $("#opcionm").val();
    var rlegalm = $("#rlegalm").val();
    var rutlegalm = $("#rutlegalm").val();
    var oficiom = $("#oficiom").val();
    var civilm = $("#civilm").val();
    var mjudicialm = $("#mjudicialm").val();
    var domiciliom = $("#domiciliom").val();
    debugger
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/clients/' + id_cliente,
        data: {
            client: {
                name: nombre,
                plan: plan,
                kind: oficio,
                region: regiones,
                commune: comuna,
                rut: rut,
                rut_legal: rutlegalm,
                mail_legal: email,
                phone_legal: 1,
                attorney_id: abogado_2,
                address: domiciliom,
                option: opcionm,
                profession: oficiom,
                marital_status: civilm,
                court_order: mjudicialm,
                name_legal: rlegalm
            }
        },
        success: function(data, textStatus, jQxhr) {
            toastr.success('SE MODIFICO EL CLIENTE')


        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }

    });

}


function menu_cliente(tipo) {
    if (tipo == "Empresa") {
        $('#mrlegalm').css("display", "");
        $('#mrutlegalm').css("display", "");
        // $('#moficiom').css("display", "");
        $('#mcivilm').css("display", "");
    }
    if (tipo == "Persona Natural") {
        $('#mcivilm').css("display", "");
        // $('#moficiom').css("display", "");
        $('#mrlegalm').css("display", "none");
        $('#mrutlegalm').css("display", "none");
    }
}