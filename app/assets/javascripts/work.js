function obtenerLoginWork() {
    console.log("obtenerLoginWork")
    $.getJSON('/login.json',
        function(data) {
            console.log(data);
            var perfil = data.role;
            var user_id = data.id;
            listadoWorkActivas(perfil, user_id);
            listadoWorkArchivadas(perfil, user_id)
        });

}


//MOSTRAR MENUS
function menuWorkAgregar() {
    console.log("menuWorkAgregar")
    $('#menuworkingresar').css("display", "");
    $('#btnworklista').css("display", "");
    $('#menuWorkLista').css("display", "none");
    $('#btnworkingreso').css("display", "none");


}

function workmenulistadoasutons() {
    $('#t-menuworklistadoasuntos').css("display", "");
    $('#menuWorkListaAsuntos').css("display", "");
}

function menuWorkListado() {
    console.log("menuWorkListado")
    $('#menuworkingresar').css("display", "none");
    $('#btnworklista').css("display", "none");
    $('#menuWorkLista').css("display", "");
    $('#btnworkingreso').css("display", "");


}

function menuWorkTareaAgregar() {
    console.log("menuWorkTareaAgregar")
    $('#menuworktareaingresar').css("display", "");
    $('#listadoworkareas').css("display", "none");
    $('#btnWorkTareaLista').css("display", "");
    $('#btnWorkTareaIngreso').css("display", "none");
    fecha("#fechaworktarea");
}


function menuWorkReunionAgregar() {
    console.log("menuWorkReunionAgregar")
    $('#menuworkreunioningresar').css("display", "");
    $('#listadoworkreuniones').css("display", "none");
    $('#btnWorkReunionlista').css("display", "");
    $('#btnWorkReunioningreso').css("display", "none");
    fecha_actual("#fechaworkreunion");
}

function menuWorkHistoriaAgregar() {
    console.log("menuWorkHistoriaAgregar")
    $('#menuworkHistoriaingresar').css("display", "");
    $('#listadoworkHistoria').css("display", "none");
    $('#btnWorkHistoriaIngreso').css("display", "none");
    $('#btnWorkHistoriaLista').css("display", "")
    fecha("#fechaworkhistoria");
}

function menuWorkListadoTarea() {
    console.log("menuWorkListadoTarea")
    $('#listadoworkareas').css("display", "");
    $('#btnWorkTareaIngreso').css("display", "");
    $('#btnWorkTareaArchivar').css("display", "");
    $('#btnWorkTareaLista').css("display", "none")
    $('#listadoworktareasarchivada').css("display", "none");
    $('#menuworktareaingresar').css("display", "none");
    $('#menuworktareaeditar').css("display", "none");

}

//LLENAR COMBOBOX

function comboboxabogadoworkmenu() {
    var option = '';
    $.getJSON('/attorneys.json',
        function(data) {
            var tr;
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                option += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
            }
            $('#comboabogadoworkmenu').append(option);

        });
}


function comboboxabogadowork() {
    var option = '';
    $.getJSON('/attorneys.json',
        function(data) {
            var tr;
            var j = 0;


            for (var i = 0; i < data.length; i++) {
                option += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
            }
            $('#comboabogadowork').append(option);

        });

}

function menuWorkListadoTareaArchivadas() {
    console.log("menuWorkListadoTarea")
    $('#listadoworkareas').css("display", "none");
    $('#btnWorkTareaIngreso').css("display", "");
    $('#btnWorkTareaArchivar').css("display", "none");
    $('#btnWorkTareaLista').css("display", "")
    $('#listadoworktareasarchivada').css("display", "");
    $('#menuworktareaingresar').css("display", "none");
}

function menuWorkListadoReunion() {
    console.log("menuWorkListadoReunion")
    $('#listadoworkreuniones').css("display", "");
    $('#btnWorkReunioningreso').css("display", "");
    $('#btnWorkReunionarchivar').css("display", "");
    $('#btnWorkReunionlista').css("display", "none")
    $('#listadoworkreunionesarchivada').css("display", "none");
    $('#menuworkreunioningresar').css("display", "none");
    $('#menuworkreunioneditar').css("display", "none");

}

function menuWorkListadoReunionArchivadas() {
    console.log("menuWorkListadoReunionArchivadas")

    $('#listadoworkreuniones').css("display", "none");
    $('#btnWorkReunioningreso').css("display", "");
    $('#btnWorkReunionarchivar').css("display", "none");
    $('#btnWorkReunionlista').css("display", "")
    $('#listadoworkreunionesarchivada').css("display", "");
    $('#menuworkreunioningresar').css("display", "none");

}

function menuWorkListadoHistoria() {
    console.log("menuWorkListadoHistoria")
    $('#listadoworkHistoria').css("display", "");
    $('#btnWorkHistoriaIngreso').css("display", "");
    $('#btnWorkHistoriaLista').css("display", "none")
    $('#listadoworkHistoriaarchivada').css("display", "none");
    $('#menuworkHistoriaingresar').css("display", "none");
    $('#menuworkHistoriaEditar').css("display", "none");

}


//AGREGAR
function agregarWork() {

    var nombrework = $("#nombrework").val();;
    var num_codigo = $("#nuevocodigogestion").val();
    var code = "G";
    var codigowork = code + num_codigo;
    var cliente = $("#comboclientework").val();
    var abogado = $("#comboabogadowork").val();

    if (nombrework != "") {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: '/works',
            data: {
                work: {
                    code: codigowork,
                    name: nombrework,
                    client_id: cliente,
                    attorney_id: abogado,
                    active: true
                }
            },
            success: function(data, textStatus, jQxhr) {
                obtenerLoginWork();
                menuWorkListado();
                generarcodigoWork();
                limpiarNombrework();
                toastr.success('Se agrego una gestion')

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        });
    } else {
        toastr.warning('SE DEBE LLENAR LA GESTION');
        document.getElementById("menuworkingresar").style.border = "1px solid red";
    }
}

function agregarWorkTareas() {
    console.log("agregarWorkTareas")
    var descripcion = document.getElementById("descripcion-tarea-work").value;
    var id_gestion = $("#idgestion").val();;
    var id_abogado = $("#id_Abogado_work_tarea_ingresar").val();

    //fecha
    var datetimeval = $("#fechaworktarea").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    if (id_abogado != "") {
        if (datetimeval != "") {
            if (id_gestion != "") {
                if (descripcion != "") {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: '/work_statuses',
                        data: {
                            work_status: {
                                work_id: id_gestion,
                                attorney_id: id_abogado,
                                "expiration_date(3i)": dia,
                                "expiration_date(2i)": mes,
                                "expiration_date(1i)": ano,
                                "expiration_date(4i)": hh,
                                "expiration_date(5i)": mm,
                                finished: false,
                                description: descripcion
                            }
                        },
                        success: function(data, textStatus, jQxhr) {
                            actualizarGestionTRH(id_gestion);
                            menuWorkListadoTarea();
                            document.getElementById("descripcion-tarea-work").value = "";

                            toastr.success('Se agrego tarea')
                        },
                        error: function(jqXhr, textStatus, errorThrown) {
                            console.log("errorThrown");
                        }
                    });
                } else {
                    toastr.warning('SE DEBE LLENAR LA TAREA');
                    document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
                }
            } else {
                toastr.warning('DEBE SELECCIONAR UNA GESTION');
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UN ABOGADO');
    }
}

function agregarReuniones() {
    console.log("agregarWorkTareas")
    var descripcion = document.getElementById("descripcion-reunion-work").value;
    var id_gestion = $("#idgestion").val();;
    var id_abogado = $("#id_Abogado_work_reunion_ingresar").val();

    //fecha
    var datetimeval = $("#fechaworkreunion").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    if (id_abogado != "") {
        if (datetimeval != "") {
            if (id_gestion != "") {
                if (descripcion != "") {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: '/meetings',
                        data: {
                            meeting: {
                                work_id: id_gestion,
                                attorney_id: id_abogado,
                                "date(3i)": dia,
                                "date(2i)": mes,
                                "date(1i)": ano,
                                "date(4i)": hh,
                                "date(5i)": mm,
                                finished: false,
                                description: descripcion
                            }
                        },
                        success: function(data, textStatus, jQxhr) {
                            actualizarGestionTRH(id_gestion);
                            menuWorkListadoReunion();
                            document.getElementById("descripcion-reunion-work").value = "";

                            toastr.success('SE AGREGO REUNION')
                        },
                        error: function(jqXhr, textStatus, errorThrown) {
                            console.log("errorThrown");
                        }
                    });
                } else {
                    toastr.warning('SE DEBE LLENAR LA DESCRIPCION');
                    document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
                }
            } else {
                toastr.warning('DEBE SELECCIONAR UNA GESTION');
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UN ABOGADO');
    }
}

function agregarHistorias() {
    console.log("agregarHistorias")
    var descripcion = document.getElementById("descripcion-historia-work").value;
    var id_gestion = $("#idgestion").val();;
    var id_abogado = $("#id_Abogado_work_historia_ingresar").val();

    //fecha
    var datetimeval = $("#fechaworkhistoria").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    if (id_abogado != "") {
        if (datetimeval != "") {
            if (id_gestion != "") {
                if (descripcion != "") {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: '/work_histories',
                        data: {
                            work_history: {
                                work_id: id_gestion,
                                attorney_id: id_abogado,
                                "date(3i)": dia,
                                "date(2i)": mes,
                                "date(1i)": ano,
                                "date(4i)": hh,
                                "date(5i)": mm,
                                description: descripcion
                            }
                        },
                        success: function(data, textStatus, jQxhr) {
                            actualizarGestionTRH(id_gestion);
                            menuWorkListadoHistoria();
                            document.getElementById("descripcion-historia-work").value = "";

                            toastr.success('SE AGREGO COMENTARIO')
                        },
                        error: function(jqXhr, textStatus, errorThrown) {
                            console.log("errorThrown");
                        }
                    });
                } else {
                    toastr.warning('SE DEBE LLENAR LA DESCRIPCION');
                    document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
                }
            } else {
                toastr.warning('DEBE SELECCIONAR UNA GESTION');
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UN ABOGADO');
    }
}

//generar codigo

function generarcodigoWork() {
    $.getJSON('/works.json',
        function(data) {

            if (data.length != 0) {
                //Crear cdigo nuevo
                var largo = data.length - 1
                var code = data[largo].code;
                var largo_codigo = code.length;
                var letra = code.substring(0, 1);
                var numero = code.substring(1, largo_codigo);
                var codigo_nuevo_g = null;
                if (numero == "") {
                    numero = 1;
                } else {
                    var codigo_nuevo_g = parseInt(numero) + 1;
                }
                $("#nuevocodigogestion").val(codigo_nuevo_g);
            }
        })
}

//MOSTRAR GESTIONES

function listadoWorkActivas(login, user) {
    console.log("listadoWorkActivas");
    var login = login;
    var user = user;

    $.getJSON('/works.json',
        function(data) {

            if (data.length != 0) {
                //Crear cdigo nuevo
                var largo = data.length - 1
                var code = data[largo].code;
                var largo_codigo = code.length;
                var letra = code.substring(0, 1);
                var numero = code.substring(1, largo_codigo);
                var codigo_nuevo_g = null;


                if (numero == "") {
                    numero = 1;
                } else {
                    var codigo_nuevo_g = parseInt(numero) + 1;
                }
                $("#nuevocodigogestion").val(codigo_nuevo_g);

                if (login != "Embajador") {
                    var tr;
                    $("#table-work tr").remove();
                    var j = 0;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].active == true) {
                            tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosWork(" + data[i].id + "))'>");
                            tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                            tr.append("<td>" + data[i].code + "</td>");
                            tr.append("<td >" + data[i].name + "</td>");
                            $('#table-work').append(tr);
                            j = j + 1;
                        }
                    }
                } else {
                    console.log("Llenar datos Emabajador")
                    workmenulistadoasutons();
                    $.getJSON('/attorneys.json',
                        function(dataabogado) {
                            for (var i = 0; i < dataabogado.length; i++) {
                                if (dataabogado[i].user_id == user) {
                                    listadoWorkActivas_Embajador(dataabogado[i].id)
                                }
                            }
                        });
                }

            } else {
                $("#nuevocodigogestion").val("1");
            }
        });

}

function listadoWorkActivas_Embajador(id_abogado) {
    console.log("listadoWorkActivas_Embajador")

    console.log(id_abogado)
    var id_abogado = id_abogado;
    var work = null;
    $.getJSON('/work_abogado.json',
        function(data) {
            console.log(data)
            var tr;
            $("#table-work tr").remove();
            $("#table-work-asuntos tr").remove();


            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].active == true && data[i].attorney_id_w == id_abogado) {
                    if (work != data[i].id) {
                        work = data[i].id;
                        tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosWorkEncargado(" + data[i].id + "))'>");
                        tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td>" + data[i].code + "</td>");
                        tr.append("<td >" + data[i].name + "</td>");
                        $('#table-work').append(tr);
                        j = j + 1;
                    }

                }
            }

            for (var i = 0; i < data.length; i++) {
                if (data[i].active == true && data[i].attorney_id == id_abogado || data[i].id_attorney_me == id_abogado || data[i].id_attorney_id_h == id_abogado) {
                    if (work != data[i].id) {
                        work = data[i].id;
                        tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosWork(" + data[i].id + "))'>");
                        tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td>" + data[i].code + "</td>");
                        tr.append("<td >" + data[i].name + "</td>");
                        $('#table-work-asuntos').append(tr);
                        j = j + 1;
                    }

                }
            }
        });
}

function listadoWorkArchivadas(login, user) {
    console.log("listadoWorkArchivadas");
    $.getJSON('/works.json',
        function(data) {
            if (login != "Embajador") {
                var tr;
                $("#table-work-archivadas tr").remove();
                var j = 0;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].active == false) {
                        tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosWork(" + data[i].id + "))'>");
                        tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td>" + data[i].code + "</td>");
                        tr.append("<td >" + data[i].name + "</td>");
                        $('#table-work-archivadas').append(tr);
                        j = j + 1;
                    }
                }
            } else {
                console.log("Llenar datos Emabajador Archivados")

                $.getJSON('/attorneys.json',
                    function(dataabogado) {
                        for (var i = 0; i < dataabogado.length; i++) {
                            if (dataabogado[i].user_id == user) {
                                listadoWorkArchivadas_Embajador(dataabogado[i].id)
                            }
                        }
                    });
            }


        });
}

function listadoWorkArchivadas_Embajador(id_abogado) {
    console.log("listadoWorkArchivadas_Embajador")
    console.log(id_abogado)
    var id_abogado = id_abogado;
    var work = null;
    $.getJSON('/work_abogado.json',
        function(data) {
            //llenar abogado a cargo de causas
            console.log(data)
            var tr;
            $("#table-work-archivadas tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].active == false) {
                    if (data[i].attorney_id == id_abogado || data[i].id_attorney_me == id_abogado || data[i].id_attorney_id_h == id_abogado) {
                        if (work != data[i].id) {
                            work = data[i].id;
                            tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosWork(" + data[i].id + "))'>");
                            tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                            tr.append("<td>" + data[i].code + "</td>");
                            tr.append("<td >" + data[i].name + "</td>");
                            $('#table-work-archivadas').append(tr);
                            j = j + 1;
                        }
                    }
                }
            }
        });
}

//SELECCIONAR GESTIONES

function seleccionDatosWork(id_work) {
    console.log("seleccionDatosWork");
    var id = id_work;
    $("#idgestion").val(id);
    console.log($("#idgestion").val())

    $('#menuCausas').css("display", "none");
    $('#menuGestiones').css("display", "");


    $.getJSON('/works.json',
        function(data) {

            for (var i = 0; i < data.length; i++) {
                if (data[i].id == id) {
                    console.log("datos works")
                    console.log(data);

                    $("#work-descripcion").val(data[i].name);
                    $.getJSON('/clients/' + data[i].client_id + '.json',
                        function(dataclient) {
                            debugger
                            $("#comboclientework").val(dataclient.id);
                            $("#comboclientework2").val(dataclient.id);
                            $('#clientework').text(dataclient.name);
                            $('#clientework2').text(dataclient.name);
                        });

                    $('#codigowork').text(data[i].code);
                    $('#codigowork2').text(data[i].code);
                    $.getJSON('/attorneys/' + data[i].attorney_id + '.json',
                        function(dataabogado) {
                            $("#comboabogadoworkmenu").val(dataabogado.id);
                        });

                }
            }
        });

    $.getJSON('/login.json',
        function(data_login) {
            var perfil = data_login.role;
            var user = data_login.id;

            if (perfil != "Embajador") {
                $.getJSON('/meetings.json',
                    function(data) {
                        var tr;
                        $("#table-work-reuniones tr").remove();
                        $("#table-work-tareas-archivadas tr").remove();
                        var j = 0;
                        console.log("llenar Reuniones")
                        for (var i = 0; i < data.length; i++) {
                            if (data[i][0] == id & data[i][1] == false) {
                                tr = $("<tr/ >");
                                tr.append("<td width='20'><i style='cursor:pointer;' onclick='(ArchivarReuniones_listado(" + data[i][5] + "))'  class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data[i][4] + "</td>");
                                tr.append("<td><i onclick='(EliminarReunion(" + data[i][5] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarWorkReunion(" + data[i][5] + "))'  class='far fa-edit'></i></td>")
                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img id='imagenreunionw" + data[i][5] + "' src=" + data[i][7] + " alt='Avatar' class='avatar'></button><div id='abogados_work_reunion" + j + "' class='dropdown-content'></div></div></td>");
                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][2].replace('.000Z', '') + "'></td>");
                                $('#table-work-reuniones').append(tr);
                                llenarAbogadoReunion_Work(j, data[i][5])
                                j = j + 1;
                            }

                        }


                    });

                $.getJSON('/meetings.json',
                    function(data_m) {
                        var tr;
                        $("#table-work-reuniones-archivadas tr").remove();
                        var j = 0;

                        console.log("llenar Reuniones2")
                        console.log(data_m);
                        console.log(id)
                        for (var i = 0; i < data_m.length; i++) {
                            if (data_m[i][0] == id & data_m[i][1] == true) {
                                tr = $("<tr/ >");
                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data_m[i][4] + "</td>");
                                tr.append("<td><i onclick='(EliminarReunion(" + data_m[i][5] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i onclick='(DesarchivarReuniones(" + data_m[i][5] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                                tr.append("<td><img src='" + data_m[i][7] + "' alt='Avatar' class='avatar'></td")
                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data_m[i][2].replace('.000Z', '') + "'></td>");
                                $('#table-work-reuniones-archivadas').append(tr);
                                j = j + 1;
                            }

                        }
                    });

                $.getJSON('/work_statuses.json',
                    function(data2) {
                        var tr;
                        $("#table-work-tareas tr").remove();

                        var j = 0;
                        for (var i = 0; i < data2.length; i++) {
                            if (data2[i][0] == id & data2[i][1] == false) {


                                tr = $("<tr/ >");
                                tr.append("<td width='20'><i style='cursor:pointer;' onclick='(ArchivarWorkTareas_listado(" + data2[i][6] + "))'  class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data2[i][5] + "</td>");
                                tr.append("<td><i onclick='(EliminarTarea(" + data2[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarWorkTarea(" + data2[i][6] + "))'  class='far fa-edit'></i></td>")
                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img id='imagentarealw" + data2[i][6] + "' src=" + data2[i][7] + " alt='Avatar' class='avatar'></button><div id='abogados_work_status" + j + "' class='dropdown-content'></div></div></td>");
                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][2].substr(0, data2[i][2].length - 14) + "'></td>");
                                $('#table-work-tareas').append(tr);

                                llenarAbogadoTarea_Work(j, data2[i][6])
                                j = j + 1;
                            }
                        }

                    });

                //Archivados
                $.getJSON('/work_statuses.json',
                    function(data2) {
                        var tr;

                        $("#table-work-tareas-archivadas tr").remove();
                        var j = 0;
                        for (var i = 0; i < data2.length; i++) {
                            if (data2[i][0] == id & data2[i][1] == true) {
                                tr = $("<tr/>");
                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data2[i][5] + "</td>");
                                tr.append("<td><i onclick='(EliminarTarea(" + data2[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i onclick='(DesarchivarWorkTareas(" + data2[i][6] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                                tr.append("<td><img src='" + data2[i][7] + "' alt='Avatar' class='avatar'></td")
                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][2].substr(0, data2[i][2].length - 14) + "'></td>");
                                $('#table-work-tareas-archivadas').append(tr);
                                j = j + 1;
                            }


                        }

                    });

                $.getJSON('/work_histories.json',
                    function(data_histories) {
                        var tr;

                        $("#table-work-historias tr").remove();
                        var j = 0;
                        for (var i = 0; i < data_histories.length; i++) {
                            if (data_histories[i][0] == id) {
                                tr = $("<tr/ >");
                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data_histories[i][3] + "</td>");
                                tr.append("<td><i onclick='(EliminarHistoria(" + data_histories[i][4] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i id='edit' onclick='(seleccionarWorkHistoria(" + data_histories[i][4] + "))'  class='far fa-edit'></i></td>")
                                tr.append("<td><img src='" + data_histories[i][5] + "' alt='Avatar' class='avatar'></td")
                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data_histories[i][1].substr(0, data_histories[i][1].length - 14) + "'></td>");
                                $('#table-work-historias').append(tr);
                                j = j + 1;
                            }


                        }

                    });
            } else {
                $.getJSON('/attorneys.json',
                    function(dataabogado) {
                        for (var i = 0; i < dataabogado.length; i++) {
                            if (dataabogado[i].user_id == user) {
                                var abogado = dataabogado[i].id;

                                $.getJSON('/meetings.json',
                                    function(data) {
                                        var tr;


                                        $("#table-work-reuniones tr").remove();
                                        $("#table-work-tareas-archivadas tr").remove();
                                        var j = 0;
                                        console.log("llenar Reuniones")
                                        for (var i = 0; i < data.length; i++) {
                                            if (data[i][0] == id & data[i][1] == false & data[i][3] == abogado) {
                                                tr = $("<tr/ >");
                                                tr.append("<td width='20'><i style='cursor:pointer;' onclick='(ArchivarReuniones_listado(" + data[i][5] + "))'  class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data[i][4] + "</td>");
                                                tr.append("<td><i onclick='(EliminarReunion(" + data[i][5] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarWorkReunion(" + data[i][5] + "))'  class='far fa-edit'></i></td>")
                                                tr.append("<td><img src='" + data[i][7] + "' alt='Avatar' class='avatar'></td")
                                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][2].replace('.000Z', '') + "'></td>");
                                                $('#table-work-reuniones').append(tr);
                                                j = j + 1;
                                            }

                                        }


                                    });

                                $.getJSON('/meetings.json',
                                    function(data_m) {
                                        var tr;


                                        $("#table-work-reuniones-archivadas tr").remove();
                                        var j = 0;

                                        console.log("llenar Reuniones2")

                                        for (var i = 0; i < data_m.length; i++) {
                                            if (data_m[i][0] == id & data_m[i][1] == true & data[i][3] == abogado) {
                                                tr = $("<tr/ >");
                                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data_m[i][4] + "</td>");
                                                tr.append("<td><i onclick='(EliminarReunion(" + data_m[i][5] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                                tr.append("<td><i onclick='(DesarchivarReuniones(" + data_m[i][5] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                                                tr.append("<td><img src='" + data[i][7] + "' alt='Avatar' class='avatar'></td")
                                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data_m[i][2].replace('.000Z', '') + "'></td>");
                                                $('#table-work-reuniones-archivadas').append(tr);
                                                j = j + 1;
                                            }

                                        }
                                    });

                                $.getJSON('/work_statuses.json',
                                    function(data2) {
                                        var tr;

                                        $("#table-work-tareas tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data2.length; i++) {
                                            if (data2[i][0] == id & data2[i][1] == false & data2[i][3] == abogado) {
                                                tr = $("<tr/ >");
                                                tr.append("<td width='20'><i style='cursor:pointer;' onclick='(ArchivarWorkTareas_listado(" + data2[i][6] + "))'  class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data2[i][5] + "</td>");
                                                tr.append("<td><i onclick='(EliminarTarea(" + data2[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarWorkTarea(" + data2[i][6] + "))'  class='far fa-edit'></i></td>")
                                                tr.append("<td><img src='" + data2[i][7] + "' alt='Avatar' class='avatar'></td")
                                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][2].substr(0, data2[i][2].length - 14) + "'></td>");
                                                $('#table-work-tareas').append(tr);
                                                j = j + 1;
                                            }
                                        }

                                    });

                                //Archivados
                                $.getJSON('/work_statuses.json',
                                    function(data2) {
                                        var tr;

                                        $("#table-work-tareas-archivadas tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data2.length; i++) {
                                            if (data2[i][0] == id & data2[i][1] == true & data2[i][3] == abogado) {
                                                tr = $("<tr/>");
                                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data2[i][5] + "</td>");
                                                tr.append("<td><i onclick='(EliminarTarea(" + data2[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                                tr.append("<td><i onclick='(DesarchivarWorkTareas(" + data2[i][6] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                                                tr.append("<td><img src='" + data2[i][7] + "' alt='Avatar' class='avatar'></td")
                                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][2].substr(0, data2[i][2].length - 14) + "'></td>");
                                                $('#table-work-tareas-archivadas').append(tr);
                                                j = j + 1;
                                            }


                                        }

                                    });

                                $.getJSON('/work_histories.json',
                                    function(data_histories) {
                                        var tr;

                                        $("#table-work-historias tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data_histories.length; i++) {
                                            if (data_histories[i][0] == id & data_histories[i][2] == abogado) {
                                                tr = $("<tr/ >");
                                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data_histories[i][3] + "</td>");
                                                tr.append("<td><i onclick='(EliminarHistoria(" + data_histories[i][4] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarWorkHistoria(" + data_histories[i][4] + "))'  class='far fa-edit'></i></td>")
                                                tr.append("<td><img src='" + data_histories[i][5] + "' alt='Avatar' class='avatar'></td")
                                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data_histories[i][1].substr(0, data_histories[i][1].length - 14) + "'></td>");
                                                $('#table-work-historias').append(tr);
                                                j = j + 1;
                                            }

                                        }

                                    });

                            }
                        }
                    });
            }
        });

}

//SELECCION EDICION

function menuWorkTareaEditar() {
    console.log("menuWorkTareaEditar")
    $('#menuworktareaeditar').css("display", "");
    $('#menuworktareaingresar').css("display", "none");
    $('#listadoworkareas').css("display", "none");
    $('#btnWorkTareaLista').css("display", "");
    $('#btnWorkTareaIngreso').css("display", "none");
}

function seleccionarWorkTarea(id_tarea) {
    var id_tarea = id_tarea;
    menuWorkTareaEditar();

    $.getJSON('/work_statuses.json',
        function(data_tarea) {
            for (var i = 0; i < data_tarea.length; i++) {
                if (data_tarea[i][6] == id_tarea) {
                    $("#descripcion-tarea-work-editar").val(data_tarea[i][5]);
                    $("#fechaworktareaeditar").val(data_tarea[i][2].substring(0, 10));
                    $("#id_tarea").val(data_tarea[i][6]);
                    $('#imagenworktareaeditar').attr('src', data_tarea[i][7]);
                    $('#id_Abogado_work_tarea_editar').val(data_tarea[i][3]);

                }
            }
        });

}


function menuWorkReunionEditar() {
    console.log("menuWorkReunionEditar")
    $('#menuworkreunioneditar').css("display", "");
    $('#menuworkreunioningresar').css("display", "none");
    $('#listadoworkreuniones').css("display", "none");
    $('#btnWorkReunionlista').css("display", "");
    $('#btnWorkReunioningreso').css("display", "none");
}

function seleccionarWorkReunion(id_reunion) {
    var id_reunion = id_reunion;
    menuWorkReunionEditar();

    $.getJSON('/meetings.json',
        function(data_reunion) {
            for (var i = 0; i < data_reunion.length; i++) {
                if (data_reunion[i][5] == id_reunion) {
                    $("#descripcion-reunion-work-editar").val(data_reunion[i][4]);
                    $("#fechaworkreunioneditar").val(data_reunion[i][2].replace('.000Z', ''));
                    $("#id_reunion").val(data_reunion[i][5]);

                    $('#imagenworkreunioneditar').attr('src', data_reunion[i][7]);
                    $('#id_Abogado_work_reunion_editar').val(data_reunion[i][3]);

                }
            }
        });

}


function menuWorkHistoriaEditar() {
    console.log("menuWorkHistoriaEditar")
    $('#menuworkHistoriaEditar').css("display", "");
    $('#menuworkHistoriaingresar').css("display", "none");
    $('#listadoworkHistoria').css("display", "none");
    $('#btnWorkHistoriaIngreso').css("display", "none");
    $('#btnWorkHistoriaLista').css("display", "")
}

function seleccionarWorkHistoria(id_historia) {
    var id_historia = id_historia;
    menuWorkHistoriaEditar();

    $.getJSON('/work_histories.json',
        function(data_historia) {
            for (var i = 0; i < data_historia.length; i++) {
                if (data_historia[i][4] == id_historia) {
                    $("#descripcion-historia-work-editar").val(data_historia[i][3]);
                    $("#fechaworkhistoriaeditar").val(data_historia[i][1].substring(0, 10));
                    $("#id_historia").val(data_historia[i][4]);
                    $('#imagenworkhistoriaeditar').attr('src', data_historia[i][5]);
                    $('#id_Abogado_work_historia_editar').val(data_historia[i][2]);

                }
            }
        });

}

//GUARDAR MODIFICACION

function modificarWork() {
    var nombrework = $("#work-descripcion").val();;

    var cliente = $("#comboclientework2").val();
    var abogado = $("#comboabogadoworkmenu").val();
    var estado = $("#Estado").val();
    var id_gestion = $("#idgestion").val();;

    if (nombrework != "") {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'PUT',
            url: '/works/' + id_gestion,
            data: {
                work: {
                    name: nombrework,
                    client_id: cliente,
                    attorney_id: abogado,
                    active: true,
                    tipo: estado
                }
            },
            success: function(data, textStatus, jQxhr) {
                obtenerLoginWork();
                toastr.success('Se MODIFICO UNA GESTION')

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        });
    } else {
        toastr.warning('SE DEBE LLENAR LA GESTION');
        document.getElementById("menuworkingresar").style.border = "1px solid red";
    }
}

function modificarWorkTareas() {
    console.log("modificarWorkTareas")
    var descripcion = document.getElementById("descripcion-tarea-work-editar").value;
    var id_tarea = $("#id_tarea").val();;
    var id_abogado = $('#id_Abogado_work_tarea_editar').val();;
    var id_gestion = $("#idgestion").val();;


    //fecha
    var datetimeval = $("#fechaworktareaeditar").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    if (datetimeval != "") {
        if (id_tarea != "") {
            if (descripcion != "") {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'PUT',
                    url: '/work_statuses/' + id_tarea,
                    data: {
                        work_status: {
                            attorney_id: id_abogado,
                            "expiration_date(3i)": dia,
                            "expiration_date(2i)": mes,
                            "expiration_date(1i)": ano,
                            "expiration_date(4i)": hh,
                            "expiration_date(5i)": mm,
                            finished: false,
                            description: descripcion
                        }
                    },
                    success: function(data, textStatus, jQxhr) {
                        actualizarGestionTRH(id_gestion);
                        menuWorkListadoTarea();

                        toastr.success('SE MODIFICO LA TAREA')
                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("errorThrown");
                    }
                });
            } else {
                toastr.warning('SE DEBE LLENAR LA TAREA');
                document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA TAREA');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
    }
}

function modificarReuniones() {
    console.log("modificarReuniones")
    var descripcion = document.getElementById("descripcion-reunion-work-editar").value;
    var id_reunion = $("#id_reunion").val();;
    var id_abogado = $('#id_Abogado_work_reunion_editar').val();
    var id_gestion = $("#idgestion").val();


    //fecha
    var datetimeval = $("#fechaworkreunioneditar").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    if (datetimeval != "") {
        if (id_reunion != "") {
            if (descripcion != "") {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'PUT',
                    url: '/meetings/' + id_reunion,
                    data: {
                        meeting: {
                            attorney_id: id_abogado,
                            "date(3i)": dia,
                            "date(2i)": mes,
                            "date(1i)": ano,
                            "date(4i)": hh,
                            "date(5i)": mm,
                            finished: false,
                            description: descripcion
                        }
                    },
                    success: function(data, textStatus, jQxhr) {
                        actualizarGestionTRH(id_gestion);
                        menuWorkListadoReunion();
                        toastr.success('SE MODIFICO REUNION')
                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("errorThrown");
                    }
                });
            } else {
                toastr.warning('SE DEBE LLENAR LA DESCRIPCION');
                document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA GESTION');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
    }
}

function modificarHistorias() {
    console.log("modificarHistorias")
    var descripcion = document.getElementById("descripcion-historia-work-editar").value;
    var id_historia = $("#id_historia").val();;
    var id_abogado = $('#id_Abogado_work_historia_editar').val();;
    var id_gestion = $("#idgestion").val();;


    //fecha
    var datetimeval = $("#fechaworkhistoriaeditar").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    if (datetimeval != "") {
        if (id_historia != "") {
            if (descripcion != "") {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'PUT',
                    url: '/work_histories/' + id_historia,
                    data: {
                        work_history: {
                            attorney_id: id_abogado,
                            "date(3i)": dia,
                            "date(2i)": mes,
                            "date(1i)": ano,
                            "date(4i)": hh,
                            "date(5i)": mm,
                            description: descripcion
                        }
                    },
                    success: function(data, textStatus, jQxhr) {
                        actualizarGestionTRH(id_gestion);
                        menuWorkListadoHistoria();

                        toastr.success('SE MODIFICO COMENTARIO')
                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("errorThrown");
                    }
                });
            } else {
                toastr.warning('SE DEBE LLENAR LA DESCRIPCION');
                document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA GESTION');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
    }
}

function modificarHistorias() {
    console.log("modificarHistorias")
    var descripcion = document.getElementById("descripcion-historia-work-editar").value;
    var id_historia = $("#id_historia").val();;
    var id_abogado = $('#id_Abogado_work_historia_editar').val();;


    //fecha
    var datetimeval = $("#fechaworkhistoriaeditar").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    if (datetimeval != "") {
        if (id_historia != "") {
            if (descripcion != "") {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'PUT',
                    url: '/work_histories/' + id_historia,
                    data: {
                        work_history: {
                            attorney_id: id_abogado,
                            "date(3i)": dia,
                            "date(2i)": mes,
                            "date(1i)": ano,
                            "date(4i)": hh,
                            "date(5i)": mm,
                            description: descripcion
                        }
                    },
                    success: function(data, textStatus, jQxhr) {
                        menuWorkListadoHistoria();
                        toastr.success('SE MODIFICO COMENTARIO')
                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("errorThrown");
                    }
                });
            } else {
                toastr.warning('SE DEBE LLENAR LA DESCRIPCION');
                document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA GESTION');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
    }
}

//ELIMINAR

function EliminarDatosGestion() {
    var id_gestion = $("#idgestion").val();;
    console.log("Eliminar Datos Gestion")
        //Revisar si tiene tareas y eliminarlas

    $.getJSON('/work_statuses.json',
        function(data2) {
            for (var i = 0; i < data2.length; i++) {
                if (data2[i][0] == id_gestion) {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        async: false,
                        method: 'DELETE',
                        url: '/work_statuses/' + data2[i][6],

                    });

                }
            }
        });
    //Revisar si tiene reuniones y eliminarlas
    $.getJSON('/meetings.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {

                if (data[i][0] == id_gestion) {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        async: false,
                        method: 'DELETE',
                        url: '/meetings/' + data[i][5],

                    });
                }
            }
        });
    //Revisar si tiene comentarios y eliminarlas.
    $.getJSON('/work_histories.json',
        function(data_histories) {
            for (var i = 0; i < data_histories.length; i++) {

                if (data_histories[i][0] == id_gestion) {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        async: false,
                        method: 'DELETE',
                        url: '/work_histories/' + data_histories[i][4],
                    });
                }

            }

        });

    EliminarGestion();

}

function EliminarGestion() {

    var id_gestion = $("#idgestion").val();;
    console.log("EliminarGestion");
    //Eliminar Gestion
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'DELETE',
        url: '/works/' + id_gestion,
    });

    toastr.error('SE ELIMINO LA GESTION');
    $("#modal_eliminar_work").modal('hide');
    obtenerLoginWork();
    LimpiarGestion();

}


function EliminarTarea(id_tarea) {
    var id_tarea = id_tarea;
    var id_gestion = $("#idgestion").val();;

    var opcion = confirm("Seguro de eliminar");
    if (opcion == true) {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'DELETE',
            url: '/work_statuses/' + id_tarea,

        });
        toastr.error('SE ELIMINO LA TAREA');
        obtenerLogin();
        actualizarGestionTRH(id_gestion);
    }
}

function EliminarReunion(id_reunion) {
    var id_reunion = id_reunion;
    var id_gestion = $("#idgestion").val();;

    var opcion = confirm("Seguro de eliminar");
    if (opcion == true) {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'DELETE',
            url: '/meetings/' + id_reunion,

        });
        toastr.error('SE ELIMINO LA REUNION');
        obtenerLogin();
        actualizarGestionTRH(id_gestion);

    }

}

function EliminarHistoria(id_historia) {
    var id_historia = id_historia;
    var opcion = confirm("Seguro de eliminar");
    if (opcion == true) {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'DELETE',
            url: '/work_histories/' + id_historia,

        });
        var id_gestion = $("#idgestion").val();;
        actualizarGestionTRH(id_gestion);
        toastr.error('SE ELIMINO EL COMENTARIO');
    }

}

//ARCHIVAR

function ArchivarWork() {
    var id_gestion = $("#idgestion").val();;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        async: true,
        url: '/works/' + id_gestion,
        data: {
            work: {
                active: false
            }
        },
        success: function(data, textStatus, jQxhr) {

            obtenerLoginWork();
            LimpiarGestion();
            toastr.warning('SE ARCHIVO GESTION')
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });

}

function ArchivarWorkTareas() {
    console.log("ArchivarWorkTareas")
    var id_tarea = $("#id_tarea").val();;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        async: true,
        url: '/work_statuses/' + id_tarea,
        data: {
            work_status: {
                finished: true
            }
        },
        success: function(data) {
            var id_gestion = $("#idgestion").val();;
            actualizarGestionTRH(id_gestion);
            toastr.warning('SE ARCHIVO LA TAREA')
        }
    });

}

function ArchivarReuniones() {
    console.log("ArchivarReuniones")
    var id_reunion = $("#id_reunion").val();;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/meetings/' + id_reunion,
        data: {
            meeting: {
                finished: true
            }
        },
        success: function(data, textStatus, jQxhr) {
            var id_gestion = $("#idgestion").val();;
            actualizarGestionTRH(id_gestion);
            toastr.warning('SE ARCHIVO REUNION')
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });

}

function ArchivarWorkTareas_listado(id_tarea) {
    console.log("ArchivarWorkTareas_listado")
    var id_tarea = id_tarea;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        async: true,
        url: '/work_statuses/' + id_tarea,
        data: {
            work_status: {
                finished: true
            }
        },
        success: function() {
            var id_gestion = $("#idgestion").val();;
            ListadoTareasVintes();
            actualizarGestionTRH(id_gestion);
            toastr.warning('SE ARCHIVO LA TAREA')
        }
    });

}

function ArchivarReuniones_listado(id_reunion) {
    console.log("ArchivarReuniones_listado")
    var id_reunion = id_reunion;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        async: true,
        url: '/meetings/' + id_reunion,
        data: {
            meeting: {
                finished: true
            }
        },
        success: function(data, textStatus, jQxhr) {
            var id_gestion = $("#idgestion").val();;
            actualizarGestionTRH(id_gestion);
            ListadoAudienciasVigentes();
            toastr.warning('SE ARCHIVO REUNION')
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });

}

//DESARCHIVAR


function DesarchivarWorkTareas(id_tarea) {
    console.log("ArchivarWorkTareas")
    var id_tarea = id_tarea;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/work_statuses/' + id_tarea,
        data: {
            work_status: {
                finished: false
            }
        },
        success: function(data, textStatus, jQxhr) {
            var id_gestion = $("#idgestion").val();;
            actualizarGestionTRH(id_gestion);
            toastr.success('SE DESARCHIVO LA TAREA')
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });

}

function DesarchivarReuniones(id_reunion) {
    console.log("ArchivarReuniones")
    var id_reunion = id_reunion;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/meetings/' + id_reunion,
        data: {
            meeting: {
                finished: false
            }
        },
        success: function(data, textStatus, jQxhr) {
            var id_gestion = $("#idgestion").val();;
            actualizarGestionTRH(id_gestion);
            toastr.success('SE DESARCHIVO REUNION')
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });

}

//FILTRO

function filtroWorkActivas() {
    console.log("filtro listadoCausasActivas");
    var Estado = $("#estado").val();
    $.getJSON('/works.json',
        function(data) {
            var tr;
            $("#table-work tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].active == true) {
                    if (Estado == "Seleccionar...") {
                        tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosWork(" + data[i].id + "))'>");
                        tr.append("<td style='width:25%'><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td style='width:40%'>" + data[i].code + "</td>");
                        tr.append("<td >" + data[i].name + "</td>");
                        $('#table-work').append(tr);
                        j = j + 1;
                    } else {
                        if (Estado == data[i].tipo) {
                            tr = $("<tr/style='cursor:pointer;' onclick='(seleccionDatosWork(" + data[i].id + "))'>");
                            tr.append("<td style='width:25%'><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                            tr.append("<td style='width:40%'>" + data[i].code + "</td>");
                            tr.append("<td >" + data[i].name + "</td>");
                            $('#table-work').append(tr);
                            j = j + 1;
                        }
                    }

                }
            }
        });
}

//LLENAR ABOGADO

function llenarAbogadoGestionTareaNueva() {
    var a = "";
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoNuevoGestion_T(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('#abogados_work_status_nuevo a').remove();
            $('#abogados_work_status_nuevo').append(a);
        });


}

function llenarAbogadoGestionTareaEditar() {
    var a = "";
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoEditarGestion_T(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('#abogados_work_status_editar a').remove();
            $('#abogados_work_status_editar').append(a);


        });
}

function llenarAbogadoGestionReunionNuevo() {
    var a = "";
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoNuevoGestion_R(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('#abogados_work_reunion_nuevo a').remove();
            $('#abogados_work_reunion_nuevo').append(a);

        });
}

function llenarAbogadoGestionReunionEditar() {
    var a = "";
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoEditarGestion_R(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('#abogados_work_reunion_editar a').remove();
            $('#abogados_work_reunion_editar').append(a);

        });
}

function llenarAbogadoGestionHistoriaNuevo() {
    var a = "";
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoNuevoGestion_H(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('#abogados_work_historia_nuevo a').remove();
            $('#abogados_work_historia_nuevo').append(a);

        });
}

function llenarAbogadoGestionHistoriaEditar() {
    var a = "";
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoEditarGestion_H(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('#abogados_woaabogados_work_historia_editarbogados_work_historia_editarrk_historia_nuevo a').remove();
            $('#abogados_work_historia_editar').append(a);

        });
}


//CAMBIAR ABOGADO

//NUEVO
function cambiarAbogadoNuevoGestion_T(id) {
    console.log("cambiarAbogadoNuevoGestion_T");
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenworktareaingresar').attr('src', dataabogado.photo);
            $('#id_Abogado_work_tarea_ingresar').val(dataabogado.id);
        });
}

function cambiarAbogadoNuevoGestion_R(id) {
    console.log("cambiarAbogadoNuevoGestion_A");
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenworkreunioningresar').attr('src', dataabogado.photo);
            $('#id_Abogado_work_reunion_ingresar').val(dataabogado.id);
        });
}

function cambiarAbogadoNuevoGestion_H(id) {
    console.log("cambiarAbogadoNuevoGestion_H");
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {

            $('#imagenworkhistoriaingresar').attr('src', dataabogado.photo);
            $('#id_Abogado_work_historia_ingresar').val(dataabogado.id);

        });
}

//EDITAR
function cambiarAbogadoEditarGestion_T(id) {
    console.log("cambiarAbogadoEditar_T");
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenworktareaeditar').attr('src', dataabogado.photo);
            $('#id_Abogado_work_tarea_editar').val(dataabogado.id);
        });
}

function cambiarAbogadoEditarGestion_R(id) {
    console.log("cambiarAbogadoEditar_A");
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenworkreunioneditar').attr('src', dataabogado.photo);
            $('#id_Abogado_work_reunion_editar').val(dataabogado.id);
        });
}

function cambiarAbogadoEditarGestion_H(id) {
    console.log("cambiarAbogadoEditar_H");
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenworkhistoriaeditar').attr('src', dataabogado.photo);
            $('#id_Abogado_work_historia_editar').val(dataabogado.id);
        });
}


function llenarAbogadoTarea_Work(id, id_t) {

    var a = "";
    var id_tarea = id_t;

    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick='cambiarAbogado_L_T_W(" + id_tarea + "," + data[i].id + ")'><img  src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
                debugger
            }
            $('#abogados_work_status' + id + ' a').remove();
            $('#abogados_work_status' + id).append(a);

        });
}

function cambiarAbogado_L_T_W(id_tarea, id_abogad) {
    $.getJSON('/attorneys/' + id_abogad + '.json',
        function(dataabogado) {
            $('#imagentarealw' + id_tarea).attr('src', dataabogado.photo);
            ModificarAbogadoWorkTarea(dataabogado.id, id_tarea)
        });
}

function llenarAbogadoReunion_Work(id, id_reunion) {
    var a = "";
    var id_reu = id_reunion;
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;' onclick='cambiarAbogado_L_R_W(" + id_reu + "," + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('abogados_work_reunion' + id + ' a').remove();
            $('#abogados_work_reunion' + id).append(a);

        });
}

function cambiarAbogado_L_R_W(id_reu, id_abo) {

    $.getJSON('/attorneys/' + id_abo + '.json',
        function(dataabogado) {
            $('#imagenreunionw' + id_reu).attr('src', dataabogado.photo);
            ModificarAbogadoWorkReunion(dataabogado.id, id_reu);
        });

}

function ModificarAbogadoWorkTarea(id_abogado, id_tarea) {

    var id_abogado = id_abogado;
    var id_work_status = id_tarea;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/work_statuses/' + id_work_status,
        data: {
            work_status: {
                attorney_id: id_abogado
            }
        },
        success: function(data, textStatus, jQxhr) {
            toastr.success('SE MODIFICO EL ABOGADO')
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });
}

function ModificarAbogadoWorkReunion(id_abogado, id_reu) {

    var id_abogado = id_abogado;
    var id_reunion = id_reu;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/meetings/' + id_reunion,
        data: {
            meeting: {
                attorney_id: id_abogado
            }
        },
        success: function(data, textStatus, jQxhr) {
            toastr.success('SE MODIFICO ABOGADO')
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });

}

function actualizarGestionTRH(id) {
    console.log("actualizarGestionTRH");
    id_work = id;
    console.log(id_work)
    seleccionDatosWork(id_work)
}

function limpiarNombrework() {
    document.getElementById("nombrework").value = "";
}

function seleccionDatosWorkEncargado(id_work) {
    var id = id_work;
    $("#idgestion").val(id);
    console.log($("#idgestion").val())

    $('#menuCausas').css("display", "none");
    $('#menuGestiones').css("display", "");
    $.getJSON('/works.json',
        function(data) {

            for (var i = 0; i < data.length; i++) {
                if (data[i].id == id) {
                    console.log("datos works")
                    console.log(data);

                    $("#work-descripcion").val(data[i].name);
                    $.getJSON('/clients/' + data[i].client_id + '.json',
                        function(dataclient) {
                            debugger
                            $("#comboclientework").val(dataclient.id);
                            $("#comboclientework2").val(dataclient.id);
                            $('#clientework').text(dataclient.name);
                            $('#clientework2').text(dataclient.name);
                        });

                    $('#codigowork').text(data[i].code);
                    $('#codigowork2').text(data[i].code);
                    $.getJSON('/attorneys/' + data[i].attorney_id + '.json',
                        function(dataabogado) {
                            $("#comboabogadoworkmenu").val(dataabogado.id);
                        });

                }
            }
        });

    $.getJSON('/meetings.json',
        function(data) {
            var tr;
            $("#table-work-reuniones tr").remove();
            $("#table-work-tareas-archivadas tr").remove();
            var j = 0;
            console.log("llenar Reuniones")
            for (var i = 0; i < data.length; i++) {
                if (data[i][0] == id & data[i][1] == false) {
                    tr = $("<tr/ >");
                    tr.append("<td width='20'><i style='cursor:pointer;' onclick='(ArchivarReuniones_listado(" + data[i][5] + "))'  class='far fa-square'></i></td>")
                    tr.append("<td width='680'>" + data[i][4] + "</td>");
                    tr.append("<td><i onclick='(EliminarReunion(" + data[i][5] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                    tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarWorkReunion(" + data[i][5] + "))'  class='far fa-edit'></i></td>")
                    tr.append("<td> <div class='dropdown'><button class='dropbtn'><img id='imagenreunionw" + data[i][5] + "' src=" + data[i][7] + " alt='Avatar' class='avatar'></button><div id='abogados_work_reunion" + j + "' class='dropdown-content'></div></div></td>");
                    tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][2].replace('.000Z', '') + "'></td>");
                    $('#table-work-reuniones').append(tr);
                    llenarAbogadoReunion_Work(j, data[i][5])
                    j = j + 1;
                }

            }


        });

    $.getJSON('/meetings.json',
        function(data_m) {
            var tr;
            $("#table-work-reuniones-archivadas tr").remove();
            var j = 0;

            console.log("llenar Reuniones2")
            console.log(data_m);
            console.log(id)
            for (var i = 0; i < data_m.length; i++) {
                if (data_m[i][0] == id & data_m[i][1] == true) {
                    tr = $("<tr/ >");
                    // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                    tr.append("<td width='680'>" + data_m[i][4] + "</td>");
                    tr.append("<td><i onclick='(EliminarReunion(" + data_m[i][5] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                    tr.append("<td><i onclick='(DesarchivarReuniones(" + data_m[i][5] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                    tr.append("<td><img src='" + data_m[i][7] + "' alt='Avatar' class='avatar'></td")
                    tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data_m[i][2].replace('.000Z', '') + "'></td>");
                    $('#table-work-reuniones-archivadas').append(tr);
                    j = j + 1;
                }

            }
        });

    $.getJSON('/work_statuses.json',
        function(data2) {
            var tr;
            $("#table-work-tareas tr").remove();
            var j = 0;
            for (var i = 0; i < data2.length; i++) {
                if (data2[i][0] == id & data2[i][1] == false) {


                    tr = $("<tr/ >");
                    tr.append("<td width='20'><i style='cursor:pointer;' onclick='(ArchivarWorkTareas_listado(" + data2[i][6] + "))'  class='far fa-square'></i></td>")
                    tr.append("<td width='680'>" + data2[i][5] + "</td>");
                    tr.append("<td><i onclick='(EliminarTarea(" + data2[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                    tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarWorkTarea(" + data2[i][6] + "))'  class='far fa-edit'></i></td>")
                    tr.append("<td> <div class='dropdown'><button class='dropbtn'><img id='imagentarealw" + data2[i][6] + "' src=" + data2[i][7] + " alt='Avatar' class='avatar'></button><div id='abogados_work_status" + j + "' class='dropdown-content'></div></div></td>");
                    tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][2].substr(0, data2[i][2].length - 14) + "'></td>");
                    $('#table-work-tareas').append(tr);

                    llenarAbogadoTarea_Work(j, data2[i][6])
                    j = j + 1;
                }
            }

        });

    //Archivados
    $.getJSON('/work_statuses.json',
        function(data2) {
            var tr;
            $("#table-work-tareas-archivadas tr").remove();
            var j = 0;
            for (var i = 0; i < data2.length; i++) {
                if (data2[i][0] == id & data2[i][1] == true) {
                    tr = $("<tr/>");
                    // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                    tr.append("<td width='680'>" + data2[i][5] + "</td>");
                    tr.append("<td><i onclick='(EliminarTarea(" + data2[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                    tr.append("<td><i onclick='(DesarchivarWorkTareas(" + data2[i][6] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                    tr.append("<td><img src='" + data2[i][7] + "' alt='Avatar' class='avatar'></td")
                    tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][2].substr(0, data2[i][2].length - 14) + "'></td>");
                    $('#table-work-tareas-archivadas').append(tr);
                    j = j + 1;
                }


            }

        });

    $.getJSON('/work_histories.json',
        function(data_histories) {
            var tr;

            $("#table-work-historias tr").remove();
            var j = 0;
            for (var i = 0; i < data_histories.length; i++) {
                if (data_histories[i][0] == id) {
                    tr = $("<tr/ >");
                    // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                    tr.append("<td width='680'>" + data_histories[i][3] + "</td>");
                    tr.append("<td><i onclick='(EliminarHistoria(" + data_histories[i][4] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                    tr.append("<td><i id='edit' onclick='(seleccionarWorkHistoria(" + data_histories[i][4] + "))'  class='far fa-edit'></i></td>")
                    tr.append("<td><img src='" + data_histories[i][5] + "' alt='Avatar' class='avatar'></td")
                    tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechaaudiencia' name='fechaaudiencia' value='" + data_histories[i][1].substr(0, data_histories[i][1].length - 14) + "'></td>");
                    $('#table-work-historias').append(tr);
                    j = j + 1;
                }


            }

        });
}

function LimpiarGestion() {
    $('#codigowork').text("");
    $('#codigowork2').text("");
    $('#clientework').text("");
    $('#clientework2').text("");
    $("#work-descripcion").val("");
    $("#table-work-reuniones tr").remove();
    $("#table-work-tareas-archivadas tr").remove();
    $("#table-work-tareas tr").remove();
    $("#table-work-reuniones-archivadas tr").remove();
    $("#table-work-tareas tr").remove();
    $("#table-work-tareas-archivadas tr").remove();
    $("#table-work-historias tr").remove();

    $("#table-work-reuniones tr").remove();
    $("#table-work-tareas-archivadas tr").remove();

    $("#table-work-reuniones-archivadas tr").remove();
    $("#table-work-tareas tr").remove();
    $("#table-work-tareas-archivadas tr").remove();
    $("#table-work-historias tr").remove();
}