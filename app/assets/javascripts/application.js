// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require admin/jquery-jvectormap-1.2.2.min.js
//= require admin/jquery.dataTables.min.js
//= require admin/jquery.dataTables.js
//= require admin/jquery-jvectormap-world-mill-en.js
//= require admin/jquery-ui.js
//= require admin/jquery.js
//= require admin/jquery.knob.min.js
//= require admin/jquery.slimscroll.js
//= require admin/jquery.sparkline.js
//= require popper
//= require bootstrap
//= require_tree .
//= require admin/adminlte.js
//= require admin/bootstrap-datepicker.js
//= require admin/bootstrap.js
//= require admin/bootstrap3-wysihtml5.all.js
//= require admin/daterangepicker.js
//= require admin/fastclick.js
//= require admin/moment.min.js
//= require admin/morris.js
//= require admin/raphael.js
//= require toastr
//= require bootstrap-modal
//= require bootstrap-modalmanager
//= require user_web.js



toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}