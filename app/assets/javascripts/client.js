//Llenar Audiencia y Causa desde el id_cause
function BuscarCliente(id_Cliente) {
    console.log("BuscarCliente");
    var x = id_Cliente;
    console.log(x);
    $.getJSON('/clients/' + x + '.json',
        function(data) {

            //   document.getElementById("idcliente").value = data.client_id;
            document.getElementById("idname").value = data.name;
            document.getElementById("idrut").value = data.rut;
            document.getElementById("idplan").value = data.plan;
            document.getElementById("idregion").value = data.region;
            document.getElementById("idcomuna").value = data.commune;
            document.getElementById("idtipo1").value = data.kind;
            document.getElementById("idtipo").value = data.kind;
            document.getElementById("idnamelegal").value = data.name_legal;
            document.getElementById("idrutlegal").value = data.rut_legal;
            document.getElementById("idmaillegal").value = data.email_legal;
            document.getElementById("idphonelegal").value = data.phone_legal;


        });

}

function BuscarCliente_abogado(id_Cliente) {
    console.log("BuscarCliente");
    var x = id_Cliente;
    console.log(x);
    $.getJSON('/clients/' + x + '.json',
        function(data) {

            //   document.getElementById("idcliente").value = data.client_id;
            document.getElementById("idname").value = data.name;
            document.getElementById("idrut").value = data.rut;
            document.getElementById("idplan").value = data.plan;
            document.getElementById("idregion").value = data.region;
            document.getElementById("idcomuna").value = data.commune;
            document.getElementById("idtipo1").value = data.kind;
            document.getElementById("idtipo").value = data.kind;
            document.getElementById("idnamelegal").value = data.name_legal;
            document.getElementById("idrutlegal").value = data.rut_legal;
            document.getElementById("idmaillegal").value = data.email_legal;
            document.getElementById("idphonelegal").value = data.phone_legal;


        });

}
$(document).ready(function() {
    $('#table-dashboard tbody-dashboard td').click(function() {
        console.log("click table");
    });

});

function BuscargestionD(id_works) {
    console.log("buscar gestion")
    var x = id_works;

    $.getJSON('/works/' + x + '.json',
        function(data) {


            $.getJSON('/clients/' + data.client_id + '.json',
                function(data) {
                    document.getElementById("idcliented").value = data.id;
                    document.getElementById("idnombrecliented").value = data.name;
                });
            document.getElementById("idnombred").value = data.name;
            document.getElementById("idmateriad").value = data.materia;
            document.getElementById("idtipod").value = data.tipo;

            $.getJSON('/attorneys/' + data.attorney_id + '.json',
                function(data) {
                    document.getElementById("idabogadod").value = data.id;
                    document.getElementById("idnombreabogadod").value = data.name;
                });


            document.getElementById("idtaread").value = data.id;
            document.getElementById("idcodigod").value = data.code;
            document.getElementById("idaread").value = data.area;
        });
    $('#causas').css("display", "none");
    $('#gestion').css("display", "");
}

function BuscarCausaD(id_causa) {
    console.log("buscarcausa");

    var x = id_causa;

    $.getJSON('/causes/' + x + '.json',
        function(data) {


            $.getJSON('/clients/' + data.client_id + '.json',
                function(data) {
                    document.getElementById("idclientem").value = data.id;
                    document.getElementById("idnombreclientem").value = data.name;
                });

            document.getElementById("idnombrem").value = data.name;
            document.getElementById("rolm").value = data.role;
            document.getElementById("Caratuladom").value = data.caratulado;
            document.getElementById("Cuantiam").value = data.amount;
            document.getElementById("Tribunalm").value = data.court;
            document.getElementById("Materiam").value = data.matter;
            document.getElementById("idprocedimientom").value = data.process;

            $.getJSON('/attorneys/' + data.attorney_id + '.json',
                function(data) {
                    document.getElementById("idabogadom").value = data.id;
                    document.getElementById("idnombreabogadom").value = data.name;
                });

            document.getElementById("idcausam").value = x;
            document.getElementById("idcodigom").value = data.code;
            document.getElementById("iddescripcionm").value = data.description;

        });
    $('#gestion').css("display", "none");
    $('#causas').css("display", "");
}



function BuscarCausa(id_causa) {
    var x = id_causa;
    console.log(x, "buscarcausa")
    $.getJSON('/causes/' + x + '.json',
        function(data) {



            $.getJSON('/clients/' + data.client_id + '.json',
                function(data) {
                    document.getElementById("idclientec").value = data.id;
                    document.getElementById("idnombreclientec").value = data.name;
                });
            document.getElementById("idnombrec").value = data.name;
            document.getElementById("rolc").value = data.role;
            document.getElementById("Caratuladoc").value = data.caratulado;
            document.getElementById("Cuantiac").value = data.amount;
            document.getElementById("Tribunalc").value = data.court;
            document.getElementById("Materiac").value = data.matter;
            document.getElementById("idprocedimientoc").value = data.process;

            $.getJSON('/attorneys/' + data.attorney_id + '.json',
                function(data) {
                    document.getElementById("idabogadoc").value = data.id;
                    document.getElementById("idnombreabogadoc").value = data.name;
                });


            document.getElementById("idcausac").value = x;
            document.getElementById("idcodigoc").value = data.code;
            document.getElementById("iddescripcionc").value = data.descripcion;

        });

}


///Llenar Cliente y Abogado de Causas y Gestion
function comboclientes() {
    console.log("llenar combobox cliente");

    var option = '';

    $.getJSON('/clients.json',
        function(data) {
            var tr;
            var j = 0;

            for (var i = 0; i < data.length; i++) {
                option += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
            }
            $('#combocliente').append(option);
            $('#combocliente2').append(option);
            $('#combocliente3').append(option);
        });



}

function comboboxabogado() {
    console.log("llenar combobox abogado");
    var option = '';
    $.getJSON('/attorneys.json',
        function(data) {
            var tr;
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                option += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
            }
            $('#comboabogado').append(option);
            $('#comboabogado2').append(option);
            $('#comboabogado_clientec').append(option);
            $('#comboabogado_clientem').append(option);

        });
}

//Revisar su uso
function comboclienteswork() {


    var option = '';

    $.getJSON('/clients.json',
        function(data) {
            var tr;
            var j = 0;


            for (var i = 0; i < data.length; i++) {
                option += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
            }
            $('#comboclientework').append(option);
            $('#comboclientework2').append(option);

        });



}



///NUEVAS FUNCIONES


//////Calender
function llenartablacalender() {

    $.getJSON('/audiences.json',
        function(data) {
            var tr;
            $("#calenderaudiences tr").remove();
            $("#calenderaudiencesarchivadas tr").remove();
            var j = 0;
            console.log("llenar Audiences")

            for (var i = 0; i < data.length; i++) {
                if (data[i][8] == false) {
                    tr = $('<tr/>');
                    tr = $("<tr/ onclick='(seleccionDatosCausa(" + data[i][3] + "))'>");
                    tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                    tr.append("<td>" + data[i][9] + "</td>");
                    tr.append("<td>" + data[i][0].replace('T', ' | ').replace('.000Z', '') + "</td>");
                    j = j + 1;
                    $('#calenderaudiences').append(tr);
                    j = j + 1;
                } else {
                    tr = $('<tr/>');
                    tr = $("<tr/ onclick='(seleccionDatosCausa(" + data[i][3] + "))'>");
                    tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                    tr.append("<td>" + data[i][9] + "</td>");
                    tr.append("<td>" + data[i][0].replace('T', ' | ').replace('.000Z', '') + "</td>");
                    j = j + 1;
                    $('#calenderaudiencesarchivadas').append(tr);
                    j = j + 1;
                }

            }


        });

    $.getJSON('/meetings.json',
        function(data) {
            var tr;

            var j = 0;
            console.log("llenar Audiences")

            for (var i = 0; i < data.length; i++) {
                if (data[i][1] == false) {
                    tr = $('<tr/>');
                    tr = $("<tr/ onclick='(seleccionDatosWork(" + data[i][0] + "))'>");
                    tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                    tr.append("<td>" + data[i][6] + "</td>");
                    tr.append("<td>" + data[i][2].replace('T', ' | ').replace('.000Z', '') + "</td>");
                    j = j + 1;
                    $('#calenderaudiences').append(tr);
                    j = j + 1;
                } else {
                    tr = $('<tr/>');
                    tr = $("<tr/ onclick='(seleccionDatosWork(" + data[i][0] + "))'>");
                    tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                    tr.append("<td>" + data[i][6] + "</td>");
                    tr.append("<td>" + data[i][2].replace('T', ' | ').replace('.000Z', '') + "</td>");
                    j = j + 1;
                    $('#calenderaudiencesarchivadas').append(tr);

                }

            }


        });

}

//Llenar listado de causas activas
function listadoCausasActivasClientes() {
    console.log("listadoCausasActivasClientes");

    $.getJSON('/causes.json',
        function(data) {
            var tr;
            $("#table-cause-cliente tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].archived == false) {
                    tr = $("<tr/ onclick='(seleccionCliente(" + data[i].client_id + "))'>");
                    tr.append("<td ><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                    tr.append("<td>" + data[i].code + "</td>");
                    $.ajax({
                        url: "/clients/" + data[i].client_id,
                        dataType: 'json',
                        async: false,
                        success: function(data) {
                            tr.append("<td >" + data.name + "</td>");
                        }
                    });
                    $('#table-cause-cliente').append(tr);
                    j = j + 1;
                }
            }



        });
}


function listadoWorkActivasClientes() {
    console.log("listadoWorkActivasClientes");
    $.getJSON('/works.json',
        function(data) {
            var tr;
            $("#table-work-cliente tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].active == true) {
                    tr = $("<tr/ onclick='(seleccionCliente(" + data[i].client_id + "))'>");
                    tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                    tr.append("<td>" + data[i].code + "</td>");
                    $.ajax({
                        url: "/clients/" + data[i].client_id,
                        dataType: 'json',
                        async: false,
                        success: function(data) {
                            tr.append("<td >" + data.name + "</td>");
                        }
                    });

                    $('#table-work-cliente').append(tr);
                    j = j + 1;
                }
            }
        });
}

function seleccionCliente_abogado(id_cliente) {
    console.log("seleccionCliente");
    var id = id_cliente;
    $.getJSON('/clients/' + id + '.json',
        function(data) {
            $('#nombrec_abogado').text(data.name);
            $('#rutc_abogado').text(data.rut);
            $('#planc_abogado').text(data.plan);
            $('#oficioc_abogado').text(data.kind);
            $('#regionc_abogado').text(data.region);
            $('#comunac_abogado').text(data.commune);

            $('#direccionc_abogado').text(data.address);
            $('#estadoc_abogado').text(data.marital_status);
            $('#representantec_abogado').text(data.name_legal);
            $('#rutlegalc_abogado').text(data.rut_legal);

            if (data.option == "Persona Natural") {
                $('#menu-rl').css("display", "none");
                $('#menu-rutlegal').css("display", "none");
            }
            if (data.option == "Empresa") {
                $('#menu-rl').css("display", "");
                $('#menu-rutlegal').css("display", "");
            }

        });
}


function obtenerLogin_cliente() {
    $.getJSON('/login.json',
        function(data) {
            var perfil = data.role;
            var user_id = data.id;

            if (perfil == "admin") {
                ListadoClientes("admin", 1);
            } else {
                $.getJSON('/attorneys.json',
                    function(dataabogado) {
                        for (var i = 0; i < dataabogado.length; i++) {
                            if (dataabogado[i].user_id == user_id) {
                                ListadoClientes(perfil, dataabogado[i].id);
                            }
                        }
                    });
            }

        });
}

function ListadoClientes(perfil, abogado_id) {
    var login = perfil;
    var abogado = abogado_id;

    $.getJSON('/clients.json',
        function(data) {
            if (data.length != 0) {
                if (login == "Embajador") {
                    var tr;
                    $("#table-cliente-abogado tr").remove();
                    var j = 0;
                    for (var i = 0; i < data.length; i++) {
                        if (abogado == data[i].attorney_id) {
                            tr = $("<a><tr/ style='cursor:pointer;' onclick='(seleccionCliente_abogado(" + data[i].id + "))'></a>");
                            tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                            tr.append("<td>" + data[i].name + "</td>");
                            $('#table-cliente-abogado').append(tr);
                            j = j + 1;
                        }
                    }
                } else {

                    var tr;
                    $("#table-cliente-abogado tr").remove();
                    var j = 0;
                    for (var i = 0; i < data.length; i++) {
                        tr = $("<a><tr/ style='cursor:pointer;' onclick='(seleccionCliente_abogado(" + data[i].id + "))'></a>");
                        tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td>" + data[i].name + "</td>");
                        $('#table-cliente-abogado').append(tr);
                        j = j + 1;
                    }
                }
            }
        });
}