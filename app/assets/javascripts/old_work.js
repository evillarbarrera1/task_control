//Llenar Sub Tareas
function llenarsubtareas(id_works) {
    var x = id_works;
    $.getJSON('/works/' + x + '.json',
        function(data) {

            $("#comboclientework").val(data.client_id);
            $("#comboabogadowork").val(data.attorney_id);
            document.getElementById("idnombre").value = data.name;
            document.getElementById("idmateria").value = data.materia;
            document.getElementById("idtipo").value = data.tipo;
            document.getElementById("idtarea").value = data.id;
            document.getElementById("idcodigo").value = data.code;
            document.getElementById("idarea").value = data.area;


        });


    $.getJSON('/work_statuses.json',
        function(data) {

            var tr;
            $("#table-workstatus tr").remove();
            var j = 0;
            console.log("llenar estado tareas")
            for (var i = 0; i < data.length; i++) {
                if (data[i][0] == x) {

                    tr = $('<tr/>');

                    if (data[i][1] == false) {
                        tr.append("<td><input class='btn btn-warning' value='' onclick='CambiarEstadoSubTareas(" + j + ")' type='button'></td>")
                    } else {
                        tr.append("<td><input class='btn btn-success' value='' onclick='CambiarEstadoSubTareas(" + j + ")' type='button'></td>")
                    }

                    tr.append("<td><input id='fechatarea" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][2].replace('.000Z', '') + "'></td>");
                    tr.append("<td>" + data[i][5] + "</td>");
                    tr.append("<td><i class='btn btn-primary' data-toggle='modal' data-target='#ModalAbogado' onclick='modalabogado(true," + j + ",3)'>" + data[i][4] + "</i> </td>");


                    $('#table-workstatus').append(tr);
                    j = j + 1;
                }
            }
        });
}

//Actualizar datos de tarea
function ActualizarTareas() {
    console.log("Actualizar Tareas")

    var id_tarea = document.getElementById("idtarea").value;
    var nombre = document.getElementById("idnombre").value;
    var area = document.getElementById("idarea").value;
    var tipo = document.getElementById("idtipo").value;
    var abogado = $("#comboabogadowork").val();
    var id_cliente = $("#comboclientework").val();
    var materia = document.getElementById("idmateria").value;
    var codigo = document.getElementById("idcodigo").value;


    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/works/' + id_tarea,
        data: {
            work: {
                name: nombre,
                area: area,
                client_id: id_cliente,
                tipo: tipo,
                materia: materia,
                attorney_id: abogado,
                code: codigo



            }
        }
    });


}

//Archivar datos de tarea
function ArchivarTarea() {
    console.log("Archivar Tareas")

    var id_tarea = document.getElementById("idtarea").value;
    var nombre = document.getElementById("idnombre").value;
    var area = document.getElementById("idarea").value;
    var tipo = document.getElementById("idtipo").value;
    var abogado = document.getElementById("idabogado").value;
    var id_cliente = document.getElementById("idcliente").value;
    var materia = document.getElementById("idmateria").value;
    var codigo = document.getElementById("idcodigo").value;


    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/works/' + id_tarea,
        data: {
            work: {
                active: 1,
                name: nombre,
                area: area,
                client_id: id_cliente,
                tipo: tipo,
                materia: materia,
                attorney_id: abogado,
                code: codigo



            }
        }
    });


}

//Eliminar Tareas
function IniciarEliminacionTareas() {
    $("#table-work tr td").click(EliminarTareas);
    console.log("eliminar tareas 1")
}

function EliminarTareas() {
    $("#table-work tr td")
    var id_tarea = $(this).parents("tr").find("td").eq(0).html();
    console.log("eliminar tareas")

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'DELETE',
        url: '/works/' + id_tarea,

    });
}

//Actualiar Sub Tareas
function ActualizarSubTareas(x, id_abogado) {
    console.log("ActualizarSubTareas")


    var fila = x;
    var table = document.getElementById("table-workstatus");
    var tablerows = table.rows;
    var terminado = tablerows[fila].cells[7].textContent;

    var abogadoseleccionado = id_abogado
    var id_abogado = 0
    if (abogadoseleccionado == 0) {
        id_abogado = tablerows[fila].cells[2].textContent;
    } else {
        id_abogado = abogadoseleccionado;
    }


    var descripcion = tablerows[fila].cells[4].textContent;
    var id = tablerows[fila].cells[5].textContent;
    var id_work = tablerows[fila].cells[6].textContent;


    //fechas
    var datetimeval = $("#fechatarea" + x).val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/work_statuses/' + id,
        data: {
            work_status: {
                "expiration_date(3i)": dia,
                "expiration_date(2i)": mes,
                "expiration_date(1i)": ano,
                "expiration_date(4i)": hh,
                "expiration_date(5i)": mm,
                attorney_id: id_abogado,
                work_id: id_work,
                finished: terminado,
                description: descripcion
            }
        }
    });
    ActualizarTodoTarea(id_work)
}

//Eliminar Sub Tareas
function EliminarSubtareas(x) {
    console.log("EliminarASubTareas")


    var fila = x;
    var table = document.getElementById("table-workstatus");
    var tablerows = table.rows;

    var id = tablerows[fila].cells[5].textContent;
    var id_work = tablerows[fila].cells[6].textContent;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'DELETE',
        url: '/work_statuses/' + id,

    });
    ActualizarTodoTarea(id_work)
}

//Cambiar Estado de Sub Tareas
function CambiarEstadoSubTareas(x) {
    console.log("cambiar estado sub Tareas")
    var d = new Date();
    var fila = x;
    var table = document.getElementById("table-workstatus");

    var tablerows = table.rows;
    var estado = tablerows[fila].cells[7].textContent;
    var id_abogado = tablerows[fila].cells[2].textContent;
    var descripcion = tablerows[fila].cells[4].textContent;
    var id = tablerows[fila].cells[5].textContent;
    var id_work = tablerows[fila].cells[6].textContent;


    var opcion = ""

    if (estado == "true") {
        opcion = confirm("Desea dejarlo Pendiente?");
    } else {
        opcion = confirm("Ha Terminado?");
    }

    //fechas
    var datetimeval = $("#fechatarea" + x).val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    if (opcion == true) {
        if (estado == "true") {

            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'PUT',
                url: '/work_statuses/' + id,
                data: {
                    work_status: {
                        "expiration_date(3i)": dia,
                        "expiration_date(2i)": mes,
                        "expiration_date(1i)": ano,
                        "expiration_date(4i)": hh,
                        "expiration_date(5i)": mm,
                        attorney_id: id_abogado,
                        work_id: id_work,
                        finished: false,
                        description: descripcion
                    }
                }
            });

            ActualizarTodoTarea(id_work)
        } else {

            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'PUT',
                url: '/work_statuses/' + id,
                data: {
                    work_status: {
                        "expiration_date(3i)": dia,
                        "expiration_date(2i)": mes,
                        "expiration_date(1i)": ano,
                        "expiration_date(4i)": hh,
                        "expiration_date(5i)": mm,
                        attorney_id: id_abogado,
                        work_id: id_work,
                        finished: true,
                        description: descripcion
                    }
                }
            });

        }

        ActualizarTodoTarea(id_work)
    } else {
        console.log("NO")
    }
    ActualizarTodoTarea(id_work)
}

//Actualizar todo de tareas
function ActualizarTodoTarea(x) {
    $.getJSON('/work_statuses.json',
        function(data) {

            var tr;
            $("#table-workstatus tr").remove();
            var j = 0;
            console.log("llenar estado tareas")
            for (var i = 0; i < data.length; i++) {
                if (data[i][0] == x) {

                    tr = $('<tr/>');

                    if (data[i][1] == false) {
                        tr.append("<td width='150'><input class='btn btn-warning' value='Pendiente' onclick='CambiarEstadoSubTareas(" + j + ")' type='button'></td>")
                    } else {
                        tr.append("<td width='150'><input class='btn btn-success' value='Terminado' onclick='CambiarEstadoSubTareas(" + j + ")' type='button'></td>")
                    }

                    tr.append("<td width='150'><input id='fechatarea" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][2].replace('.000Z', '') + "'></td>");
                    tr.append("<td contenteditable='true' style='display:none;'>" + data[i][3] + "</td>");
                    tr.append("<td width='150'> <i class='btn btn-primary' data-toggle='modal' data-target='#ModalAbogado' onclick='modalabogado(true," + j + ",3)'>" + data[i][4] + "</i> </td>");
                    tr.append("<td width='150' contenteditable='true'>" + data[i][5] + "</td>");
                    tr.append("<td style='display:none;'>" + data[i][6] + "</td>");
                    tr.append("<td style='display:none;'>" + data[i][0] + "</td>");
                    tr.append("<td style='display:none;'>" + data[i][1] + "</td>");
                    tr.append("<td> <i class='material-icons btn' onclick='ActualizarSubTareas(" + j + ")'>autorenew</i> </td>");
                    tr.append("<td> <i class='material-icons btn' onclick='EliminarSubtareas(" + j + ")'>clear</i> </td>");
                    $('#table-workstatus').append(tr);
                    j = j + 1;
                }
            }
        });
}

//Menu de Tareas-Causas-Audiencias por Cliente
function llenartareascliente(x) {
    id_clientetarea = x;
    $("#clitareas tr td").click(llenardatos(id_clientetarea));


}

function llenardatos(id) {
    console.log(id)
    var cli_id = id;
    console.log("llenar tareas cliente .")
    $.getJSON('/work_json.json',
        function(data) {
            var tr;
            $("#clitareas tr td").remove();
            $("#cliestadocausa tr td").remove();
            $("#cliaudiencias tr td").remove();
            $("#clisubtareas tr td").remove();

            var j = 0;

            console.log("llenar tareas");
            for (var i = 0; i < data.length; i++) {
                console.log(cli_id)
                if (data[i][2] == cli_id) {
                    tr = $('<tr/>');
                    tr.append("<td  contenteditable='true' style='display:none'>" + data[i][0] + "</td>");
                    tr.append("<td ><i class='btn' onclick='LlenarEstadotareascliente(" + data[i][0] + ")'>" + data[i][3] + "</i></td>");
                    tr.append("<td contenteditable='true'><i class='btn' onclick='LlenarEstadotareascliente(" + data[i][0] + ")'>" + data[i][1] + "</i></td>");
                    tr.append("<td  contenteditable='true' style='display:none'>" + data[i][2] + "</td>");
                    tr.append("<td> <i class='material-icons btn' onclick='IniciarActualizacionTareasCliente()'>autorenew</i> </td>");
                    tr.append("<td> <i class='material-icons btn' onclick='EliminarSubtareas(" + j + ")'>clear</i> </td>");
                    $('#clitareas').append(tr);
                    j = j + 1;
                }
            }

        });


    $.getJSON('/causes.json',
        function(causescliente) {
            var tr;
            $("#clicausas tr td").remove();
            var j = 0;


            for (var i = 0; i < causescliente.length; i++) {
                console.log(cli_id)
                if (causescliente[i].client_id == cli_id) {
                    tr = $('<tr/>');
                    tr.append("<td contenteditable='true' style='display:none'>" + causescliente[i].id + "</td>");
                    tr.append("<td contenteditable='true'><i class='btn' onclick='LlenarEstadoaudienciascliente(" + causescliente[i].id + ")'>" + causescliente[i].name + "</i></td>");
                    tr.append("<td contenteditable='true'><i class='btn' onclick='LlenarEstadoaudienciascliente(" + causescliente[i].id + ")'>" + causescliente[i].court + "</i></td>");
                    tr.append("<td contenteditable='true'><i class='btn' onclick='LlenarEstadoaudienciascliente(" + causescliente[i].id + ")'>" + causescliente[i].code + "</i></td>");
                    tr.append("<td contenteditable='true'><i class='btn' onclick='LlenarEstadoaudienciascliente(" + causescliente[i].id + ")'>" + causescliente[i].description + "</i></td>");
                    tr.append("<td> <i class='material-icons btn' onclick='ActualizarCausasCliente()'>autorenew</i> </td>");
                    tr.append("<td> <i class='material-icons btn' onclick='EliminarSubtareas(" + j + ")'>clear</i> </td>");
                    $('#clicausas').append(tr);
                    j = j + 1;
                }
            }

        });


}

function href() {
    $(location).attr('href', "/client_works");
}

function llenarclientescombo() {
    console.log("llenarclientes")

    var option = '';

    $.getJSON('/clients.json',
        function(data) {
            var tr;
            var j = 0;


            for (var i = 0; i < data.length; i++) {
                option += '<option onclick=(llenartareascliente("' + data[i].id + '"))>' + data[i].name + '</option>';
            }
            $('#DropClientes').append(option);

        });



}

function LlenarEstadotareascliente(x) {
    console.log("llenar tablas estado tareas cliente")

    var id_work = x;
    $.getJSON('/work_statuses.json',
        function(data) {

            var tr;
            $("#clisubtareas tr").remove();
            var j = 0;
            console.log("llenar estado tareas")
            for (var i = 0; i < data.length; i++) {
                if (data[i][0] == id_work) {
                    if (data[i][0] == x) {

                        tr = $('<tr/>');

                        if (data[i][1] == false) {
                            tr.append("<td width='150'><input class='btn btn-warning' value='Pendiente' onclick='CambiarEstadoSubTareasCliente(" + j + ")' type='button'></td>")
                        } else {
                            tr.append("<td width='150'><input class='btn btn-success' value='Terminado' onclick='CambiarEstadoSubTareasCliente(" + j + ")' type='button'></td>")
                        }

                        tr.append("<td width='150'><input id='fechatareacliente" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][2].replace('.000Z', '') + "'></td>");
                        tr.append("<td contenteditable='true' style='display:none;'>" + data[i][3] + "</td>");
                        tr.append("<td width='150'> <i class='btn btn-primary' data-toggle='modal' data-target='#ModalAbogado' onclick='modalabogado(true," + j + ",4)'>" + data[i][4] + "</i> </td>");
                        tr.append("<td width='150' contenteditable='true'>" + data[i][5] + "</td>");
                        tr.append("<td style='display:none;'>" + data[i][6] + "</td>");
                        tr.append("<td style='display:none;'>" + data[i][0] + "</td>");
                        tr.append("<td style='display:none;'>" + data[i][1] + "</td>");
                        tr.append("<td> <i class='material-icons btn' onclick='ActualizarSubTareasCliente(" + j + ")'>autorenew</i> </td>");
                        tr.append("<td> <i class='material-icons btn' onclick='EliminarSubtareasCliente(" + j + ")'>clear</i> </td>");
                        $('#clisubtareas').append(tr);
                        j = j + 1;
                    }
                }
            }
        });
}

function LlenarEstadoaudienciascliente(x) {

    $.getJSON('/audiences.json',
        function(data) {
            var tr;
            $("#cliaudiencias tr").remove();
            var j = 0;
            console.log("llenar Audiences")

            for (var i = 0; i < data.length; i++) {
                if (data[i][3] == x) {

                    tr = $('<tr/>');
                    tr.append("<td><input id='fechaaudienciacliente" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][0].replace('.000Z', '') + "'></td>");
                    tr.append("<td style='display:none;'>" + data[i][1] + "</td>");
                    tr.append("<td> <i class='btn btn-primary' data-toggle='modal' data-target='#ModalAbogado' onclick='modalabogado(true," + j + ",5)'>" + data[i][2] + "</i> </td>");
                    tr.append("<td style='display:none;'>" + data[i][3] + "</td>");
                    tr.append("<td style='display:none;'>" + data[i][6] + "</td>");
                    tr.append("<td> <i class='material-icons btn' onclick='ActualizarAudienciaCliente(" + j + ")'>autorenew</i> </td>");
                    tr.append("<td> <i class='material-icons btn' onclick='EliminarAudienciaCliente(" + j + ")'>clear</i> </td>");
                    $('#cliaudiencias').append(tr);
                    j = j + 1;
                }
            }
            llenarAbogado();
        });


    $.getJSON('/cause_statuses.json',
        function(data2) {
            var tr;
            $("#cliestadocausa tr").remove();
            var j = 0;
            console.log("llenar Causas")
            for (var i = 0; i < data2.length; i++) {
                if (data2[i][6] == x) {


                    tr = $('<tr/>');
                    if (data2[i][5] == false) {
                        tr.append("<td  width='150'><input class='btn btn-warning' value='Pendiente' onclick='CambiarEstadoCausasEstadoCliente(" + j + ")' type='button'></td>")
                    } else {
                        tr.append("<td width='150'><input class='btn btn-success' value='Terminado' onclick='CambiarEstadoCausasEstadoCliente(" + j + ")' type='button'></td>")
                    }
                    tr.append("<td width='200' contenteditable='true'>" + data2[i][0] + "</td>");

                    tr.append("<td width='200'><input id='fechacausatareacliente" + j + "' type='datetime-local'  name='fechaaudiencia' value='" + data2[i][1].replace('.000Z', '') + "'></td>");
                    tr.append("<td  width='150'> <i class='btn btn-primary' data-toggle='modal' data-target='#ModalAbogado' onclick='modalabogado(true," + j + ",6)'>" + data2[i][3] + "</i> </td>");
                    tr.append("<td  style ='display:none;'>" + data2[i][4] + "</td>");
                    tr.append("<td  style ='display:none;'>" + data2[i][5] + "</td>");
                    tr.append("<td  style ='display:none;'>" + data2[i][6] + "</td>");
                    tr.append("<td  style ='display:none;'>" + data2[i][2] + "</td>");
                    tr.append("<td> <i class='material-icons btn' onclick='ActualizarCausaTareasCliente(" + j + "," + 0 + ")'>autorenew</i> </td>");
                    tr.append("<td> <i class='material-icons btn' onclick='EliminarCausaTareasCliente(" + j + ")'>clear</i> </td>");


                    $('#cliestadocausa').append(tr);
                    j = j + 1;
                }
            }

        });
}

//Actualizar Tareas por Cliente

function IniciarActualizacionTareasCliente() {
    $("#clitareas tr td").click(ActualizarTareasCliente);
}

function ActualizarTareasCliente() {
    console.log("Actualizar Tareas Cliente")

    var id_tarea = $(this).parents("tr").find("td").eq(0).html();
    var nombre = $(this).parents("tr").find("td").eq(1).html();
    var descripcion = $(this).parents("tr").find("td").eq(2).html();
    var id_cliente = $(this).parents("tr").find("td").eq(3).html();
    console.log(id_tarea)
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/works/' + id_tarea,
        data: {
            work: {
                name: nombre,
                description: descripcion,
                client_id: id_cliente
            }
        }
    });


}
//Eliminar Tareas por Cliente

//Acualizar Estado de Tareas por Cliente
function ActualizarSubTareasCliente(x, id_abogado) {
    console.log("ActualizarSubTareasCliente")


    var fila = x;
    var id_abogado = id_abogado;
    var table = document.getElementById("clisubtareas");
    var tablerows = table.rows;

    var terminado = tablerows[fila].cells[7].textContent;
    if (id_abogado != "") {
        id_abogado = id_abogado;
    } else {
        id_abogado = tablerows[fila].cells[2].textContent;
    }
    var descripcion = tablerows[fila].cells[4].textContent;
    var id = tablerows[fila].cells[5].textContent;
    var id_work = tablerows[fila].cells[6].textContent;

    //fechas
    var datetimeval = $("#fechatareacliente" + x).val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/work_statuses/' + id,
        data: {
            work_status: {
                "expiration_date(3i)": dia,
                "expiration_date(2i)": mes,
                "expiration_date(1i)": ano,
                "expiration_date(4i)": hh,
                "expiration_date(5i)": mm,
                attorney_id: id_abogado,
                work_id: id_work,
                finished: terminado,
                description: descripcion
            }
        }
    });
    ActualizarTodoTarea(id_work)
}

//Eliminar Estado de TAreas por Cliente
function EliminarSubtareasCliente(x) {
    console.log("EliminarASubTareasCliente")


    var fila = x;
    var table = document.getElementById("clisubtareas");
    var tablerows = table.rows;

    var id = tablerows[fila].cells[5].textContent;
    var id_work = tablerows[fila].cells[6].textContent;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'DELETE',
        url: '/work_statuses/' + id,

    });
    ActualizarTodoTarea(id_work)
}

//Actualiar Causas
function IniciarActualizacionCliente() {
    $("#cliestadocausa tr td").click(ActualizarCausasCliente);

}

function ActualizarCausasCliente() {
    console.log("actualizarcausascliente")
    var id_cause = $(this).parents("tr").find("td").eq(0).html();
    var nombre = $(this).parents("tr").find("td").eq(1).html();
    var tribunal = $(this).parents("tr").find("td").eq(2).html();
    var codigo = $(this).parents("tr").find("td").eq(3).html();
    var descripcion = $(this).parents("tr").find("td").eq(4).html();
    var id_abogado = $(this).parents("tr").find("td").eq(5).html();
    var id_cliente = $(this).parents("tr").find("td").eq(6).html();

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/causes/' + id_cause,
        data: {
            cause: {
                name: nombre,
                court: tribunal,
                code: codigo,
                description: descripcion,
                attorney_id: id_abogado,
                client_id: id_cliente

            }
        }
    });
}
//Eliminar Causas


//Actualizar datos de audiencia
function ActualizarAudienciaCliente(x, id_abogado) {
    console.log("ActualizarAudiencia")
    var fila = x;
    var abogadoseleccionado = id_abogado
    var table = document.getElementById("cliaudiencias");

    var tablerows = table.rows;
    var id_abogado = 0
    if (abogadoseleccionado == 0) {
        id_abogado = tablerows[fila].cells[1].textContent;
    } else {
        id_abogado = abogadoseleccionado;
    }
    console.log(id_abogado)

    var id = tablerows[fila].cells[4].textContent;
    var id_causa = tablerows[fila].cells[3].textContent;
    //fechas
    var datetimeval = $("#fechaaudienciacliente" + x).val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/audiences/' + id,
        data: {
            audience: {
                "date(3i)": dia,
                "date(2i)": mes,
                "date(1i)": ano,
                "date(4i)": hh,
                "date(5i)": mm,
                attorney_id: id_abogado,
                cause_id: id_causa
            }
        }

    });
    ActualizarTodoCliente(id_causa)


}

//Eliminar Audiencias por Cliente
function EliminarAudienciaCliente(x) {
    console.log("EliminarAudienciaCliente")


    var fila = x;
    var table = document.getElementById("cliaudiencias");
    var tablerows = table.rows;
    console.log(tablerows[fila].cells[4].textContent)
    var id = tablerows[fila].cells[4].textContent;


    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'DELETE',
        url: '/audiences/' + id,

    });

}

//Actualizar Estado Causas Cliente
function ActualizarCausaTareasCliente(x, id_abogado) {
    console.log("ActualizarCausaTareasCliente")
    var d = new Date();
    var abogadoseleccionado = id_abogado;
    var fila = x;
    var table = document.getElementById("cliestadocausa");
    var tablerows = table.rows;
    var id_abogado = 0;
    var datos_descripcion = tablerows[fila].cells[1].textContent;

    if (abogadoseleccionado != "") {
        id_abogado = abogadoseleccionado;

    } else {
        id_abogado = tablerows[fila].cells[7].value;

    }



    var id = tablerows[fila].cells[4].textContent;
    var estado = tablerows[fila].cells[5].textContent;

    var id_causa = tablerows[fila].cells[6].textContent;
    var opcion = ""
        //fechas
    var datetimeval = $("#fechacausatareacliente" + x).val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/cause_statuses/' + id,
        data: {
            cause_status: {
                attorney_id: id_abogado,
                "expiration_date(3i)": dia,
                "expiration_date(2i)": mes,
                "expiration_date(1i)": ano,
                "expiration_date(4i)": hh,
                "expiration_date(5i)": mm,
                finished: estado,
                description: datos_descripcion
            }
        }
    });

    ActualizarTodo(id_causa)


}

//Eliminar Estado Causas Cliente
function EliminarCausaTareasCliente(x) {
    console.log("EliminarCausaTareasCliente")

    var d = new Date();
    var fila = x;
    var table = document.getElementById("cliestadocausa");
    var tablerows = table.rows;
    var id = tablerows[fila].cells[4].textContent;
    var id_causa = tablerows[fila].cells[6].textContent;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'DELETE',
        url: '/cause_statuses/' + id,

    });

    ActualizarTodo(id_causa)
}

//Actualizar todas las tablas   
function ActualizarTodoCliente(x) {
    console.log("actualizar todo")
    $.getJSON('/audiences.json',
        function(data) {
            var tr;
            $("#table-audiencia tr").remove();
            var j = 0;
            console.log("llenar Audiences")
            for (var i = 0; i < data.length; i++) {
                if (data[i][3] == x) {
                    console.log(data[i][1])
                    tr = $('<tr/>');
                    tr.append("<td contenteditable='true'>" + data[i][0] + "</td>");
                    tr.append("<td style='display:none;'>" + data[i][1] + "</td>");
                    tr.append("<td> <i class='btn btn-primary' data-toggle='modal' data-target='#ModalAbogado' onclick='modalabogado(true," + j + ")'>" + data[i][2] + "</i> </td>");
                    tr.append("<td style='display:none;'>" + data[i][3] + "</td>");
                    tr.append("<td style='display:none;'>" + data[i][6] + "</td>");
                    tr.append("<td> <i class='material-icons btn' onclick='ActualizarAudiencia(" + j + "," + 0 + ")'>autorenew</i> </td>");
                    tr.append("<td> <i class='material-icons btn' onclick='EliminarAudiencia(" + j + ")'>clear</i> </td>");
                    $('#table-audiencia').append(tr);
                    j = j + 1;



                }
            }
        });


    $.getJSON('/cause_statuses.json',
        function(data2) {
            var tr;
            $("#table-tareas tr").remove();
            var j = 0;
            console.log("llenar Causas")
            for (var i = 0; i < data2.length; i++) {
                if (data2[i][6] == x) {


                    tr = $('<tr/>');
                    if (data2[i][5] == false) {
                        tr.append("<td><input class='btn btn-warning' value='Pendiente' onclick='CambiarEstado(" + j + ")' type='button'></td>")
                    } else {
                        tr.append("<td><input class='btn btn-success' value='Terminado' onclick='CambiarEstado(" + j + ")' type='button'></td>")
                    }
                    tr.append("<td contenteditable='true'>" + data2[i][0] + "</td>");
                    tr.append("<td contenteditable='true'>" + data2[i][1] + "</td>");
                    tr.append("<td> <i class='btn btn-primary' data-toggle='modal' data-target='#ModalAbogado' onclick='modalabogado(true)'>" + data2[i][3] + "</i> </td>");
                    tr.append("<td  style ='display:none;'>" + data2[i][4] + "</td>");
                    tr.append("<td  style ='display:none;'>" + data2[i][5] + "</td>");
                    tr.append("<td  style ='display:none;'>" + data2[i][6] + "</td>");
                    tr.append("<td> <i class='material-icons btn' onclick='ActualizarCausaTareas(" + j + ")'>autorenew</i> </td>");
                    tr.append("<td> <i class='material-icons btn' onclick='EliminarCausaTareas(" + j + ")'>clear</i> </td>");
                    $('#table-tareas').append(tr);
                    j = j + 1;
                }
            }

        });

    function formato(texto) {
        return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
    }
}

//Modificar Contrasena Usuario 

function ModificarUsuario() {

    console.log("Modificar Usuario")

    $.ajax({
        url: '/users/password',
        type: 'PUT',
        data: {
            password: "654321",
            password_confirmation: "654321",
            current_password: "123456"
        }
    })


}

//Cambiar Estado de Sub Tareas
function CambiarEstadoSubTareasCliente(x) {
    console.log("cambiar estado sub Tareas")
    var d = new Date();
    var fila = x;
    var table = document.getElementById("clisubtareas");

    var tablerows = table.rows;
    var estado = tablerows[fila].cells[7].textContent;
    var id_abogado = tablerows[fila].cells[2].textContent;
    var descripcion = tablerows[fila].cells[4].textContent;
    var id = tablerows[fila].cells[5].textContent;
    var id_work = tablerows[fila].cells[6].textContent;


    var opcion = ""

    if (estado == "true") {
        opcion = confirm("Desea dejarlo Pendiente?");
    } else {
        opcion = confirm("Ha Terminado?");
    }

    //fechas
    var datetimeval = $("#fechatareacliente" + x).val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    if (opcion == true) {
        if (estado == "true") {

            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'PUT',
                url: '/work_statuses/' + id,
                data: {
                    work_status: {
                        "expiration_date(3i)": dia,
                        "expiration_date(2i)": mes,
                        "expiration_date(1i)": ano,
                        "expiration_date(4i)": hh,
                        "expiration_date(5i)": mm,
                        attorney_id: id_abogado,
                        work_id: id_work,
                        finished: false,
                        description: descripcion
                    }
                }
            });

            ActualizarTodoTarea(id_work)
        } else {

            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'PUT',
                url: '/work_statuses/' + id,
                data: {
                    work_status: {
                        "expiration_date(3i)": dia,
                        "expiration_date(2i)": mes,
                        "expiration_date(1i)": ano,
                        "expiration_date(4i)": hh,
                        "expiration_date(5i)": mm,
                        attorney_id: id_abogado,
                        work_id: id_work,
                        finished: true,
                        description: descripcion
                    }
                }
            });

        }

        ActualizarTodoTarea(id_work)
    } else {
        console.log("NO")
    }
    ActualizarTodoTarea(id_work)
}


//Cambiar Estado de Estado de Causas
function CambiarEstadoCausasEstadoCliente(x, id_abogado) {
    console.log("cambiar estado causa cliente")
    var d = new Date();
    var fila = x;
    var abogadoseleccionado = id_abogado;
    var table = document.getElementById("cliestadocausa");
    var tablerows = table.rows;
    var id_abogado = 0;
    var datos_descripcion = tablerows[fila].cells[1].textContent;

    if (abogadoseleccionado != "") {
        id_abogado = abogadoseleccionado;
    } else {
        id_abogado = tablerows[fila].cells[7].value;
    }


    var id = tablerows[fila].cells[4].textContent;
    var estado = tablerows[fila].cells[5].textContent;

    var id_causa = tablerows[fila].cells[6].textContent;
    var opcion = ""

    if (estado == "true") {
        opcion = confirm("Desea dejarlo Pendiente?");
    } else {
        opcion = confirm("Ha Terminado?");
    }


    //fechas
    var datetimeval = $("#fechacausatareacliente" + x).val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);
    console.log(ano);
    console.log(mes);
    console.log(dia);
    console.log(hh);
    console.log(mm);


    if (opcion == true) {
        if (estado == "true") {

            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'PUT',
                url: '/cause_statuses/' + id,
                data: {
                    cause_status: {
                        attorney_id: id_abogado,
                        "expiration_date(3i)": dia,
                        "expiration_date(2i)": mes,
                        "expiration_date(1i)": ano,
                        "expiration_date(4i)": hh,
                        "expiration_date(5i)": mm,
                        finished: false,
                        description: datos_descripcion
                    }
                }
            });

            ActualizarTodo(id_causa)
        } else {

            $.ajax({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'PUT',
                url: '/cause_statuses/' + id,
                data: {
                    cause_status: {
                        attorney_id: id_abogado,
                        "expiration_date(3i)": dia,
                        "expiration_date(2i)": mes,
                        "expiration_date(1i)": ano,
                        "expiration_date(4i)": hh,
                        "expiration_date(5i)": mm,
                        finished: true,
                        description: datos_descripcion
                    }
                }
            });

        }

        ActualizarTodo(id_causa)
    } else {
        console.log("NO")
    }
    ActualizarTodo(id_causa)
}

function gestionGestion() {
    $('#gestion').css("display", "");
    $('#configuracion').css("display", "none");
}

function gestionConfiguracion() {
    $('#gestion').css("display", "none");
    $('#configuracion').css("display", "");
}