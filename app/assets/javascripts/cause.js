//Llenar listado de causas activas
function tex() {
    var textarea = document.querySelector('textarea');
    textarea.addEventListener('keydown', autosize);
}

function obtenerLogin() {
    console.log("Obtener login ")
    $.getJSON('/login.json',
        function(data) {

            var perfil = data.role;
            var user_id = data.id;
            ListadoTareasVintes(perfil, user_id)
            listadoCausasActivas(perfil, user_id);
            listadoCausasArchivadas(perfil, user_id)
        });
}

function obtenerLogin_seleccion() {
    $.getJSON('/login.json',
        function(data) {

            var perfil = data.role;
            var user_id = data.id;
            listadoCausasActivas(perfil, user_id);
        });
}

function GenerarNumeroCausa() {
    $.getJSON('/causes.json',
        function(data) {
            if (data.length != 0) {
                //Crear cdigo !nuevo
                var largo = data.length - 1
                var code = data[largo].code;
                var largo_codigo = code.length;
                var letra = code.substring(0, 1);
                var numero = code.substring(1, largo_codigo);
                var codigo_nuevo = null;
                if (numero == "") {
                    numero = 1;
                } else {
                    var codigo_nuevo = parseInt(numero) + 1;
                }
                $("#nuevocodigo").val(codigo_nuevo);
            }
        })
}

function listadoCausasActivas(login, user) {
    // var tipo = document.getElementById("tipo").value;
    var login = login;
    var user = user;

    $.getJSON('/causes.json',
        function(data) {
            if (data.length != 0) {
                //Crear cdigo !nuevo
                var largo = data.length - 1
                var code = data[largo].code;
                var largo_codigo = code.length;
                var letra = code.substring(0, 1);
                var numero = code.substring(1, largo_codigo);
                var codigo_nuevo = null;
                if (numero == "") {
                    numero = 1;
                } else {
                    var codigo_nuevo = parseInt(numero) + 1;
                }
                $("#nuevocodigo").val(codigo_nuevo);
                if (login != "Embajador") {
                    var tr;
                    $("#table-cause tr").remove();
                    var j = 0;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].archived == false) {
                            tr = $("<a><tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'></a>");
                            tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                            tr.append("<td>" + data[i].code + "</td>");
                            tr.append("<td>" + data[i].name + "</td>");
                            $('#table-cause').append(tr);
                            j = j + 1;
                        }
                    }
                } else {

                    $.getJSON('/attorneys.json',
                        function(dataabogado) {

                            for (var i = 0; i < dataabogado.length; i++) {
                                if (dataabogado[i].user_id == user) {
                                    listadoCausasActivas_Embajador(dataabogado[i].id)
                                }
                            }
                        });

                }
            } else {
                $("#nuevocodigo").val("1");
            }
        });
}

function listadoCausasActivas_Embajador(id_abogado) {
    causamenulistadoasutons();
    var id_abogado = id_abogado;
    var causa = null;
    $.getJSON('/causes_abogado.json',
        function(data) {
            //llenar abogado a cargo de causas

            var tr;
            $("#table-cause tr").remove();
            $("#table-cause-asuntos tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].archived == false && data[i].id_attorney_c == id_abogado) {
                    if (causa != data[i].id) {
                        causa = data[i].id;
                        tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa_Encargado(" + data[i].id + "))'>");
                        tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td>" + data[i].code + "</td>");
                        tr.append("<td >" + data[i].name + "</td>");
                        $('#table-cause').append(tr);
                        j = j + 1;
                    }

                }
            }

            for (var i = 0; i < data.length; i++) {
                if (data[i].archived == false && data[i].attorney_id == id_abogado || data[i].id_attorney_au == id_abogado || data[i].id_attorney_id_h == id_abogado) {
                    if (causa != data[i].id) {
                        causa = data[i].id;
                        tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'>");
                        tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td>" + data[i].code + "</td>");
                        tr.append("<td >" + data[i].name + "</td>");
                        $('#table-cause-asuntos').append(tr);
                        j = j + 1;
                    }

                }
            }
        });
}

function listadoCausasArchivadas_Embajador(id_abogado) {

    var id_abogado = id_abogado;
    var causa = null;
    $.getJSON('/causes_abogado.json',
        function(data) {
            //llenar abogado a cargo de causas

            var tr;
            $("#table-cause-arch tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].archived == true) {
                    if (data[i].attorney_id == id_abogado || data[i].id_attorney_au == id_abogado || data[i].id_attorney_id_h == id_abogado) {
                        if (causa != data[i].id) {
                            causa = data[i].id;
                            tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'>");
                            tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                            tr.append("<td>" + data[i].code + "</td>");
                            tr.append("<td >" + data[i].name + "</td>");
                            $('#table-cause-arch').append(tr);
                            j = j + 1;
                        }

                    }
                }
            }
        });
}

function listadoCausasArchivadas(login, user) {
    var login = login;
    var user = user;
    $.getJSON('/causes.json',
        function(data) {
            if (login != "Embajador") {
                var tr;
                $("#table-cause-arch tr").remove();
                var j = 0;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].archived == true) {
                        tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'>");
                        tr.append("<td><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                        tr.append("<td>" + data[i].code + "</td>");
                        tr.append("<td >" + data[i].name + "</td>");
                        $('#table-cause-arch').append(tr);
                        j = j + 1;
                    }
                }
            } else {

                $.getJSON('/attorneys.json',
                    function(dataabogado) {

                        for (var i = 0; i < dataabogado.length; i++) {
                            if (dataabogado[i].user_id == user) {
                                listadoCausasArchivadas_Embajador(dataabogado[i].id)
                            }
                        }
                    });
            }


        });
}

function filtroCausasActivas() {
    var fmateria = $("#fmateria").val();
    var finstancia = $("#finstancia").val();
    $.getJSON('/causes.json',
        function(data) {
            var tr;
            $("#table-cause tr").remove();
            var j = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].archived == false) {
                    if (fmateria == "Seleccionar...") {
                        if (finstancia == "Seleccionar...") {
                            tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'>");
                            tr.append("<td style='width:25%'><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                            tr.append("<td style='width:40%'>" + data[i].code + "</td>");
                            tr.append("<td >" + data[i].name + "</td>");
                            $('#table-cause').append(tr);
                        } else {
                            if (data[i].caratulado == finstancia) {
                                tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'>");
                                tr.append("<td style='width:25%'><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                                tr.append("<td style='width:40%'>" + data[i].code + "</td>");
                                tr.append("<td >" + data[i].name + "</td>");
                                $('#table-cause').append(tr);
                            }
                        }

                        j = j + 1;
                    } else {
                        if (finstancia == "Seleccionar...") {
                            if (data[i].matter == fmateria) {
                                tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'>");
                                tr.append("<td style='width:25%'><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                                tr.append("<td style='width:40%'>" + data[i].code + "</td>");
                                tr.append("<td >" + data[i].name + "</td>");
                                $('#table-cause').append(tr);
                            }
                        } else {
                            if (data[i].matter == fmateria & data[i].caratulado == finstancia) {
                                tr = $("<tr/ style='cursor:pointer;' onclick='(seleccionDatosCausa(" + data[i].id + "))'>");
                                tr.append("<td style='width:25%'><i class='fas fa-circle' style='color: #c4a215'></i></td>");
                                tr.append("<td style='width:40%'>" + data[i].code + "</td>");
                                tr.append("<td >" + data[i].name + "</td>");
                                $('#table-cause').append(tr);
                            }
                        }
                    }



                }
            }
        });
}

function causemenuexpediente() {
    $('#expediente').css("display", "");
    $('#tramitacion').css("display", "none");
}

function causamenulistadoasutons() {
    $('#t-menucausalistadoasuntos').css("display", "");
    $('#menucausalistadoasuntos').css("display", "");
}

function causemenutramitacion() {
    $('#expediente').css("display", "none");
    $('#tramitacion').css("display", "");
}

function causemenuingreso() {
    $('#menucausalistado').css("display", "none");
    $('#btncausaingreso').css("display", "none");
    $('#menucausaingresar').css("display", "");
    $('#btncausalista').css("display", "");

}

function causamenulistado() {
    $('#menucausaingresar').css("display", "none");
    $('#menucausalistado').css("display", "");
    $('#btncausalista').css("display", "none");
    $('#btncausaingreso').css("display", "");
}

//Mostrar menu para agregar causa-tareas
function mostrarcausatareaingreso() {
    $('#btncausatareaingreso').css("display", "none");
    $('#menucausatareaingresar').css("display", "");
    $('#btncausatarealista').css("display", "");
    $('#listadocausatareas').css("display", "none");

    fecha("#fechacuasatarea");

}

function mostrarcausatareaeditar() {
    $('#btncausatareaingreso').css("display", "none");
    $('#menucausatareaeditar').css("display", "");
    $('#btncausatarealista').css("display", "");
    $('#listadocausatareas').css("display", "none");

}

function mostrarlistacausatarea() {
    $('#btncausatareaingreso').css("display", "");
    $('#menucausatareaingresar').css("display", "none");
    $('#menucausatareaeditar').css("display", "none");
    $('#btncausatarealista').css("display", "none");
    $('#listadocausatareas').css("display", "");
    $('#btncausatareaarchivar').css("display", "");
    $('#listadocausatareasarchivada').css("display", "none");
}

//Mostrar menu para agregar causa-audiencia
function mostrarcausaaudienciaingreso() {
    $('#btncausaaudienciaingreso').css("display", "none");
    $('#menucausaaudienciaingresar').css("display", "");
    $('#btncausaaudiencialista').css("display", "");
    $('#listadocausaaudiencias').css("display", "none");
    fecha_actual("#fechacuasaaudiencia");

}

//Mostrar menu para editar causa-audiencia
function mostrarcausaaudienciaeditar() {
    $('#btncausaaudienciaingreso').css("display", "none");
    $('#menucausaaudienciaingresar').css("display", "none");
    $('#btncausaaudiencialista').css("display", "");
    $('#listadocausaaudiencias').css("display", "none");
    $('#menucausaaudienciaeditar').css("display", "");
}

function mostrarlistacausaaudiencia() {
    $('#btncausaaudienciaingreso').css("display", "");
    $('#menucausaaudienciaingresar').css("display", "none");
    $('#btncausaaudiencialista').css("display", "none");
    $('#listadocausaaudiencias').css("display", "");
    $('#menucausaaudienciaeditar').css("display", "none");
    $('#listadocausaaudiencias-archivadas').css("display", "none");
    $('#btncausaaudienciaarchivar').css("display", "");
}

//Mostrar menu para agregar causa-audiencia
function mostrarcausahistoriaciaingreso() {
    $('#btncausahistoriaingreso').css("display", "none");
    $('#menucausahistoriaingresar').css("display", "");
    $('#btncausahistorialista').css("display", "");
    $('#listadocausahistoria').css("display", "none");


    //Fecha
    var now = new Date();
    var utcString = now.toISOString().substring(0, 19);
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    var localDatetime = year + "-" +
        (month < 10 ? "0" + month.toString() : month) + "-" +
        (day < 10 ? "0" + day.toString() : day);
    $('#fechacuasahistoria').val(localDatetime);




}

function mostrarcausahistoriaciaeditar() {
    $('#btncausahistoriaingreso').css("display", "none");
    $('#menucausahistoriaingresar').css("display", "none");
    $('#btncausahistorialista').css("display", "");
    $('#listadocausahistoria').css("display", "none");
    $('#menucausahistoriaeditar').css("display", "");


}

function mostrarlistacausahistoria() {
    $('#btncausahistoriaingreso').css("display", "");
    $('#menucausahistoriaingresar').css("display", "none");
    $('#menucausahistoriaeditar').css("display", "none");
    $('#btncausahistorialista').css("display", "none");
    $('#listadocausahistoria').css("display", "");
}

//Mostrar Menu Listado Tareas archivadas

function mostrarlistacausatareaarchivadas() {
    $('#btncausatareaingreso').css("display", "");
    $('#menucausatareaingresar').css("display", "none");
    $('#menucausatareaeditar').css("display", "none");
    $('#btncausatarealista').css("display", "");
    $('#btncausatareaarchivar').css("display", "none");
    $('#listadocausatareasarchivada').css("display", "");
    $('#listadocausatareas').css("display", "none");
}

//Mostrar Menu Listado Tareas archivadas

function mostrarlistacausaaudienciaarchivadas() {
    $('#btncausaaudienciaingreso').css("display", "");
    $('#menucausaaudienciaingresar').css("display", "none");
    $('#btncausaaudiencialista').css("display", "");
    $('#listadocausaaudiencias').css("display", "none");
    $('#menucausaaudienciaeditar').css("display", "none");
    $('#listadocausaaudiencias-archivadas').css("display", "");
    $('#btncausaaudienciaarchivar').css("display", "none");

}

function agregarCausa() {

    var nombrecausa = document.getElementById("nombrecausa").value;
    var code = "K";
    var num_codigo = $("#nuevocodigo").val();
    var codigocausa = code + num_codigo;
    var cliente = $("#combocliente").val();
    var abogado = $("#comboabogado").val();
    if (nombrecausa != "") {


        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: '/causes',
            data: {
                cause: {
                    name: nombrecausa,
                    code: codigocausa,
                    attorney_id: abogado,
                    client_id: cliente,
                    archived: false
                }
            },
            success: function(data, textStatus, jQxhr) {

                limpiarNombreCausa();
                toastr.success('SE AGREGO CAUSA')
                GenerarNumeroCausa();
                actualizarListadoCausas();
                causamenulistado();

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        });
    } else {
        toastr.warning('SE DEBE LLENAR EL NOMBRE');

        document.getElementById("nombrecausa").style.border = "1px solid red";
    }

}

function limpiarNombreCausa() {
    document.getElementById("nombrecausa").value = "";
}

//Llenar Audiencia y Causa desde el id_cause
function seleccionDatosCausa(id_causa) {
    console.log("Actualizacion de TAH causas 2")
    var id = id_causa;

    $("#idcausa").val(id);
    $('#menuCausas').css("display", "");
    $('#menuGestiones').css("display", "none");


    $.getJSON('/causes/' + id + '.json',
        function(data) {
            $("#rol").val(data.role);
            $("#Tribunal").val(data.court);
            $("#Materia").val(data.matter);
            $("#procedimiento").val(data.process);
            $("#Cuantia").val(data.amount);
            $("#descripcion").val(data.description);
            $("#instancia").val(data.caratulado);
            $("#nombre").val(data.name);

            $.getJSON('/clients/' + data.client_id + '.json',
                function(dataclient) {
                    $("#combocliente2").val(dataclient.id);
                    $("#combocliente3").val(dataclient.id);
                    $('#clientecausa').text(dataclient.name);
                    $('#clientecausa2').text(dataclient.name);
                });
            $('#codigocausa').text(data.code);
            $('#codigocausa2').text(data.code);


            $.getJSON('/attorneys/' + data.attorney_id + '.json',
                function(dataabogado) {
                    $("#comboabogado2").val(dataabogado.id);
                });


        });

    $.getJSON('/login.json',
        function(data_login) {
            var perfil = data_login.role;
            var user = data_login.id;

            if (perfil != "Embajador") {
                $.getJSON('/audiences.json',
                    function(data) {
                        var tr;
                        $("#table-cause-audiencias tr").remove();
                        var j = 0;
                        for (var i = 0; i < data.length; i++) {
                            if (data[i][3] == id & data[i][8] == false) {
                                tr = $("<tr/ >");
                                tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(archivarCausaAudiencia_listado(" + data[i][6] + "))' class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data[i][7] + "</td>");
                                tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarCausaAudiencia(" + data[i][6] + "))'  class='far fa-edit'></i></td>")
                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img  id='imagenaul" + data[i][6] + "' src=" + data[i][10] + " alt='Avatar' class='avatar'></button><div id='abogados_audiencia" + j + "' class='dropdown-content'></div></div></td>");
                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][0].replace('.000Z', '') + "'></td>");

                                $('#table-cause-audiencias').append(tr);
                                llenarAbogadoAudiencia(j, data[i][6]);
                                j = j + 1;
                            }

                        }


                    });

                $.getJSON('/audiences.json',
                    function(data) {
                        var tr;

                        $("#table-cause-audiencias-archivada tr").remove();
                        var j = 0;
                        for (var i = 0; i < data.length; i++) {
                            if (data[i][3] == id & data[i][8] == true) {
                                tr = $('<tr/>');
                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data[i][7] + "</td>");
                                tr.append("<td><i  onclick='(eliminarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i onclick='(desarchivarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data[i][10] + " alt='Avatar' class='avatar'></button><div id='abogados_audiencia_Arc" + j + "' class='dropdown-content'></div></div></td>");
                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][0].replace('.000Z', '') + "'></td>");
                                $('#table-cause-audiencias-archivada').append(tr);
                                // llenarAbogadoAudiencia_Archivadas(j);
                                j = j + 1;
                            }

                        }


                    });


                $.getJSON('/cause_statuses.json',
                    function(data2) {
                        var tr;
                        $("#table-cause-tareas tr").remove();
                        var j = 0;
                        for (var i = 0; i < data2.length; i++) {
                            if (data2[i][5] == id & data2[i][4] == false) {
                                tr = $("<tr/ >");
                                tr.append("<td width='20'><i style='cursor:pointer;' onclick='(archivarCausaTarea_listado(" + data2[i][3] + "))' class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data2[i][0] + "</td>");
                                tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaTarea(" + data2[i][3] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarCausaTarea(" + data2[i][3] + "))'  class='far fa-edit'></i></td>")
                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img id='imagentareal" + data2[i][3] + "' src=" + data2[i][6] + " alt='Avatar' class='avatar'></button><div id='abogados_causa_status" + j + "' class='dropdown-content'></div></div></td>");
                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechacausatarea' name='fechacausatarea' value='" + data2[i][1].substring(0, 10) + "'></td>");
                                $('#table-cause-tareas').append(tr);

                                llenarAbogadoTarea(j, data2[i][3]);
                                j = j + 1;
                            }


                        }

                    });

                //Archivados
                $.getJSON('/cause_statuses.json',
                    function(data2) {
                        var tr;
                        $("#table-cause-tareas-archivadas tr").remove();
                        var j = 0;
                        for (var i = 0; i < data2.length; i++) {
                            if (data2[i][5] == id & data2[i][4] == true) {
                                tr = $("<tr/ >");
                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                tr.append("<td width='680'>" + data2[i][0] + "</td>");
                                tr.append("<td><i  onclick='(eliminarCausaTarea(" + data2[i][3] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i  onclick='(desarchivarCausaTarea(" + data2[i][3] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data2[i][6] + " alt='Avatar' class='avatar'></button><div id='abogados_causa_status_arc" + j + "' class='dropdown-content'></div></div></td>");
                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date' id='fechacausatarea' name='fechacausatarea' value='" + data2[i][1].substring(0, 10) + "'></td>");
                                $('#table-cause-tareas-archivadas').append(tr);
                                llenarAbogadoTareaArchivada(j);
                                j = j + 1;
                            }


                        }

                    });


                $.getJSON('/cause_histories.json',
                    function(data_histories) {
                        var tr;
                        $("#table-cause-historia tr").remove();
                        var j = 0;
                        for (var i = 0; i < data_histories.length; i++) {
                            if (data_histories[i][3] == id) {
                                tr = $("<tr/>");
                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                tr.append("<td disabled='true' >" + data_histories[i][0] + "</td>");
                                tr.append("<td><i  onclick='(eliminarCausaHistoria(" + data_histories[i][2] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                tr.append("<td><i style='cursor:pointer;' id='edit' onclick='(seleccionarCausaHistoria(" + data_histories[i][2] + "))'  class='far fa-edit'></i></td>")
                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data_histories[i][4] + " alt='Avatar' class='avatar'></button><div id='cause_histories" + j + "' class='dropdown-content'></div></div></td>");
                                tr.append("<td width='200'><input disabled='true' id='fechacausatarea" + j + "' type='date' id='fechacausatarea' name='fechacausatarea' value='" + data_histories[i][1].substring(0, 10) + "'></td>");
                                $('#table-cause-historia').append(tr);
                                // llenarAbogadoHistoria(j);
                                j = j + 1;
                            }


                        }

                    });



            } else {
                $.getJSON('/attorneys.json',
                    function(dataabogado) {

                        for (var i = 0; i < dataabogado.length; i++) {
                            if (dataabogado[i].user_id == user) {
                                var abogado = dataabogado[i].id;

                                $.getJSON('/audiences.json',
                                    function(data) {
                                        var tr;
                                        $("#table-cause-audiencias tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data.length; i++) {
                                            if (data[i][3] == id & data[i][8] == false & data[i][1] == abogado) {
                                                tr = $("<tr/ >");
                                                tr.append("<td  width='20'><i style='cursor:pointer;' onclick='(archivarCausaAudiencia_listado(" + data[i][6] + "))' class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data[i][7] + "</td>");
                                                tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarCausaAudiencia(" + data[i][6] + "))'  class='far fa-edit'></i></td>")
                                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data[i][10] + " alt='Avatar' class='avatar'></button><div id='abogados_audiencia" + j + "' class='dropdown-content'></div></div></td>");
                                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][0].replace('.000Z', '') + "'></td>");
                                                $('#table-cause-audiencias').append(tr);
                                                // llenarAbogadoAudiencia(j);
                                                j = j + 1;
                                            }

                                        }

                                    });

                                $.getJSON('/audiences.json',
                                    function(data) {
                                        var tr;

                                        $("#table-cause-audiencias-archivada tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data.length; i++) {
                                            if (data[i][3] == id & data[i][8] == true & data[i][1] == abogado) {
                                                tr = $('<tr/>');
                                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data[i][7] + "</td>");
                                                tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                                                tr.append("<td><i style='cursor:pointer;' onclick='(desarchivarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-box' style='color: #c4a215;'></i></td>")
                                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data[i][10] + " alt='Avatar' class='avatar'></button><div id='abogados_audiencia_Arc" + j + "' class='dropdown-content'></div></div></td>");
                                                tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][0].replace('.000Z', '') + "'></td>");
                                                $('#table-cause-audiencias-archivada').append(tr);
                                                // llenarAbogadoAudiencia_Archivadas(j);
                                                j = j + 1;
                                            }

                                        }


                                    });


                                $.getJSON('/cause_statuses.json',
                                    function(data2) {
                                        var tr;
                                        $("#table-cause-tareas tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data2.length; i++) {
                                            if (data2[i][5] == id & data2[i][4] == false & data2[i][2] == abogado) {
                                                tr = $("<tr/ >");
                                                tr.append("<td width='20'><i style='cursor:pointer;' onclick='(archivarCausaTarea_listado(" + data2[i][3] + "))' class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data2[i][0] + "</td>");
                                                tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaTarea(" + data2[i][3] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarCausaTarea(" + data2[i][3] + "))'  class='far fa-edit'></i></td>")
                                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data2[i][6] + " alt='Avatar' class='avatar'></button><div id='abogados_causa_status" + j + "' class='dropdown-content'></div></div></td>");
                                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date'  name='fechaaudiencia' value='" + data2[i][1].substring(0, 10) + "'></td>");
                                                $('#table-cause-tareas').append(tr);
                                                // llenarAbogadoTarea(j);
                                                j = j + 1;
                                            }


                                        }

                                    });

                                //Archivados
                                $.getJSON('/cause_statuses.json',
                                    function(data2) {
                                        var tr;
                                        $("#table-cause-tareas-archivadas tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data2.length; i++) {
                                            if (data2[i][5] == id & data2[i][4] == true & data2[i][2] == abogado) {
                                                tr = $("<tr/ >");
                                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data2[i][0] + "</td>");
                                                tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaTarea(" + data2[i][3] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                                                tr.append("<td><i style='cursor:pointer;' onclick='(desarchivarCausaTarea(" + data2[i][3] + "))'  class='fas fa-box' style='color: #c4a215;'></i></td>")
                                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data2[i][6] + " alt='Avatar' class='avatar'></button><div id='abogados_causa_status_arc" + j + "' class='dropdown-content'></div></div></td>");
                                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date'  name='fechaaudiencia' value='" + data2[i][1].substring(0, 10) + "'></td>");
                                                $('#table-cause-tareas-archivadas').append(tr);
                                                // llenarAbogadoTareaArchivada(j);
                                                j = j + 1;
                                            }


                                        }

                                    });


                                $.getJSON('/cause_histories.json',
                                    function(data_histories) {
                                        var tr;
                                        $("#table-cause-historia tr").remove();
                                        var j = 0;
                                        for (var i = 0; i < data_histories.length; i++) {
                                            if (data_histories[i][3] == id & data_histories[i][5] == abogado) {
                                                tr = $("<tr/ >");
                                                // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                                                tr.append("<td width='680'>" + data_histories[i][0] + "</td>");
                                                tr.append("<td><i  onclick='(eliminarCausaHistoria(" + data_histories[i][2] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarCausaHistoria(" + data_histories[i][2] + "))'  class='far fa-edit'></i></td>")
                                                tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data_histories[i][4] + " alt='Avatar' class='avatar'></button><div id='cause_histories" + j + "' class='dropdown-content'></div></div></td>");
                                                tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='date'name='fechaaudiencia' value='" + data_histories[i][1].substring(0, 10) + "'></td>");
                                                $('#table-cause-historia').append(tr);
                                                // llenarAbogadoHistoria(j);
                                                j = j + 1;
                                            }


                                        }

                                    });


                            }
                        }
                    });

            }


        });
}

function seleccionDatosCausa_Encargado(id_causa) {
    var id = id_causa;

    $("#idcausa").val(id);
    $('#menuCausas').css("display", "");
    $('#menuGestiones').css("display", "none");


    $.getJSON('/causes/' + id + '.json',
        function(data) {
            $("#rol").val(data.role);
            $("#Tribunal").val(data.court);
            $("#Materia").val(data.matter);
            $("#procedimiento").val(data.process);
            $("#Cuantia").val(data.amount);
            $("#descripcion").val(data.description);
            $("#instancia").val(data.caratulado);
            $("#nombre").val(data.name);

            $.getJSON('/clients/' + data.client_id + '.json',
                function(dataclient) {
                    $("#combocliente2").val(dataclient.id);
                    $("#combocliente3").val(dataclient.id);
                    $('#clientecausa').text(dataclient.name);
                    $('#clientecausa2').text(dataclient.name);
                });
            $('#codigocausa').text(data.code);
            $('#codigocausa2').text(data.code);


            $.getJSON('/attorneys/' + data.attorney_id + '.json',
                function(dataabogado) {
                    $("#comboabogado2").val(dataabogado.id);
                });


        });

    $.getJSON('/login.json',
        function(data_login) {
            var perfil = data_login.role;
            var user = data_login.id;


            $.getJSON('/audiences.json',
                function(data) {
                    var tr;
                    $("#table-cause-audiencias tr").remove();
                    var j = 0;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i][3] == id & data[i][8] == false) {
                            tr = $("<tr/ >");
                            tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(archivarCausaAudiencia_listado(" + data[i][6] + "))' class='far fa-square'></i></td>")
                            tr.append("<td width='680'>" + data[i][7] + "</td>");
                            tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                            tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarCausaAudiencia(" + data[i][6] + "))'  class='far fa-edit'></i></td>")
                            tr.append("<td> <div class='dropdown'><button class='dropbtn'><img  id='imagenaul" + data[i][6] + "' src=" + data[i][10] + " alt='Avatar' class='avatar'></button><div id='abogados_audiencia" + j + "' class='dropdown-content'></div></div></td>");
                            tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][0].replace('.000Z', '') + "'></td>");

                            $('#table-cause-audiencias').append(tr);
                            llenarAbogadoAudiencia(j, data[i][6]);
                            j = j + 1;
                        }

                    }


                });

            $.getJSON('/audiences.json',
                function(data) {
                    var tr;

                    $("#table-cause-audiencias-archivada tr").remove();
                    var j = 0;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i][3] == id & data[i][8] == true) {
                            tr = $('<tr/>');
                            // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                            tr.append("<td width='680'>" + data[i][7] + "</td>");
                            tr.append("<td><i  onclick='(eliminarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                            tr.append("<td><i onclick='(desarchivarCausaAudiencia(" + data[i][6] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                            tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data[i][10] + " alt='Avatar' class='avatar'></button><div id='abogados_audiencia_Arc" + j + "' class='dropdown-content'></div></div></td>");
                            tr.append("<td width='200'><input id='fechaaudiencia" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data[i][0].replace('.000Z', '') + "'></td>");
                            $('#table-cause-audiencias-archivada').append(tr);
                            // llenarAbogadoAudiencia_Archivadas(j);
                            j = j + 1;
                        }

                    }


                });


            $.getJSON('/cause_statuses.json',
                function(data2) {
                    var tr;
                    $("#table-cause-tareas tr").remove();
                    var j = 0;
                    for (var i = 0; i < data2.length; i++) {
                        if (data2[i][5] == id & data2[i][4] == false) {
                            tr = $("<tr/ >");
                            tr.append("<td width='20'><i style='cursor:pointer;' onclick='(archivarCausaTarea_listado(" + data2[i][3] + "))' class='far fa-square'></i></td>")
                            tr.append("<td width='680'>" + data2[i][0] + "</td>");
                            tr.append("<td><i style='cursor:pointer;' onclick='(eliminarCausaTarea(" + data2[i][3] + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>")
                            tr.append("<td><i id='edit' style='cursor:pointer;' onclick='(seleccionarCausaTarea(" + data2[i][3] + "))'  class='far fa-edit'></i></td>")
                            tr.append("<td> <div class='dropdown'><button class='dropbtn'><img id='imagentareal" + data2[i][3] + "' src=" + data2[i][6] + " alt='Avatar' class='avatar'></button><div id='abogados_causa_status" + j + "' class='dropdown-content'></div></div></td>");
                            tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][1].replace('.000Z', '') + "'></td>");
                            $('#table-cause-tareas').append(tr);

                            llenarAbogadoTarea(j, data2[i][3]);
                            j = j + 1;
                        }


                    }

                });

            //Archivados
            $.getJSON('/cause_statuses.json',
                function(data2) {
                    var tr;
                    $("#table-cause-tareas-archivadas tr").remove();
                    var j = 0;
                    for (var i = 0; i < data2.length; i++) {
                        if (data2[i][5] == id & data2[i][4] == true) {
                            tr = $("<tr/ >");
                            // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                            tr.append("<td width='680'>" + data2[i][0] + "</td>");
                            tr.append("<td><i  onclick='(eliminarCausaTarea(" + data2[i][3] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                            tr.append("<td><i  onclick='(desarchivarCausaTarea(" + data2[i][3] + "))'  class='fas fa-box' style='cursor:pointer;color: #c4a215;'></i></td>")
                            tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data2[i][6] + " alt='Avatar' class='avatar'></button><div id='abogados_causa_status_arc" + j + "' class='dropdown-content'></div></div></td>");
                            tr.append("<td width='200'><input id='fechacausatarea" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data2[i][1].replace('.000Z', '') + "'></td>");
                            $('#table-cause-tareas-archivadas').append(tr);
                            llenarAbogadoTareaArchivada(j);
                            j = j + 1;
                        }


                    }

                });


            $.getJSON('/cause_histories.json',
                function(data_histories) {
                    var tr;

                    $("#table-cause-historia tr").remove();
                    var j = 0;
                    for (var i = 0; i < data_histories.length; i++) {
                        if (data_histories[i][3] == id) {
                            tr = $("<tr/>");
                            // tr.append("<td width='20'><i class='far fa-square'></i></td>")
                            tr.append("<td disabled='true' >" + data_histories[i][0] + "</td>");
                            tr.append("<td><i  onclick='(eliminarCausaHistoria(" + data_histories[i][2] + "))'  class='fas fa-eraser ' style='cursor:pointer;color: #c4a215;' ></i></td>")
                            tr.append("<td><i style='cursor:pointer;' id='edit' onclick='(seleccionarCausaHistoria(" + data_histories[i][2] + "))'  class='far fa-edit'></i></td>")
                            tr.append("<td> <div class='dropdown'><button class='dropbtn'><img src=" + data_histories[i][4] + " alt='Avatar' class='avatar'></button><div id='cause_histories" + j + "' class='dropdown-content'></div></div></td>");
                            tr.append("<td width='200'><input disabled='true' id='fechacausatarea" + j + "' type='datetime-local' id='fechaaudiencia' name='fechaaudiencia' value='" + data_histories[i][1].replace('.000Z', '') + "'></td>");
                            $('#table-cause-historia').append(tr);
                            // llenarAbogadoHistoria(j);
                            j = j + 1;
                        }


                    }

                });





        });
}

function autosize() {
    var el = this;
    setTimeout(function() {
        el.style.cssText = 'height:auto; padding:0';
        el.style.cssText = 'height:' + el.scrollHeight + 'px';
    }, 0);
}

function llenarAbogadoAudiencia(id, id_audiencia) {
    var a = "";
    var id_au = id_audiencia;
    $.getJSON('/attorneys.json',
        function(data) {

            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;' onclick='cambiarAbogado_L_A(" + id_au + "," + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }

            $('abogados_audiencia' + id + ' a').remove();
            $('#abogados_audiencia' + id).append(a);

        });
}

// function llenarAbogadoAudiencia_Archivadas(id) {
//     var a = "";

//     $.getJSON('/attorneys.json',
//         function(data) {
//             for (var i = 0; i < data.length; i++) {
//                 a += "<a id='" + data[i].id + "'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
//             }
//             $('abogados_audiencia_Arc' + id + ' a').remove();
//             $('#abogados_audiencia_Arc' + id).append(a);

//         });
// }

function llenarAbogadoTarea(id, id_t) {

    var a = "";
    var id_tarea = id_t;

    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick='cambiarAbogado_L_T(" + id_tarea + "," + data[i].id + ")'><img  src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";

            }
            $('abogados_causa_status' + id + ' a').remove();
            $('#abogados_causa_status' + id).append(a);

        });
}

function ModificarAbogadoCausaTarea(id_abogado, id_tarea) {

    var id_abogado = id_abogado;
    var id_cause_status = id_tarea;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/cause_statuses/' + id_cause_status,
        data: {
            cause_status: {
                attorney_id: id_abogado
            },
            success: function(data, textStatus, jQxhr) {

                //limpiarNombreCausa();
                toastr.success('SE MODIFICO EL ABOGADO')

                actualizarTAHCausa(id_causa);
                //actualizarListadoCausas();

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });
}

function ModificarAbogadoCausaAudiencia(id_abogado, id_audiencia) {

    var id_abogado = id_abogado;
    var id_audiencia = id_audiencia;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/audiences/' + id_audiencia,
        data: {
            audience: {
                attorney_id: id_abogado
            },
            success: function(data, textStatus, jQxhr) {
                toastr.success('SE MODIFICO EL ABOGADO')
                    // actualizarTAHCausa(id_causa);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });

    // $.ajax({
    //     headers: {
    //         'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    //     },
    //     method: 'PUT',
    //     url: '/cause_statuses/' + id_cause_status,
    //     data: {
    //         cause_status: {
    //             attorney_id: id_abogado
    //         },
    //         success: function(data, textStatus, jQxhr) {

    //             //limpiarNombreCausa();
    //             toastr.success('SE MODIFICO EL ABOGADO')

    //             // actualizarTAHCausa(id_causa);
    //             //actualizarListadoCausas();

    //         },
    //         error: function(jqXhr, textStatus, errorThrown) {
    //             console.log("errorThrown");
    //         }
    //     }
    // });
}

function llenarAbogadoTareaArchivada(id) {
    var a = "";

    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('abogados_causa_status_arc' + id + ' a').remove();
            $('#abogados_causa_status_arc' + id).append(a);

        });
}

// function llenarAbogadoHistoria(id) {
//     var a = "";

//     $.getJSON('/attorneys.json',
//         function(data) {
//             for (var i = 0; i < data.length; i++) {
//                 a += "<a id='" + data[i].id + "'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
//             }
//             $('cause_histories' + id + ' a').remove();
//             $('#cause_histories' + id).append(a);
//         });
// }

function llenarAbogadoTareaNueva() {
    var a = "";
    var dato1 = "tarea"
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick='cambiarAbogadoNuevo_T(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar'  class='avatar'></a>";
            }
            $('#abogados_causa_status_nuevo a').remove();
            $('#abogados_causa_status_nuevo').append(a);
        });
    //
}

function llenarAbogadoTareaEditar() {

    var a = "";

    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "'  style='cursor:pointer;' onclick='cambiarAbogadoEditar_T(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('abogados_causa_status_editar a').remove();
            $('#abogados_causa_status_editar').append(a);
        });
}

function llenarAbogadoAudienciaNueva() {

    var a = "";

    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoNuevo_A(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('abogados_audiencia_ingresar a').remove();
            $('#abogados_audiencia_ingresar').append(a);
        });
}

function llenarAbogadoAudienciaEditar() {

    var a = "";

    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoEditar_A(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('abogados_audiencia_editar a').remove();
            $('#abogados_audiencia_editar').append(a);
        });
}

function llenarAbogadoHistoriaNueva() {
    var a = "";
    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoNuevo_H(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('abogados_historia_ingresar a').remove();
            $('#abogados_historia_ingresar').append(a);
        });
}

function llenarAbogadoHistoriaEditar() {

    var a = "";

    $.getJSON('/attorneys.json',
        function(data) {
            for (var i = 0; i < data.length; i++) {
                a += "<a id='" + data[i].id + "' style='cursor:pointer;'  onclick = 'cambiarAbogadoEditar_H(" + data[i].id + ")'><img src='" + data[i].photo + "' alt='Avatar' class='avatar'></a>";
            }
            $('abogados_historia_editar a').remove();
            $('#abogados_historia_editar').append(a);
        });
}


//Actualizar datos de Causas
function Archivar() {
    var id_cause = document.getElementById("idcausa").value;
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/causes/' + id_cause,
        data: {
            cause: {
                archived: true
            }
        }
    });
    toastr.warning('SE ARCHIVO LA CAUSA')

    //Actualizacion de lista
    actualizarListadoCausas();

}

function EliminarCausas() {
    var id_cause = document.getElementById("idcausa").value;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'DELETE',
        url: '/causes/' + id_cause,

    });
    toastr.error('SE ELIMINO CAUSA');
    actualizarListadoCausas();
    $("#modal_eliminar").modal('hide');

}

function actualizarListadoCausas() {
    //Actualizacion de lista
    obtenerLogin();
    listadoCausasArchivadas();

}

function actualizarTAHCausa(id_causa) {
    console.log("Actualizacion de TAH causas");
    var id = id_causa;
    seleccionDatosCausa(id);
}

//Actualizar datos de Causas
function ActualizarCausas() {

    var id_cause = $("#idcausa").val();;
    var id_cliente = $("#combocliente2").val();
    var role = document.getElementById("rol").value;
    var id_abogado = $("#comboabogado2").val();
    var tribunal = document.getElementById("Tribunal").value;
    var materia = document.getElementById("Materia").value;
    var procedimiento = document.getElementById("procedimiento").value;
    var cuantia = document.getElementById("Cuantia").value;
    var descripcion = document.getElementById("descripcion").value;
    var caratulado = document.getElementById("instancia").value;
    var name = document.getElementById("nombre").value;

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/causes/' + id_cause,
        data: {
            cause: {
                court: tribunal,
                description: descripcion,
                attorney_id: id_abogado,
                client_id: id_cliente,
                role: role,
                caratulado: caratulado,
                amount: cuantia,
                matter: materia,
                process: procedimiento,
                name: name,
                archived: false
            }
        }
    });
    toastr.error('SE MODIFICO CAUSA');
}

// Funciones mantenedor causa - tarea
function agregarCausaTarea() {
    var descripcion = document.getElementById("descripcion-tarea-causa").value;
    var id_cause = $("#idcausa").val();;
    var id_abogado = $('#id_Abogado_tarea_ingresar').val();

    //fecha
    var datetimeval = $("#fechacuasatarea").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    if (id_abogado != "") {
        if (datetimeval != "") {
            if (id_cause != "") {
                if (descripcion != "") {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: '/cause_statuses',
                        data: {
                            cause_status: {
                                cause_id: id_cause,
                                attorney_id: id_abogado,
                                "expiration_date(3i)": dia,
                                "expiration_date(2i)": mes,
                                "expiration_date(1i)": ano,
                                "expiration_date(4i)": hh,
                                "expiration_date(5i)": mm,
                                finished: false,
                                description: descripcion
                            }
                        },
                        success: function(data, textStatus, jQxhr) {

                            //limpiarNombreCausa();
                            mostrarlistacausatarea();
                            document.getElementById("descripcion-tarea-causa").value = "";
                            toastr.success('Se agrego tarea')
                            actualizarTAHCausa(id_cause);

                        },
                        error: function(jqXhr, textStatus, errorThrown) {
                            console.log("errorThrown");
                        }
                    });
                } else {
                    toastr.warning('SE DEBE LLENAR LA TAREA');
                    document.getElementById("descripcion-tarea-causa").style.border = "1px solid red";
                }
            } else {
                toastr.warning('DEBE SELECCIONAR UNA CAUSA');
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UN ABOGADO');
    }

}


function agregarAudiencia() {
    var id_causa = $("#idcausa").val();
    var id_abogado = $('#id_Abogado_audiencia_ingresar').val();
    var descripcion = $("#descripcion_audiencia").val();

    //fecha
    var datetimeval = $("#fechacuasaaudiencia").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);


    if (id_abogado != "") {
        if (datetimeval != "") {
            if (id_causa != "") {
                if (descripcion != "") {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: '/audiences',
                        data: {
                            audience: {
                                "date(3i)": dia,
                                "date(2i)": mes,
                                "date(1i)": ano,
                                "date(4i)": hh,
                                "date(5i)": mm,
                                attorney_id: id_abogado,
                                cause_id: id_causa,
                                description: descripcion,
                                finished: false
                            }
                        },
                        success: function(data, textStatus, jQxhr) {

                            //limpiarNombreCausa();
                            mostrarlistacausaaudiencia();
                            document.getElementById("descripcion_audiencia").value = "";
                            toastr.success('Se agrego audiencia')
                            actualizarTAHCausa(id_causa);
                            //actualizarListadoCausas();

                        },
                        error: function(jqXhr, textStatus, errorThrown) {
                            console.log("errorThrown");
                        }

                    });
                } else {
                    toastr.warning('SE DEBE LLENAR LA AUDIENCIA');
                }
            } else {
                toastr.warning('DEBE SELECCIONAR UNA CAUSA');
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UN ABOGADO');
    }


}

//Editar Cusa Tareas
function seleccionarCausaTarea(id_cause_status) {
    var id_cause_status = id_cause_status;
    mostrarcausatareaeditar();




    $.getJSON('/cause_statuses.json',
        function(data_statuses) {

            for (var i = 0; i < data_statuses.length; i++) {
                if (data_statuses[i][3] == id_cause_status) {
                    $("#descripcion-tarea-causa-editar").val(data_statuses[i][0]);

                    $("#fechacuasatareaeditar").val(data_statuses[i][1].substring(0, 10));
                    $("#id_causastatus").val(data_statuses[i][3]);
                    $('#imagentareaeditar').attr('src', data_statuses[i][6]);
                    $('#id_Abogado_tarea_editar').val(data_statuses[i][3]);
                }
            }


        });

}


//Editar Cusa Historia
function seleccionarCausaHistoria(id_historia) {
    var id_historia = id_historia;
    mostrarcausahistoriaciaeditar();

    $.getJSON('/cause_histories.json',
        function(data_historia) {

            for (var i = 0; i < data_historia.length; i++) {
                if (data_historia[i][2] == id_historia) {

                    $("#descripcion_causa_historia-editar").val(data_historia[i][0]);
                    $("#fechacuasahistoriaeditar").val(data_historia[i][1].substring(0, 10));
                    $("#id_historia").val(data_historia[i][2]);
                    $('#imagenhistoriaeditar').attr('src', data_historia[i][4]);
                    $('#id_Abogado_historia_editar').val(data_historia[i][5]);

                }
            }


        });

}



//Editar Cusa Audiencia
function seleccionarCausaAudiencia(id_audiencia) {
    var id_audiencia = id_audiencia;
    mostrarcausaaudienciaeditar();

    $.getJSON('/audiences.json',
        function(data_audiences) {
            for (var i = 0; i < data_audiences.length; i++) {
                if (data_audiences[i][6] == id_audiencia) {
                    $("#descripcion_audiencia-editar").val(data_audiences[i][7]);
                    $("#fechacuasaaudienciaeditar").val(data_audiences[i][0].replace('.000Z', ''));
                    $("#id_audiencia").val(data_audiences[i][6]);

                    $('#imagenaudienciaeditar').attr('src', data_audiences[i][10]);
                    $('#id_Abogado_audiencia_editar').val(data_audiences[i][1]);


                }
            }


        });

}

//Modificar una cusa tarea seleccionarada
function modificarCausaTarea(id_cause_status) {
    var id_causa = $("#idcausa").val();
    var id_cause_status = $("#id_causastatus").val();
    var datos_descripcion = $("#descripcion-tarea-causa-editar").val();
    var id_abogado = $("#id_Abogado_tarea_editar").val();

    //fecha
    var datetimeval = $("#fechacuasatareaeditar").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/cause_statuses/' + id_cause_status,
        data: {
            cause_status: {
                attorney_id: id_abogado,
                "expiration_date(3i)": dia,
                "expiration_date(2i)": mes,
                "expiration_date(1i)": ano,
                "expiration_date(4i)": hh,
                "expiration_date(5i)": mm,
                finished: false,
                description: datos_descripcion
            },
            success: function(data, textStatus, jQxhr) {

                //limpiarNombreCausa();
                mostrarlistacausatarea();
                toastr.success('Se modifico tarea')

                actualizarTAHCausa(id_causa);
                //actualizarListadoCausas();

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });
}

//Modificar una causa audiencia seleccionarada
function modificarCausaHistoria(id_historia) {
    var id_causa = $("#idcausa").val();
    var id_historia = $("#id_historia").val();
    var datos_descripcion = $("#descripcion_causa_historia-editar").val();
    var id_abogado = $("#id_Abogado_historia_editar").val();
    //fecha
    var datetimeval = $("#fechacuasahistoriaeditar").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/cause_histories/' + id_historia,
        data: {
            cause_history: {
                attorney_id: id_abogado,
                "date(3i)": dia,
                "date(2i)": mes,
                "date(1i)": ano,
                "date(4i)": hh,
                "date(5i)": mm,
                description: datos_descripcion
            },
            success: function(data, textStatus, jQxhr) {
                mostrarlistacausahistoria();
                toastr.success('Se modifico el comentario')
                actualizarTAHCausa(id_causa);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });
}

//Modificar una causa historia seleccionarada
function modificarCausaAudicencia(id_audiencia) {

    var id_causa = $("#idcausa").val();
    var id_audiencia = $("#id_audiencia").val();
    var datos_descripcion = $("#descripcion_audiencia-editar").val();
    var id_abogado = $("#id_Abogado_audiencia_editar").val();

    //fecha
    var datetimeval = $("#fechacuasaaudienciaeditar").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/audiences/' + id_audiencia,
        data: {
            audience: {
                attorney_id: id_abogado,
                "date(3i)": dia,
                "date(2i)": mes,
                "date(1i)": ano,
                "date(4i)": hh,
                "date(5i)": mm,
                finished: false,
                description: datos_descripcion
            },
            success: function(data, textStatus, jQxhr) {
                mostrarlistacausaaudiencia();
                toastr.success('Se modifico Audiencia')
                actualizarTAHCausa(id_causa);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });
}

function archivarCausaTarea() {
    var id_cause_status = $("#id_causastatus").val();
    var id_causa = $("#idcausa").val();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/cause_statuses/' + id_cause_status,
        data: {
            cause_status: {
                finished: true
            },
            success: function(data, textStatus, jQxhr) {
                toastr.warning('Se archivo la tarea')

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });

}


function archivarCausaAudiencia() {
    var id_audiencia = $("#id_audiencia").val();
    var id_causa = $("#idcausa").val();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/audiences/' + id_audiencia,
        data: {
            audience: {
                finished: true
            },
            success: function(data, textStatus, jQxhr) {
                toastr.warning('SE ARCHIVO LA AUDIENCIA')
                actualizarTAHCausa(id_causa);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });
}

function archivarCausaTarea_listado(id_tarea) {
    console.log("archivarCausaTarea_listado");
    var id_cause_status = id_tarea;
    var id_causa = $("#idcausa").val();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',

        url: '/cause_statuses/' + id_cause_status,
        data: {
            cause_status: {
                finished: true
            }
        },
        success: function(data) {
            ListadoTareasVintes();
            console.log(id_causa);
            actualizarTAHCausa(id_causa);
            toastr.warning('SE ARCHIVO LA TAREA')
        }

    });


}


function archivarCausaAudiencia_listado(id_audiencia) {
    var id_audiencia = id_audiencia;
    var id_causa = $("#idcausa").val();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        async: true,
        url: '/audiences/' + id_audiencia,
        data: {
            audience: {
                finished: true
            }
        },
        success: function() {
            actualizarTAHCausa(id_causa);
            ListadoAudienciasVigentes();
            toastr.warning('SE ARCHIVO LA AUDIENCIA')
        }
    });
}


function desarchivarCausaAudiencia(id_audiencia) {
    var id_audiencia = id_audiencia;
    var id_causa = $("#idcausa").val();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/audiences/' + id_audiencia,
        data: {
            audience: {
                finished: false
            }
        },
        success: function(data, textStatus, jQxhr) {
            toastr.warning('SE ARCHIVO LA AUDIENCIA')
            actualizarTAHCausa(id_causa);
        },
        error: function(jqXhr, textStatus, errorThrown) {
            console.log("errorThrown");
        }
    });
}




function desarchivarCausaTarea(id_cause_status) {
    var id_cause_status = id_cause_status;
    var id_causa = $("#idcausa").val();
    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'PUT',
        url: '/cause_statuses/' + id_cause_status,
        data: {
            cause_status: {
                finished: false
            },
            success: function(data, textStatus, jQxhr) {
                toastr.warning('SE ARCHIVO LA TAREA')
                actualizarTAHCausa(id_causa);
            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("errorThrown");
            }
        }
    });
}

function eliminarCausaTarea(id_tarea) {
    var opcion = confirm("Seguro de eliminar la tarea");
    var id_causa = $("#idcausa").val();
    var id_causa_tarea = id_tarea;
    if (opcion == true) {
        var id_cause_status = id_causa_tarea;
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'DELETE',
            url: '/cause_statuses/' + id_cause_status,

        });
    }
    toastr.error('SE ELIMINO TAREA');
    obtenerLogin();
    actualizarTAHCausa(id_causa);
}


function eliminarCausaAudiencia(id_audiencia) {
    var opcion = confirm("Seguro de eliminar la audiencia");
    var id_causa = $("#idcausa").val();
    if (opcion == true) {
        var id_audiencia = id_audiencia;
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'DELETE',
            url: '/audiences/' + id_audiencia,

        });
    }
    toastr.error('SE ELIMINO AUDIENCIA');
    obtenerLogin();
    actualizarTAHCausa(id_causa);

}

function eliminarCausaHistoria(id_historia) {
    var opcion = confirm("Seguro de eliminar la historia");
    var id_causa = $("#idcausa").val();

    if (opcion == true) {
        var id_cause_histories = id_historia;
        $.ajax({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'DELETE',
            url: '/cause_histories/' + id_cause_histories,

        });
    }
    toastr.error('SE ELIMINO HISTORIA');
    actualizarTAHCausa(id_causa);
}


function agregarCausaHistoria() {

    var id_causa = $("#idcausa").val();
    var id_abogado = $("#id_Abogado_historia_ingresar").val();
    var descripcion = $("#descripcion_causa_historia").val();

    //fecha
    var datetimeval = $("#fechacuasahistoria").val();
    var ano = datetimeval.substring(0, 4);
    var mes = datetimeval.substring(7, 5);
    var dia = datetimeval.substring(10, 8);
    var hh = datetimeval.substring(13, 11);
    var mm = datetimeval.substring(16, 14);

    if (id_abogado != "") {
        if (datetimeval != "") {
            if (id_causa != "") {
                if (descripcion != "") {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: '/cause_histories',
                        data: {
                            cause_history: {
                                "date(3i)": dia,
                                "date(2i)": mes,
                                "date(1i)": ano,
                                "date(4i)": hh,
                                "date(5i)": mm,
                                attorney_id: id_abogado,
                                cause_id: id_causa,
                                description: descripcion
                            }
                        },
                        success: function(data, textStatus, jQxhr) {
                            mostrarlistacausahistoria();
                            document.getElementById("descripcion_causa_historia").value = "";
                            toastr.success('Se agrego la historia')
                            actualizarTAHCausa(id_causa);
                        },
                        error: function(jqXhr, textStatus, errorThrown) {
                            console.log("errorThrown");
                        }
                    });
                } else {
                    toastr.warning('SE DEBE LLENAR CON UN COMENTARIO');
                }
            } else {
                toastr.warning('DEBE SELECCIONAR UNA CAUSA');
            }
        } else {
            toastr.warning('DEBE SELECCIONAR UNA FECHA Y HORA');
        }
    } else {
        toastr.warning('DEBE SELECCIONAR UN ABOGADO');
    }

}

//CREAR CODIGO DE CAUSA

function crearCodigoCausa() {

    var nuevo_codigo = null;

    $.getJSON('/causes.json',
        function(data) {
            var largo = data.length - 1
            var code = data[largo].code;
            var largo_codigo = code.length
            var letra = code.substring(0, 1);
            var numero = code.substring(1, largo_codigo)

            codigo_nuevo = parseInt(numero) + 1;

            $("#nuevocodigo").val(codigo_nuevo);
        });

}

//CAMBIAR ABOGADO

//NUEVO
function cambiarAbogadoNuevo_T(id) {

    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagentareaingresar').attr('src', dataabogado.photo);
            $('#id_Abogado_tarea_ingresar').val(dataabogado.id);
        });
}

function cambiarAbogado_L_T(id, id_abo) {
    var id_tarea = id;
    $.getJSON('/attorneys/' + id_abo + '.json',
        function(dataabogado) {

            $('#imagentareal' + id_tarea).attr('src', dataabogado.photo);
            ModificarAbogadoCausaTarea(dataabogado.id, id_tarea)
        });
}

function cambiarAbogadoNuevo_A(id) {
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenaudienciaingresar').attr('src', dataabogado.photo);
            $('#id_Abogado_audiencia_ingresar').val(dataabogado.id);
        });
}

function cambiarAbogado_L_A(id, id_abo) {
    var id_au = id;
    $.getJSON('/attorneys/' + id_abo + '.json',
        function(dataabogado) {

            $('#imagenaul' + id_au).attr('src', dataabogado.photo);
            ModificarAbogadoCausaAudiencia(dataabogado.id, id_au)
        });
}

function cambiarAbogadoNuevo_H(id) {
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {

            $('#imagenhistoriaingresar').attr('src', dataabogado.photo);
            $('#id_Abogado_historia_ingresar').val(dataabogado.id);

        });
}

//EDITAR
function cambiarAbogadoEditar_T(id) {
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagentareaeditar').attr('src', dataabogado.photo);
            $('#id_Abogado_tarea_editar').val(dataabogado.id);
        });
}

function cambiarAbogadoEditar_A(id) {
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenaudienciaeditar').attr('src', dataabogado.photo);
            $('#id_Abogado_audiencia_editar').val(dataabogado.id);
        });
}

function cambiarAbogadoEditar_H(id) {
    $.getJSON('/attorneys/' + id + '.json',
        function(dataabogado) {
            $('#imagenhistoriaeditar').attr('src', dataabogado.photo);
            $('#id_Abogado_historia_editar').val(dataabogado.id);
        });
}


function ListadoTareasVintes(login, user) {
    tipo = login;
    id_user = user;

    $.getJSON('/attorneys.json',
        function(dataabogado) {
            for (var i = 0; i < dataabogado.length; i++) {
                if (dataabogado[i].user_id == id_user) {
                    dahboard(dataabogado[i].id)
                    ListadoAudienciasVigentes(dataabogado[i].id)
                }
            }
        });


}

function dahboard(id_abogado) {

    id_abogado = id_abogado;
    $.getJSON('/dashboard_causes.json',
        function(data) {

            if (data.length != 0) {
                $("#table-dashboard-tarea tr").remove();
                $("#table-dashboard-tarea-hoy tr").remove();
                $("#table-dashboard-tarea-m tr").remove();
                var tr;
                var j = 0;
                for (var i = 0; i < data.length; i++) {
                    var today = new Date();
                    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                    //Tareas Vencidas
                    if (data[i].attorney_id == id_abogado) {
                        if (data[i].fecha_tarea < date) {
                            if (data[i].tipo == 'causa') {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td><i  style='cursor:pointer'  onclick='(archivarCausaTarea_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>");
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(eliminarCausaTarea(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td class='badge bg-lead'>" + data[i].fecha_tarea + "</td>");
                                $('#table-dashboard-tarea').append(tr);
                                j = j + 1;
                            } else {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(ArchivarWorkTareas_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(EliminarTarea(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td class='badge bg-lead'>" + data[i].fecha_tarea + "</td>");
                                $('#table-dashboard-tarea').append(tr);
                                j = j + 1;
                            }
                        }
                    }


                    //Tareas de Hoy
                    if (data[i].attorney_id == id_abogado) {
                        if (data[i].fecha_tarea == date) {
                            if (data[i].tipo == 'causa') {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td><i  style='cursor:pointer'  onclick='(archivarCausaTarea_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(eliminarCausaTarea(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td class='badge bg-lead'>" + data[i].fecha_tarea + "</td>");
                                $('#table-dashboard-tarea-hoy').append(tr);
                                j = j + 1;
                            } else {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(ArchivarWorkTareas_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(EliminarTarea(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td class='badge bg-lead'>" + data[i].fecha_tarea + "</td>");
                                $('#table-dashboard-tarea-hoy').append(tr);
                                j = j + 1;
                            }
                        }
                    }

                    //Tareas de Manana
                    var datem = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + (today.getDate() + 1);
                    if (data[i].attorney_id == id_abogado) {
                        if (data[i].fecha_tarea == datem) {
                            if (data[i].tipo == 'causa') {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td><i  style='cursor:pointer'  onclick='(archivarCausaTarea_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(eliminarCausaTarea(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td class='badge bg-lead'>" + data[i].fecha_tarea + "</td>");
                                $('#table-dashboard-tarea-m').append(tr);
                                j = j + 1;
                            } else {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(ArchivarWorkTareas_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(EliminarTarea(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td class='badge bg-lead'>" + data[i].fecha_tarea + "</td>");
                                $('#table-dashboard-tarea-m').append(tr);
                                j = j + 1;
                            }
                        }
                    }


                }
            }
        });
}

function ListadoAudienciasVigentes(id_abogado) {
    id_abogado = id_abogado;
    $.getJSON('/dashboard_audiencias.json',
        function(data) {
            if (data.length != 0) {

                $("#table-dashboard-audiencia tr").remove();
                $("#table-dashboard-audiencia-h tr").remove();
                $("#table-dashboard-audiencia-m tr").remove();
                var tr;
                var j = 0;
                for (var i = 0; i < data.length; i++) {

                    var today = new Date();
                    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();



                    if (data[i].attorney_id == id_abogado) {
                        if (data[i].fecha_audiencia < date) {
                            if (data[i].tipo == 'audiencia') {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td><i  style='cursor:pointer'  onclick='(archivarCausaAudiencia_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>")
                                tr.append("<td >" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(eliminarCausaAudiencia(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td style='margin-left: 100px;' class='badge bg-lead'>" + data[i].fecha_audiencia + "</td>");
                                $('#table-dashboard-audiencia').append(tr);
                                j = j + 1;
                            } else {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(ArchivarReuniones_listado(" + data[i].id_t + "))' class='far fa-circle'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(EliminarReunion(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td style='margin-left: 100px;'  class='badge bg-lead'>" + data[i].fecha_audiencia + "</td>");
                                $('#table-dashboard-audiencia').append(tr);
                                j = j + 1;
                            }
                        }
                    }

                    //Tareas hoy
                    if (data[i].attorney_id == id_abogado) {
                        if (data[i].fecha_audiencia == date) {
                            if (data[i].tipo == 'audiencia') {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td><i  style='cursor:pointer'  onclick='(archivarCausaAudiencia_listado(" + data[i].id_t + "))' class='far fa-square'></i></td>")
                                tr.append("<td >" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(eliminarCausaAudiencia(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td style='margin-left: 100px;' class='badge bg-lead'>" + data[i].fecha_audiencia + "</td>");
                                $('#table-dashboard-audiencia-h').append(tr);
                                j = j + 1;
                            } else {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(ArchivarReuniones_listado(" + data[i].id_t + "))' class='far fa-square'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(EliminarReunion(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td style='margin-left: 100px;'  class='badge bg-lead'>" + data[i].fecha_audiencia + "</td>");
                                $('#table-dashboard-audiencia-h').append(tr);
                                j = j + 1;
                            }
                        }
                    }




                    //Tareas Manana
                    var datem = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + (today.getDate() + 1);
                    if (data[i].attorney_id == id_abogado) {
                        if (data[i].fecha_audiencia == datem) {
                            if (data[i].tipo == 'audiencia') {
                                tr = $("<tr style='height: 80px;' class='dh_tr'/>");
                                tr.append("<td><i  style='cursor:pointer'  onclick='(archivarCausaAudiencia_listado(" + data[i].id_t + "))' class='far fa-square'></i></td>")
                                tr.append("<td >" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(eliminarCausaAudiencia(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td style='margin-left: 100px;' class='badge bg-lead'>" + data[i].fecha_audiencia + "</td>");
                                $('#table-dashboard-audiencia-m').append(tr);
                                j = j + 1;
                            } else {
                                tr = $("<tr/ class='dh_tr'>");
                                tr.append("<td width='20'><i  style='cursor:pointer'  onclick='(ArchivarReuniones_listado(" + data[i].id_t + "))' class='far fa-square'></i></td>")
                                tr.append("<td>" + data[i].description + "<br><a>" + data[i].name + " / " + data[i].cliente + "</a></td>");
                                tr.append("<td class='order-icon'><i style='cursor:pointer;' onclick='(EliminarReunion(" + data[i].id_t + "))'  class='fas fa-eraser ' style='color: #c4a215;' ></i></td>");
                                tr.append("<td><i id='edit' style='cursor:pointer;' onclick='()'  class='far fa-edit'></i></td>");
                                tr.append("<td style='margin-left: 100px;'  class='badge bg-lead'>" + data[i].fecha_audiencia + "</td>");
                                $('#table-dashboard-audiencia-m').append(tr);
                                j = j + 1;
                            }
                        }
                    }




                }

            }
        });
}

function fecha_actual(nombre_fecha) {
    //Fecha
    var now = new Date();
    var utcString = now.toISOString().substring(0, 19);
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    var localDatetime = year + "-" +
        (month < 10 ? "0" + month.toString() : month) + "-" +
        (day < 10 ? "0" + day.toString() : day) + "T" +
        (hour < 10 ? "0" + hour.toString() : hour) + ":" +
        (minute < 10 ? "0" + minute.toString() : minute) +
        utcString.substring(16, 19);


    $(nombre_fecha).val(localDatetime);
}

function fecha(fecha) {
    var now = new Date();
    var utcString = now.toISOString().substring(0, 19);
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    var localDatetime = year + "-" +
        (month < 10 ? "0" + month.toString() : month) + "-" +
        (day < 10 ? "0" + day.toString() : day);
    $(fecha).val(localDatetime);
}