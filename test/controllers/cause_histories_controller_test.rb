require 'test_helper'

class CauseHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cause_history = cause_histories(:one)
  end

  test "should get index" do
    get cause_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_cause_history_url
    assert_response :success
  end

  test "should create cause_history" do
    assert_difference('CauseHistory.count') do
      post cause_histories_url, params: { cause_history: { attorney_id: @cause_history.attorney_id, cause_id: @cause_history.cause_id, date: @cause_history.date, description: @cause_history.description } }
    end

    assert_redirected_to cause_history_url(CauseHistory.last)
  end

  test "should show cause_history" do
    get cause_history_url(@cause_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_cause_history_url(@cause_history)
    assert_response :success
  end

  test "should update cause_history" do
    patch cause_history_url(@cause_history), params: { cause_history: { attorney_id: @cause_history.attorney_id, cause_id: @cause_history.cause_id, date: @cause_history.date, description: @cause_history.description } }
    assert_redirected_to cause_history_url(@cause_history)
  end

  test "should destroy cause_history" do
    assert_difference('CauseHistory.count', -1) do
      delete cause_history_url(@cause_history)
    end

    assert_redirected_to cause_histories_url
  end
end
