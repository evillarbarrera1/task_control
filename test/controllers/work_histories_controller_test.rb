require 'test_helper'

class WorkHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @work_history = work_histories(:one)
  end

  test "should get index" do
    get work_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_work_history_url
    assert_response :success
  end

  test "should create work_history" do
    assert_difference('WorkHistory.count') do
      post work_histories_url, params: { work_history: { attorney_id: @work_history.attorney_id, date: @work_history.date, description: @work_history.description, work_id: @work_history.work_id } }
    end

    assert_redirected_to work_history_url(WorkHistory.last)
  end

  test "should show work_history" do
    get work_history_url(@work_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_work_history_url(@work_history)
    assert_response :success
  end

  test "should update work_history" do
    patch work_history_url(@work_history), params: { work_history: { attorney_id: @work_history.attorney_id, date: @work_history.date, description: @work_history.description, work_id: @work_history.work_id } }
    assert_redirected_to work_history_url(@work_history)
  end

  test "should destroy work_history" do
    assert_difference('WorkHistory.count', -1) do
      delete work_history_url(@work_history)
    end

    assert_redirected_to work_histories_url
  end
end
