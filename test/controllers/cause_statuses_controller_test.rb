require 'test_helper'

class CauseStatusesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cause_status = cause_statuses(:one)
  end

  test "should get index" do
    get cause_statuses_url
    assert_response :success
  end

  test "should get new" do
    get new_cause_status_url
    assert_response :success
  end

  test "should create cause_status" do
    assert_difference('CauseStatus.count') do
      post cause_statuses_url, params: { cause_status: { attorney_id: @cause_status.attorney_id, cause_id: @cause_status.cause_id, description: @cause_status.description, expiration_date: @cause_status.expiration_date, finished: @cause_status.finished } }
    end

    assert_redirected_to cause_status_url(CauseStatus.last)
  end

  test "should show cause_status" do
    get cause_status_url(@cause_status)
    assert_response :success
  end

  test "should get edit" do
    get edit_cause_status_url(@cause_status)
    assert_response :success
  end

  test "should update cause_status" do
    patch cause_status_url(@cause_status), params: { cause_status: { attorney_id: @cause_status.attorney_id, cause_id: @cause_status.cause_id, description: @cause_status.description, expiration_date: @cause_status.expiration_date, finished: @cause_status.finished } }
    assert_redirected_to cause_status_url(@cause_status)
  end

  test "should destroy cause_status" do
    assert_difference('CauseStatus.count', -1) do
      delete cause_status_url(@cause_status)
    end

    assert_redirected_to cause_statuses_url
  end
end
