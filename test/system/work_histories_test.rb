require "application_system_test_case"

class WorkHistoriesTest < ApplicationSystemTestCase
  setup do
    @work_history = work_histories(:one)
  end

  test "visiting the index" do
    visit work_histories_url
    assert_selector "h1", text: "Work Histories"
  end

  test "creating a Work history" do
    visit work_histories_url
    click_on "New Work History"

    fill_in "Attorney", with: @work_history.attorney_id
    fill_in "Date", with: @work_history.date
    fill_in "Description", with: @work_history.description
    fill_in "Work", with: @work_history.work_id
    click_on "Create Work history"

    assert_text "Work history was successfully created"
    click_on "Back"
  end

  test "updating a Work history" do
    visit work_histories_url
    click_on "Edit", match: :first

    fill_in "Attorney", with: @work_history.attorney_id
    fill_in "Date", with: @work_history.date
    fill_in "Description", with: @work_history.description
    fill_in "Work", with: @work_history.work_id
    click_on "Update Work history"

    assert_text "Work history was successfully updated"
    click_on "Back"
  end

  test "destroying a Work history" do
    visit work_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Work history was successfully destroyed"
  end
end
