require "application_system_test_case"

class CauseHistoriesTest < ApplicationSystemTestCase
  setup do
    @cause_history = cause_histories(:one)
  end

  test "visiting the index" do
    visit cause_histories_url
    assert_selector "h1", text: "Cause Histories"
  end

  test "creating a Cause history" do
    visit cause_histories_url
    click_on "New Cause History"

    fill_in "Attorney", with: @cause_history.attorney_id
    fill_in "Cause", with: @cause_history.cause_id
    fill_in "Date", with: @cause_history.date
    fill_in "Description", with: @cause_history.description
    click_on "Create Cause history"

    assert_text "Cause history was successfully created"
    click_on "Back"
  end

  test "updating a Cause history" do
    visit cause_histories_url
    click_on "Edit", match: :first

    fill_in "Attorney", with: @cause_history.attorney_id
    fill_in "Cause", with: @cause_history.cause_id
    fill_in "Date", with: @cause_history.date
    fill_in "Description", with: @cause_history.description
    click_on "Update Cause history"

    assert_text "Cause history was successfully updated"
    click_on "Back"
  end

  test "destroying a Cause history" do
    visit cause_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cause history was successfully destroyed"
  end
end
