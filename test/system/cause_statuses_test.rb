require "application_system_test_case"

class CauseStatusesTest < ApplicationSystemTestCase
  setup do
    @cause_status = cause_statuses(:one)
  end

  test "visiting the index" do
    visit cause_statuses_url
    assert_selector "h1", text: "Cause Statuses"
  end

  test "creating a Cause status" do
    visit cause_statuses_url
    click_on "New Cause Status"

    fill_in "Attorney", with: @cause_status.attorney_id
    fill_in "Cause", with: @cause_status.cause_id
    fill_in "Description", with: @cause_status.description
    fill_in "Expiration date", with: @cause_status.expiration_date
    fill_in "Finished", with: @cause_status.finished
    click_on "Create Cause status"

    assert_text "Cause status was successfully created"
    click_on "Back"
  end

  test "updating a Cause status" do
    visit cause_statuses_url
    click_on "Edit", match: :first

    fill_in "Attorney", with: @cause_status.attorney_id
    fill_in "Cause", with: @cause_status.cause_id
    fill_in "Description", with: @cause_status.description
    fill_in "Expiration date", with: @cause_status.expiration_date
    fill_in "Finished", with: @cause_status.finished
    click_on "Update Cause status"

    assert_text "Cause status was successfully updated"
    click_on "Back"
  end

  test "destroying a Cause status" do
    visit cause_statuses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cause status was successfully destroyed"
  end
end
