require "application_system_test_case"

class WorkStatusesTest < ApplicationSystemTestCase
  setup do
    @work_status = work_statuses(:one)
  end

  test "visiting the index" do
    visit work_statuses_url
    assert_selector "h1", text: "Work Statuses"
  end

  test "creating a Work status" do
    visit work_statuses_url
    click_on "New Work Status"

    fill_in "Attorney", with: @work_status.attorney_id
    fill_in "Description", with: @work_status.description
    fill_in "Expiration date", with: @work_status.expiration_date
    fill_in "Finished", with: @work_status.finished
    fill_in "Work", with: @work_status.work_id
    click_on "Create Work status"

    assert_text "Work status was successfully created"
    click_on "Back"
  end

  test "updating a Work status" do
    visit work_statuses_url
    click_on "Edit", match: :first

    fill_in "Attorney", with: @work_status.attorney_id
    fill_in "Description", with: @work_status.description
    fill_in "Expiration date", with: @work_status.expiration_date
    fill_in "Finished", with: @work_status.finished
    fill_in "Work", with: @work_status.work_id
    click_on "Update Work status"

    assert_text "Work status was successfully updated"
    click_on "Back"
  end

  test "destroying a Work status" do
    visit work_statuses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Work status was successfully destroyed"
  end
end
