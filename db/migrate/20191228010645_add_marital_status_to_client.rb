class AddMaritalStatusToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :marital_status, :string
  end
end
