class AddCodeColumnToRegion < ActiveRecord::Migration[5.2]
  def change
    add_column :regions, :code, :string
  end
end
