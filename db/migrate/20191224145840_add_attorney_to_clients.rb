class AddAttorneyToClients < ActiveRecord::Migration[5.2]
  def change
    add_reference :clients, :attorney, foreign_key: true
  end
end
