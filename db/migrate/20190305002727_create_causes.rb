class CreateCauses < ActiveRecord::Migration[5.2]
  def change
    create_table :causes do |t|
      t.string :name
      t.string :court
      t.string :code
      t.string :description
      t.references :attorney, foreign_key: true
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
