class AddWorkColluns2ToWorks < ActiveRecord::Migration[5.2]
  def change
    add_reference :works, :attorney, foreign_key: true
  end
end
