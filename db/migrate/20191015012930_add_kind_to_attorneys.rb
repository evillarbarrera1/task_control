class AddKindToAttorneys < ActiveRecord::Migration[5.2]
  def change
    add_column :attorneys, :kind, :string
  end
end
