class CreateAudiences < ActiveRecord::Migration[5.2]
  def change
    create_table :audiences do |t|
      t.datetime :date
      t.references :attorney, foreign_key: true
      t.references :cause, foreign_key: true

      t.timestamps
    end
  end
end
