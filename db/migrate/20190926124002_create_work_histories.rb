class CreateWorkHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :work_histories do |t|
      t.string :description
      t.datetime :date
      t.references :attorney, foreign_key: true
      t.references :work, foreign_key: true

      t.timestamps
    end
  end
end
