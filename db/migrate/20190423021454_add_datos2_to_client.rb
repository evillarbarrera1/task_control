class AddDatos2ToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :kind, :string
    add_column :clients, :region, :string
    add_column :clients, :commune, :string
    add_column :clients, :rut, :string
    add_column :clients, :name_legal, :string
    add_column :clients, :rut_legal, :string
    add_column :clients, :mail_legal, :string
    add_column :clients, :phone_legal, :string
  end
end
