class CreateCauseStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :cause_statuses do |t|
      t.references :cause, foreign_key: true
      t.references :attorney, foreign_key: true
      t.datetime :expiration_date
      t.boolean :finished
      t.string :description

      t.timestamps
    end
  end
end
