class AddCourtOrderToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :court_order, :string
  end
end
