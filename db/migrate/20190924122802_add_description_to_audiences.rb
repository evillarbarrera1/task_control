class AddDescriptionToAudiences < ActiveRecord::Migration[5.2]
  def change
    add_column :audiences, :description, :string
  end
end
