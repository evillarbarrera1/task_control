class AddWorkActiveToWorks < ActiveRecord::Migration[5.2]
  def change
    add_column :works, :active, :boolean
  end
end
