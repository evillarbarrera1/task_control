class AddFinishedToAudiences < ActiveRecord::Migration[5.2]
  def change
    add_column :audiences, :finished, :boolean
  end
end
