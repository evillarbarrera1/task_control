class AddCausesToCauses < ActiveRecord::Migration[5.2]
  def change
    add_column :causes, :role, :string
    add_column :causes, :caratulado, :string
    add_column :causes, :amount, :string
    add_column :causes, :matter, :string
    add_column :causes, :process, :string
    add_column :causes, :archived, :boolean
  end
end
