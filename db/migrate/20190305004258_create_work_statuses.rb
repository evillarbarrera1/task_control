class CreateWorkStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :work_statuses do |t|
      t.references :work, foreign_key: true
      t.references :attorney, foreign_key: true
      t.datetime :expiration_date
      t.boolean :finished
      t.string :description

      t.timestamps
    end
  end
end
