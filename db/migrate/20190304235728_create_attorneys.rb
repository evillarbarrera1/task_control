class CreateAttorneys < ActiveRecord::Migration[5.2]
  def change
    create_table :attorneys do |t|
      t.string :name
      t.string :rut
      t.string :profession
      t.string :job
      t.string :civil_status
      t.datetime :birthdate
      t.string :mobile
      t.string :email
      t.string :photo
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
