class AddColumnsToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :address, :string
    add_column :clients, :option, :string
    add_column :clients, :profession, :string
  end
end
