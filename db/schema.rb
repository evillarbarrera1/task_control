# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_28_010704) do

  create_table "attorneys", force: :cascade do |t|
    t.string "name"
    t.string "rut"
    t.string "profession"
    t.string "job"
    t.string "civil_status"
    t.datetime "birthdate"
    t.string "mobile"
    t.string "email"
    t.string "photo"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "kind"
    t.index ["user_id"], name: "index_attorneys_on_user_id"
  end

  create_table "audiences", force: :cascade do |t|
    t.datetime "date"
    t.integer "attorney_id"
    t.integer "cause_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.boolean "finished"
    t.index ["attorney_id"], name: "index_audiences_on_attorney_id"
    t.index ["cause_id"], name: "index_audiences_on_cause_id"
  end

  create_table "cause_histories", force: :cascade do |t|
    t.string "description"
    t.datetime "date"
    t.integer "attorney_id"
    t.integer "cause_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attorney_id"], name: "index_cause_histories_on_attorney_id"
    t.index ["cause_id"], name: "index_cause_histories_on_cause_id"
  end

  create_table "cause_statuses", force: :cascade do |t|
    t.integer "cause_id"
    t.integer "attorney_id"
    t.datetime "expiration_date"
    t.boolean "finished"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attorney_id"], name: "index_cause_statuses_on_attorney_id"
    t.index ["cause_id"], name: "index_cause_statuses_on_cause_id"
  end

  create_table "causes", force: :cascade do |t|
    t.string "name"
    t.string "court"
    t.string "code"
    t.string "description"
    t.integer "attorney_id"
    t.integer "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
    t.string "caratulado"
    t.string "amount"
    t.string "matter"
    t.string "process"
    t.boolean "archived"
    t.index ["attorney_id"], name: "index_causes_on_attorney_id"
    t.index ["client_id"], name: "index_causes_on_client_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "plan"
    t.string "kind"
    t.string "region"
    t.string "commune"
    t.string "rut"
    t.string "name_legal"
    t.string "rut_legal"
    t.string "mail_legal"
    t.string "phone_legal"
    t.integer "attorney_id"
    t.string "address"
    t.string "option"
    t.string "profession"
    t.string "marital_status"
    t.string "court_order"
    t.index ["attorney_id"], name: "index_clients_on_attorney_id"
    t.index ["user_id"], name: "index_clients_on_user_id"
  end

  create_table "commons", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "meetings", force: :cascade do |t|
    t.string "description"
    t.datetime "date"
    t.integer "attorney_id"
    t.integer "work_id"
    t.boolean "finished"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attorney_id"], name: "index_meetings_on_attorney_id"
    t.index ["work_id"], name: "index_meetings_on_work_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "work_histories", force: :cascade do |t|
    t.string "description"
    t.datetime "date"
    t.integer "attorney_id"
    t.integer "work_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attorney_id"], name: "index_work_histories_on_attorney_id"
    t.index ["work_id"], name: "index_work_histories_on_work_id"
  end

  create_table "work_statuses", force: :cascade do |t|
    t.integer "work_id"
    t.integer "attorney_id"
    t.datetime "expiration_date"
    t.boolean "finished"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attorney_id"], name: "index_work_statuses_on_attorney_id"
    t.index ["work_id"], name: "index_work_statuses_on_work_id"
  end

  create_table "works", force: :cascade do |t|
    t.string "name"
    t.string "area"
    t.integer "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "materia"
    t.string "tipo"
    t.string "code"
    t.integer "attorney_id"
    t.boolean "active"
    t.index ["attorney_id"], name: "index_works_on_attorney_id"
    t.index ["client_id"], name: "index_works_on_client_id"
  end

end
