Rails.application.routes.draw do

  resources :work_histories
  resources :meetings
  resources :commons
  resources :cause_histories
  resources :user_web
  resources :dashboard_causes
  resources :dashboard_audiencias

  get 'calender/index'
  get 'user_web/index'
  get 'user/index'
  
  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  resources :works
  resources :causes
  resources :clients
  resources :attorneys
  resources :cause_statuses
  resources :work_statuses
  resources :audiences
  resources :client_works
  resources :work_json
  resources :user
  resources :calender
  resources :regions
  resources :user_datos
  resources :login
  resources :causes_abogado
  resources :work_abogado

  root 'welcome#index'
  get  'welcome/index'

end
